import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const headerBackgroundState = (state: RootState) => state.headerBackgroundReducer

export const headerBackgroundSelector = createSelector(
  headerBackgroundState,
  state => state
)
