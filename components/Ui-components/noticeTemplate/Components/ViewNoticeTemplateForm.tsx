import React, { useEffect, useState } from "react";
import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import { getOneNoticeTemplate, noticeTemplateSelector } from "../../../../redux/noticeTemplate";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";

interface ViewEditNoticeTemplate {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (name: string, slug: string, 
        description: string, thumb_url: string) => void;
    response: any;
}
const ViewEditNoticeTemplate = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
    response
}: ViewEditNoticeTemplate) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        thumb_url: "",
        description:"",
        // language_id:"",
        // typeId:"",
        // client_Id:"",
        // is_premium:"",
        // has_header_background:"",
        // has_page_background:"",
        // notice_type_id:"",
        // model:"",
        // clip_arts:"",
        // borders:"",
        // frames: "",
        // header_backgrounds: "",
        // languages:"",
        // page_backgrounds:"",
        // tags:"",
        // template_groups:""

    });

    // error handle
    const { dataOne: OneNoticeTemplateData, error: apiError } = useAppSelector(noticeTemplateSelector);
    const errors = useApiErrorHandling(apiError, response);

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                handleEdit(formData.name, formData.slug, formData.thumb_url, formData.description,);
                setClickStatus(false);
            };
        }
    }, [clickStatus, handleEdit,handleChange]);

    // single view get
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        dispatch(
            getOneNoticeTemplate({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                id: viewId,
            })
        ).then((res: any) => {
            if (res.payload.code === 200) {
                setSkelton(false);
            }
        });
    };

    useEffect(() => {
        setFormData({
            name: OneNoticeTemplateData[0]?.name,
            slug: OneNoticeTemplateData[0]?.slug,
            description: OneNoticeTemplateData[0]?.description,
            thumb_url: OneNoticeTemplateData[0]?.language_id,

        });
    }, [skelton]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="slug"
                                defaultValue={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Description</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="description"
                                defaultValue={formData.description}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Language</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="language_id"
                                defaultValue={formData.thumb_url}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    {/* <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="typeId"
                                defaultValue={formData.typeId}
                                onChange={handleChange}
                            />
                        )}
                    </div> */}

                </div>
            </div>
        </>
    );
};

export default ViewEditNoticeTemplate;
