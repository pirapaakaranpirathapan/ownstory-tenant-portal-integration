import React, { useEffect, useState } from "react";
import {
    getOneRelationship,
    relationshipSelector,
} from "../../../../redux/relationship";
import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import { borderSelector } from "../../../../redux/borders";

interface ViewEditBorder {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (name: string, slug: string, thumb_url: string) => void;
}
const ViewEditBorder = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
}: ViewEditBorder) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        thumb_url: "",
    });

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleEdit(formData.name, formData.slug, formData.thumb_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleEdit]);

    const { dataOne: OneBordersData } =
        useAppSelector(borderSelector);
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        // dispatch(
        //     getOneRelationship({
        //         activePartner: ACTIVE_PARTNER,
        //         activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        //         id: viewId,
        //     })
        // ).then((res: any) => {
        //     if (res.payload.code === 200) {
        //         setSkelton(false);
        //     }
        // });
    };

    useEffect(() => {
        setFormData({
            name: OneBordersData[0]?.name,
            slug: OneBordersData[0]?.slug,
            thumb_url: OneBordersData[0]?.thumb_url,
        });
    }, [loadData]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="slug"
                                defaultValue={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Thumb Url<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="ThumbUrl"
                                name="thumb_url"
                                defaultValue={formData.thumb_url}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default ViewEditBorder;
