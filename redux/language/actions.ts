
import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_ROUTE } from "../../config/constants";
import { get } from "../../connections/fetch_wrapper";


//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular Languages, and depending on the response, either returns the data or an error message.

export const getAllInterfaceLanguages = createAsyncThunk(
  "languages/getall",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/interface-languages`);
      if (response.code === 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);