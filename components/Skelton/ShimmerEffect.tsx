 const ShimmerEffect = ({ width, height }: any) => {
    return (
      <div
        className="shimmer-effect skelton-disable"
        style={{ width: width, height: height ,cursor: "default", pointerEvents: "none" }}
        
      ></div>
    );
  };
  export default  ShimmerEffect;