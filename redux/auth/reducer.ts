

import { createReducer } from '@reduxjs/toolkit';
import { login, logout } from './actions';
import { getCookie, deleteCookie, setCookie } from 'cookies-next';
import router, { useRouter } from 'next/router'
import { ResponseStatus } from '../../helpers/response_status';
import { encryptToken } from '../../utils/encryptionAndDecryptionUtils';
import { IApiResponse } from '../../interface/manage/IApiResponse';
import { IAuth } from '../../interface/IAuth';


interface IReducerInitialState {
  data: IAuth;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: [] as unknown as String[],
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: {} as unknown as IAuth,
};

//  The `authReducer` manages the state related to authentication.
//  It handles login and logout actions, updating the state accordingly
export const authReducer = createReducer(initialState, (builder) => {

  //login
  builder
    .addCase(login.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(login.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      let data = payload as IApiResponse
      state.data = data.data;
      const accessToken = data.data.data?.["access_token"];

      const encryptedToken = encryptToken(accessToken);
      setCookie('token', encryptedToken);
      const token = getCookie('token')
      if (token !== "") {
        window.location.href = '/dashboard'
      }
    })
    .addCase(login.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  //logout
  builder
    .addCase(logout.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = initialState.data;
    })
    .addCase(logout.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = initialState.data;
      deleteCookie('token');
      // deleteCookie('activepartner');
      // deleteCookie('activeserviceprovider');
      // deleteCookie('serviceprovidername')
      router.push('/');
    })
    .addCase(logout.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


});
export default authReducer;
