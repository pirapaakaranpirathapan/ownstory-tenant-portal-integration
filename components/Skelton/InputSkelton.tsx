import React from "react";
import ShimmerEffect from "./ShimmerEffect";

const InputSkelton = () => {
    return (
        <div>
            <td className=" col-2 p-2">
                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                    <div className="p-0 m-0 text-truncate rounded">
                        <ShimmerEffect width="450px" height="35px" />
                    </div>
                </div>
            </td>
        </div>
    );
};

export default InputSkelton;
