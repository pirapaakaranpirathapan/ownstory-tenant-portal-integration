import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const customerState = (state: RootState) => state.customerReducer

export const customerSelector = createSelector(
  customerState,
  state => state
)
