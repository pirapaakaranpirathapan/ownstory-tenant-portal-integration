
import { PutObjectCommand } from "@aws-sdk/client-s3";
import React, { ChangeEvent, useContext, useEffect, useState } from "react";
import { Input, Notification, Upload } from "@arco-design/web-react";
import { Button, Modal } from "react-bootstrap";
import Image from "next/image";
import cross from "../../public/images/cross.svg";
import { Progress } from "@arco-design/web-react";
import { Spin } from "@arco-design/web-react";
import BG from "./../../public/backImage.svg";
import PinturaImageUploader from "../Pintura";
import { getLabel } from "../../utils/lang-manager";
import s3Client, { bucketName } from "../../connections/s3_connection";
interface Props {
  name: string;
  textOnly?: boolean;
  edit?: boolean;
  imageUrl?: string;
  headerText: string;
  classLi?: boolean;
  handleUploadImage: (uploadImageUrl: string) => void;
  showPopupFun?: (val: boolean) => void;
}
export const ImageNewUploder = ({
  name,
  handleUploadImage,
  textOnly,
  edit,
  imageUrl,
  headerText,
  classLi,
  showPopupFun,
}: Props) => {
  const [uploadImageUrl, setUploadImageUrl] = useState("");
  const [avatarUrl, setNewAvatarUrl] = React.useState<any>(null);
  const [cropper, setCropper] = React.useState<any>();
  const [btnShow, setBtnShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [btnStatus, setBtnStatus] = useState(false);
  const handleUpload = async (files: any) => {
    setLoading(true);
    setBtnStatus(true);
    const uuid = require("uuid");
    const pushKey = `${uuid.v4()}`;
    const file = files;
    const putObjectCommand = new PutObjectCommand({
      Bucket: bucketName,
      Key: pushKey,
      Body: file,
      ContentType: file.type,
    });
    try {
      const data = await s3Client.send(putObjectCommand);
      const objectUrl = `https://${bucketName}.s3.amazonaws.com/${pushKey}`;
      setClickStatus(true);
      setUploadImageUrl(objectUrl);
      handleClose();
      setLoading(false);
    } catch (error) {
      setLoading(false);
      Notification.error({
        title: "Error uploading image",
        duration: 3000,
        content: undefined,
      });
    } finally {
    }
  };
  const [clickstatus, setClickStatus] = useState(false);

  const getNewAvatarUrl = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setNewAvatarUrl(URL.createObjectURL(e.target.files[0]));
      setBtnShow(true);
    } else {
      setBtnShow(false);
    }
  };
  const convertTypeJpgToPng = (file: any) => {
    const newFile = new File([file], "profile.png", { type: "image/png" });
    console.log("newFile", newFile);
    if (newFile) {
      handleUpload(newFile);
    }
  };

  useEffect(() => {
    if (clickstatus) {
      handleUploadImage(uploadImageUrl);
      setClickStatus(false);
      setBtnStatus(false);
    }
  }, [clickstatus]);
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    showPopupFun && showPopupFun(false);
  };
  const handleShow = () => {
    setShow(true), setNewAvatarUrl(null);
    setBtnShow(false);
    showPopupFun && showPopupFun(true);
  };
  const initialLanguage = "ENGLISH"
  return (
    <div>
      <Modal
        show={show}
        onHide={() => {
          handleClose();
          setBtnShow(false);
        }}
      >
        <div>
          <Modal.Header className="px-4 py-3 d-flex align-items-center">
            <Modal.Title id="contained-modal-title-vcenter ">
              <div className="">
                <h5 className="mb-0 font-150 font-bold ">
                  {loading
                    ? "Photo " + `${getLabel("isUploading", initialLanguage)}`
                    : `${getLabel("", initialLanguage)} ` +
                    headerText +
                    ` ${getLabel("", initialLanguage)}`}
                </h5>
              </div>
            </Modal.Title>
            <div
              className="pointer custom-close-button d-flex align-items-center justify-content-center"
              onClick={() => {
                handleClose();
                setBtnShow(false);
              }}
            >
              <Image src={cross} alt="world-icon" className="" />
            </div>
          </Modal.Header>
        </div>
        {!loading ? (
          <Modal.Body>
            <div>
              {edit ? (
                <></>
              ) : (
                <>
                  <input
                    type="file"
                    accept="image/png, image/jpeg, image/jpg"
                    className="form-control mb-3"
                    onChange={getNewAvatarUrl}
                  />
                </>
              )}

              {avatarUrl && (
                <>
                  <PinturaImageUploader
                    src={avatarUrl}
                    onVoidProcess={async (res) => {
                      console.log("resPin", res);
                      setNewAvatarUrl(res.dest);
                      convertTypeJpgToPng(res);
                    }}
                  />
                </>
              )}

              {imageUrl && (
                <PinturaImageUploader
                  src={imageUrl}
                  onVoidProcess={async (res) => {
                    console.log("resPin", res);
                    setNewAvatarUrl(res.dest);
                    convertTypeJpgToPng(res);
                  }}
                />
              )}
            </div>
          </Modal.Body>
        ) : (
          <Modal.Body className="d-flex justify-content-center align-items-center">
            <div className="image-container">
              <div
                style={{
                  borderRadius: "12px",
                  border: "2px",
                  minHeight: "410px",
                  minWidth: "410px",
                }}
              ></div>
            </div>
            <div className="">
              <Spin size={40} className="spinner" />
            </div>
          </Modal.Body>
        )}
      </Modal>
      {textOnly ? (
        <div>
          {classLi ? (
            <>
              <p
                className="pointer p-0 m-0 px-1 px-sm-4 font-90 font-normal"
                onClick={handleShow}
              >
                {name}
              </p>
            </>
          ) : (
            <>
              <li className="pointer p-0 m-0 px-1 px-sm-4" onClick={handleShow}>
                {name}
              </li>
            </>
          )}
        </div>
      ) : (
        <button
          type="button"
          className="btn btn-sm system-outline-primary font-bold px-1 px-sm-4"
          onClick={handleShow}
        >
          {name}
        </button>
      )}
    </div>
  );
};
