
import { createReducer } from '@reduxjs/toolkit';
import { getAllInterfaceLanguages } from './actions';
import { ResponseStatus } from '../../helpers/response_status';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const languageReducer = createReducer(initialState, (builder) => {

  builder
    .addCase(getAllInterfaceLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllInterfaceLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllInterfaceLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default languageReducer;
