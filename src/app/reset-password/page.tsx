"use client"
import React, { useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap';
import { getLabel } from '../../../utils/lang-manager';
import arrow from "../../../public/images/left-arrow.png"
import Image from 'next/image';
import Link from 'next/link';
import { IconCheckCircle } from '@arco-design/web-react/icon';
import validatePassword from '../../../utils/password-check';
const page = () => {
    const [loginData, setLoginData] = useState({ password: "", password_confirmation: "" } as unknown as any);
    const handleSubmit = () => {

    }
    const initialLanguage = "ENGLISH";
    const onChangeSelect = (name: string, value: any) => {
        setLoginData({
            ...loginData,
            [name]: value,
        });
    };
    const [showPassword, setShowPassword] = useState(false);
    const [showConPassword, setShowConPassword] = useState(false);
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };
    const toggleConPasswordVisibility = () => {
        setShowConPassword(!showConPassword);
    };
    const [isValid, setIsValid] = useState(false);
    const handleChange = (e: any) => {
        const value = e.target.value;
        setPassword(value);
        const a = validatePassword(value);
        console.log(a);

        if (a === true) {
            setIsValid(true);
        } else {
            setIsValid(false);
        }

    };
    const [password, setPassword] = useState('');
    // const [conformPassword, setConfirmPassword] = useState('');
    const [passwordsMatch, setPasswordsMatch] = useState(false);
    const conFormCheck = (e: any) => {
        const value = e.target.value;
        // setConfirmPassword(value);
        if (password !== value) {
            setPasswordsMatch(false);
        } else {
            setPasswordsMatch(true);
        }
    }
    return (
        <>
            <div className="d-flex justify-content-center w-100 mb-5 mt-5">
                <div className="col-9 col-sm-6 col-md-4 col-lg-4 col-xl-3 login-container">
                    <h5 className="d-flex mb-4 login-header">
                        blackbee
                    </h5>
                    <p className="d-flex login-header-title p-0 m-0">
                        Set new password
                    </p>
                    <p className="d-flex login-header-title_sub">
                        You new password must be different from previous used passwords.
                    </p>

                    <Form onSubmit={handleSubmit} className="">
                        <div className="d-flex justify-content-between align-items-center mb-1 mt-3">
                            <Form.Label className="m-0">
                                {getLabel("password", initialLanguage)}
                            </Form.Label>
                        </div>
                        <InputGroup className="password-container">
                            <div>
                                <Image
                                    onClick={togglePasswordVisibility}
                                    src={showPassword ? "/images/openeye.svg" : "/images/closeeye.svg"}
                                    alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                                    width={16}
                                    height={16}
                                    className="input-group-eye"
                                />
                            </div>
                            <div className="w-100">
                                <Form.Control
                                    id="formPassword"
                                    type={showPassword ? "text" : "password"}
                                    value={loginData?.password}
                                    onChange={(e) => {
                                        onChangeSelect("password", e.target.value);
                                        handleChange(e);
                                    }}
                                    placeholder="Password"
                                    className="form-control pe-5 inner-shadow-default p-2"
                                />{" "}
                            </div>

                            <div className='d-flex'>
                                {isValid ? <IconCheckCircle style={{ fontSize: 20, marginRight: 2, color: 'rgba(78, 203, 113, 1)' }} />
                                    :
                                    <IconCheckCircle style={{ fontSize: 20, marginRight: 2 }} />}

                                <p>Must be at least 12 characters, one cap letter, one number and a symbol.</p>
                            </div>
                        </InputGroup>
                        <div className="d-flex justify-content-between align-items-center mb-1 mt-3">
                            <Form.Label className="m-0">
                                {getLabel("confirmPassword", initialLanguage)}
                            </Form.Label>
                        </div>
                        <InputGroup className="password-container">
                            <div>
                                <Image
                                    onClick={toggleConPasswordVisibility}
                                    src={showConPassword ? "/images/openeye.svg" : "/images/closeeye.svg"}
                                    alt={showConPassword ? "eyeIcon icon" : "closeeye icon"}
                                    width={16}
                                    height={16}
                                    className="input-group-eye"
                                />
                            </div>
                            <div className="w-100">
                                <Form.Control
                                    id="password_confirmation"
                                    type={showConPassword ? "text" : "password"}
                                    value={loginData?.password_confirmation}
                                    onChange={(e) => {
                                        onChangeSelect("password_confirmation", e.target.value);
                                        conFormCheck(e);
                                    }}
                                    placeholder="Conform Password"
                                    className="form-control pe-5 inner-shadow-default p-2"
                                />{" "}
                            </div>
                            <div className='d-flex'>
                                {passwordsMatch ? <IconCheckCircle style={{ fontSize: 20, marginRight: 2, color: 'rgba(78, 203, 113, 1)' }} />
                                    :
                                    <IconCheckCircle style={{ fontSize: 20, marginRight: 2 }} />}

                                <p>Both password must match.</p>
                            </div>
                        </InputGroup>
                        <button
                            type="submit"
                            className="w-100 btn btn-dark mt-3 mb-4 p-2"
                        >
                            Continue
                        </button>
                        <div className="d-flex justify-content-center">
                            {/* <Error error={errors.email} /> */}
                        </div>
                        <div className="d-flex back-login">
                            <Link
                                href="/"
                                className="text-decoration-none text-dark font-bold font-100"
                            >
                                <Image src={arrow} alt='' width={20} height={20} style={{ padding: 3 }} />Back to log in
                            </Link>
                        </div>

                    </Form>
                </div>
            </div>
            <div className="bottom-container  d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <a href="#" className="a-tag-disable">
                    <span className="pointer">{getLabel("termsofService", initialLanguage)}</span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="mx-sm-3 mx-0 pointer">
                        {getLabel("support", initialLanguage)}
                    </span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="pointer">2022 {getLabel("companyName", initialLanguage)}</span>
                </a>
            </div>
        </>
    )
}

export default page