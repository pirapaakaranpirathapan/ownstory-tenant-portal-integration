
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createContactMethod, deleteContactMethod, getAllContactMethods, getOneContactMethod, updateContactMethod } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const contactMethodReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all contact method" action
  builder
    .addCase(getAllContactMethods.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllContactMethods.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllContactMethods.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one contact method" action
  builder
    .addCase(getOneContactMethod.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneContactMethod.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneContactMethod.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create contact method" action
  builder
    .addCase(createContactMethod.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createContactMethod.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createContactMethod.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update contact method" action
  builder
    .addCase(updateContactMethod.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateContactMethod.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateContactMethod.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete contactMethod" action
  builder
    .addCase(deleteContactMethod.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteContactMethod.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteContactMethod.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default contactMethodReducer;
