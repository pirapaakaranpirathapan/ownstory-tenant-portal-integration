import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const noticeTypesState = (state: RootState) => state.noticeTypeReducer

export const noticeTypesSelector = createSelector(
  noticeTypesState,
  state => state
)
