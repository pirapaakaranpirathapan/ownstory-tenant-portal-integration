
import { createAsyncThunk } from '@reduxjs/toolkit';
import { get } from '../../connections/fetch_wrapper';
import { BASE_ROUTE } from '../../config/constants';

//This  action function handles the asynchronous retrieval of partners & service provoiders, and depending on the response, either returns the data or an error message.
export const getAllServiceProvider = createAsyncThunk(
  'data/partners-serviceProviders',
  async (_, thunkAPI) => {
    try {
      const response = await get(`${BASE_ROUTE}/auth/data`);

      if (response.code == 200) {
        return response;
      } else {
        thunkAPI.rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.message);
    }
  }

);