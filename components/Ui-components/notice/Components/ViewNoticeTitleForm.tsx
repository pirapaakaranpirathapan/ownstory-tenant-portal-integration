import React, { useEffect, useState } from "react";

import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import { getOneNoticeTitle, noticeTitleSelector } from "../../../../redux/notice";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";

interface ViewEditNoticeTitle {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit:(name: string, slug: string,description: string, language_id: string,typeId: string, client_Id: string,) => void;
    response: any;
}
const ViewEditRelationship = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
    response
}: ViewEditNoticeTitle) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        description:"",
        language_id:"",
        typeId:"",
        client_Id:""
    });
    // error handle
    const { dataOne: getOneNoticeTitleData, error: apiError } = useAppSelector(noticeTitleSelector);
    const errors = useApiErrorHandling(apiError, response);
    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                handleEdit(formData.name, formData.slug, formData.description, formData.language_id, formData.typeId, formData.client_Id);
                setClickStatus(false);
            };
        }
    }, [clickStatus, handleEdit,handleChange]);

    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        dispatch(
            getOneNoticeTitle({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                //id:number= 1,
                id: viewId,
            })
        ).then((res: any) => {
            if (res.payload.code === 200) {
                setSkelton(false);
            }
        });
    };

    useEffect(() => {
        setFormData({
            name: getOneNoticeTitleData[0]?.name,
            slug: getOneNoticeTitleData[0]?.slug,
            description: getOneNoticeTitleData[0]?.description,
            language_id: getOneNoticeTitleData[0]?.language_id,
            typeId: getOneNoticeTitleData[0]?.typeId,
            client_Id: getOneNoticeTitleData[0]?.client_Id,

        });
    }, [skelton]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                        <div className="font-semibold">Name</div>
                            {/* Name<span className="font-semibold">*</span> */}
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="slug"
                                defaultValue={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Description</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="description"
                                defaultValue={formData.description}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Language</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="language_id"
                                defaultValue={formData.language_id}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="typeId"
                                defaultValue={formData.typeId}
                                onChange={handleChange}
                            />
                        )}
                    </div>

                </div>
            </div>
        </>
    );
};

export default ViewEditRelationship;
