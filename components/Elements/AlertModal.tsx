import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import cross from "../../public/images/cross.svg";

type Props = {
  show: boolean;
  onClose: Function;
  size?: "sm" | "lg" | "xl";
  position?:
  | "centered"
  | "full-screen"
  | "left"
  | "right"
  | "bottom"
  | "top"
  | "left-bottom"
  | "right-bottom"
  | "left-top"
  | "right-top"
  | "center";
  title?: string;
  description?: string;
  deleteLabelText?: string;
  deleteLabel?: any;
  showCloseButton?: boolean;
  footerButtonText?: any;
  footer?: any;
  children?: any;
  isSmallModal?: boolean;
  isNotiesModal?: boolean;
  notiesId?: string;
};

const AlertModal: React.FC<Props> = ({
  show,
  onClose,
  size,
  position,
  title,
  description,
  deleteLabelText,
  children,
  showCloseButton,
  footer,
  footerButtonText,
  deleteLabel,
  isSmallModal,
  isNotiesModal,
  notiesId,
}) => {
  const [showModal, setShowModal] = useState(show);
  const [positionModal, setPositionModal] = useState(position);

  const handleClose = () => {
    onClose();
    setShowModal(false);
  };

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  useEffect(() => {
    setPositionModal(position);
  }, [position]);

  return (
    <Modal
      dialogClassName={`modal-dialog-${position}`}
      show={showModal}
      size={size}
      className={isSmallModal == true ? "small-modal" : ""}
      onHide={handleClose}
      fullscreen={positionModal == "full-screen" ? true : undefined}
      aria-labelledby="contained-modal-title-vcenter"
    >
      <Modal.Body className="p-0 pb-4 pt-2">
        {title != undefined && (
          <Modal.Title
            id="contained-modal-title-vcenter"
            className={`px-4 d-flex align-items-center justify-content-between ${isSmallModal == true ? "mb-3" : ""
              }`}
          >
            <div className="">
              <h1 className="mb-0 font-bold font-150">{title}</h1>
              {isNotiesModal == true ? (
                <>
                  <p className="p-0 m-0 font-normal font-100 notices-id">
                    Notice ID {notiesId}
                  </p>
                </>
              ) : (
                <></>
              )}
              {description && (
                <p className="font-90 text-secondary m-0 p-0">{description}</p>
              )}
            </div>
            {isSmallModal == true ? (
              ""
            ) : (
              <div
                className="pointer custom-close-button d-flex align-items-center justify-content-center"
                onClick={handleClose}
              >
                <Image src={cross} alt="world-icon" className="" />
              </div>
            )}
          </Modal.Title>
        )}
        {isSmallModal == true ? "" : <hr className="p-0 " />}
        <div className="px-4 py-0">{children}</div>
      </Modal.Body>
      {footer !== undefined && (
        <Modal.Footer className="my-modal-footer px-4 py-3">
          <div>{footer}</div>
          <div className="text-danger pointer">{deleteLabel}</div>
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default AlertModal;
