import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { languagesSelector } from "../../../../redux/languages";
import Error from "../../../Errors/Error";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";

interface AddLanguages {
    actionAddBtnRef: any;
    handleCreate: (code: string, name: string, native_name: string) => void;
    response: any;
}
const CreateLanguages = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddLanguages) => {
      // error handle
    const { error: apiError } = useAppSelector(languagesSelector);
    const errors = useApiErrorHandling(apiError, response);
    const [formData, setFormData] = useState({
        code: "",
        name: "",
        native_name: "",
  });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.code, formData.name, formData.native_name);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);



    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Code <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="code"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.code}
                            onChange={handleChange}
                        />
                        <Error error={errors.code} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData?.name}
                            onChange={handleChange}
                        />
                        <Error error={errors.name} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Native name</div>
                        <input
                            type="text"
                            name="native_name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.native_name}
                            onChange={handleChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateLanguages;
