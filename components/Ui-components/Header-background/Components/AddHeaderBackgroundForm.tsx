import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { getLabel } from "../../../../utils/lang-manager";
import { Upload } from "@arco-design/web-react";
import FileUploder from "../../../ImageUploder/FileUploder";
import { headerBackgroundSelector } from "../../../../redux/header-backgrounds";

interface AddBorders {
    actionAddBtnRef: any;
    handleCreate: (state_id:string, name: string, image_url: string, color: string) => void;
    response: any
}
const CreateHeaderBackground = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddBorders) => {
    const { error: apiError } = useAppSelector(headerBackgroundSelector);
    const [formData, setFormData] = useState({
        state_id: "",
        name: "",
        image_url: "",
        color: "",
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.state_id, formData.name, formData.image_url, formData.color);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);

    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            state_id: formData.state_id,
            name: formData.name,
            image_url: url,
            color: formData.color
        });
    }

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("stateId", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="state_id"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.state_id}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.state_id[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("headerBackgroundName", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.name[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("imageUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="image_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.image_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.image_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("color", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="color"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.color}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.color[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Header Background
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateHeaderBackground;
