import React from 'react'

const Error = ({ error }: { error: string }) => {
    return (
        <div>
            <div className="row  align-items-start m-0 text-danger">
                {error}
            </div>
        </div>
    )
}

export default Error