import React from "react";
import ShimmerEffect from "./ShimmerEffect";

const TableHeaderDiscriptionSkelton = () => {
    return (
        <div className="row">
            <td className="col-12">
                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                    <div className="p-0 m-0 text-truncate">
                        <ShimmerEffect width="150px" height="20px" />
                    </div>
                </div>
            </td>
            <td className="col-12">
                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                    <div className="p-0 m-0 text-truncate">
                        <ShimmerEffect width="450px" height="20px" />
                    </div>
                </div>
            </td>
        </div>
    );
};

export default TableHeaderDiscriptionSkelton;
