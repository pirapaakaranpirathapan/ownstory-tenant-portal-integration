import axios from "axios";
import { getCookie, deleteCookie, setCookie } from 'cookies-next'
import { getToken } from "../helpers/helpers";


export const axiosInstance = axios.create({
        baseURL: process.env.PROXY_PREFIX,
        headers: {
                "Content-type": "application/json",
                "Accept": "application/json",
        },
        params: {
                lang: 'en'
        },

});

axiosInstance.interceptors.request.use(
        async (config) => {
                const token = await getToken();

                if (token && token != "undefined") {


                        config.headers.Authorization = `Bearer ${token}`
                } else {
                        config.headers.Authorization = String(getCookie('authHeader'))
                }
                return config;
        },
        error => Promise.reject(error)
);




axiosInstance.interceptors.response.use(undefined, async (err) => {
        const status = err.response ? err.response.status : null;
        if (status === 401) {
                deleteCookie('token');
                if (window.location.pathname !== '/') {
                        window.location.href = '/';
                }
                // try {
                //     const response = await get(`${BASE_ROUTE}/auth/refresh`);
                //     console.log("responseeee",response);

                //     deleteCookie('token');
                //     if(response.success){
                //             setCookie('token',response.data?.access_token)
                //             return axiosInstance(err.response.config);
                //     }else {
                //             if (window.location.pathname !== '/') {
                //                     window.location.href = '/';
                //                 }
                //     }

                // } catch (response1) {

                // }
        }
        // if (status === 401) {
        //     try {
        //         const response = await get(`${BASE_ROUTE}/auth/refresh`);
        //         console.log("responseeee",response);

        //         deleteCookie('token');
        //         if(response.success){
        //                 setCookie('token',response.data?.access_token)
        //                 return axiosInstance(err.response.config);
        //         }else {
        //                 if (window.location.pathname !== '/') {
        //                         window.location.href = '/';
        //                     }
        //         }

        //     } catch (response1) {

        //     }
        // }

        return Promise.reject(err);
});

// axiosInstance.interceptors.response.use(undefined, err => {
//         const status = err.response ? err.response.status : null;
//         if (status === 401) {

//                 //  return axiosInstance(err.response.config);
//         }
//         return Promise.reject(err);
// });




export const axiosInstanceFormData = axios.create({
        baseURL: process.env.PROXY_PREFIX,
        headers: {
                "Content-type": "multipart/form-data"
        },
        params: {
                lang: 'en'
        },
});

axiosInstanceFormData.interceptors.request.use(
        async (config) => {
                const token = await getToken();
                console.log("");
                if (token) {
                        config.headers.Authorization = String(getCookie('authHeader'))
                }
                return config;
        },
        error => Promise.reject(error)
);

axiosInstanceFormData.interceptors.response.use(undefined, err => {
        const status = err.response ? err.response.status : null;
        if (status === 401) {
                // logout();
        }
        return Promise.reject(err);
});


export const axiosInstanceServer = axios.create({
        baseURL: process.env.BASE_URL,
        headers: {
                "Content-type": "application/json",
                "Accept": "application/json",
        },
        params: {
                lang: 'en'
        },

});

axiosInstanceServer.interceptors.request.use(
        async (config) => {
                const token = await getToken();
                console.log("");


                if (token) {
                        config.headers.Authorization = String(getCookie('authHeader'))
                }
                return config;
        },
        error => Promise.reject(error)
);

axiosInstanceServer.interceptors.response.use(undefined, err => {
        const status = err.response ? err.response.status : null;
        if (status === 401) {
                // logout();
        }
        return Promise.reject(err);
});