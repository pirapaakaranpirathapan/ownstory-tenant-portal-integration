import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { pageBackgroundSelector } from "../../../../redux/PageBackground";
import { getLabel } from "../../../../utils/lang-manager";
import FileUploder from "../../../ImageUploder/FileUploder";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";
import Error from "../../../Errors/Error";
interface AddPageBackground {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string,thumb_url: String ,description: String) => void;
    response :any
}
const CreatePageBackground = ({
    handleCreate,
    actionAddBtnRef,
    response,
}: AddPageBackground) => {
    const { error: apiError } = useAppSelector(pageBackgroundSelector);
    const errors = useApiErrorHandling(apiError, response);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        thumb_url: "",
        description: "",
        // is_premium: string,
        // has_header_background: string,
        // has_page_background: string,
        // page_type_id: string,
        // clip_arts: string,
        // borders: string,
        // frames: string,
        // languages: string,
        // tags: string,
        // template_groups: string,
        // header_backgrounds: string,
        // page_backgrounds: string
    });
    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }
        if (clickStatus) {
            handleCreate(formData.name, formData.slug,formData.thumb_url ,formData.description);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);
    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            name: formData.name,
            thumb_url:url,
            slug: formData.slug,
            description:formData.description,
        });
    }

    return (
        <>
             <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("name", initialLanguage)}
                            <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                      <Error error={errors.name}/>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("slug", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                        <Error error={errors.slug} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("description", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="description"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.description}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Page Background
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
}


export default CreatePageBackground;
