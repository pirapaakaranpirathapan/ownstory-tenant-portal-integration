"use client";
import Image from "next/image";
import Link from "next/link";
import updown from "../../../public/images/updown.svg";
import backArrow from "../../../public/images/back-arrow.png";
import { useCallback, useEffect, useRef, useState } from "react";

import { getCookie, setCookie } from "cookies-next";
import { getLabel } from "../../../utils/lang-manager";
import { useAppDispatch, useAppSelector } from "../../../redux/store/hooks";
import { serviceProviderSelector } from "../../../redux/service-providers";
import { usePathname, useRouter } from "next/navigation";
type Props = {
  showBar: boolean;
  setIsLoading: any;
  initialLanguage: string;
  handleLinkClick?: () => void;
};
interface ServiceProvider {
  uuid: string;
}
interface Partner {
  uuid: string;
  service_providers: ServiceProvider[];
}

const AdminConsoleSidebar: React.FC<Props> = ({
  showBar,
  setIsLoading,
  initialLanguage,
  handleLinkClick,
}) => {
  const [visibleServiceProviders, setVisibleServiceProviders] = useState(15);
  const dropdownRef = useRef<HTMLDivElement | null>(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [activeData, setActiveData] = useState<Partner[]>([]);
  const [activeServiceProvider, setActiveServiceProvider] = useState<{
    name: string;
    id: string;
  }>({ name: "", id: "" });
  const [cookiesSet, setCookiesSet] = useState(false);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const pathname = usePathname();

  const dispatch = useAppDispatch();
  const popupRef = useRef<HTMLDivElement>(null);
  const { data: getData } = useAppSelector(serviceProviderSelector);

  useEffect(() => {
    if (Array.isArray(getData) && getData.length > 0) {
      const activePartnerIndex = getData.findIndex((partner) =>
        partner.service_providers.some((provider: { uuid: string }) =>
          isActiveId(provider.uuid)
        )
      );
      if (activePartnerIndex !== -1) {
        const activePartner = getData[activePartnerIndex];
        const partnersWithoutActive = [
          ...getData.slice(0, activePartnerIndex),
          ...getData.slice(activePartnerIndex + 1),
        ];
        const sortedServiceProviders = activePartner.service_providers
          .slice()
          .sort((a: { uuid: string }, b: { uuid: string }) => {
            if (isActiveId(a.uuid)) return -1;
            if (isActiveId(b.uuid)) return 1;
            return 0;
          });
        const sortedActivePartner = {
          ...activePartner,
          service_providers: sortedServiceProviders,
        };
        const sortedData = [sortedActivePartner, ...partnersWithoutActive];
        setActiveData(sortedData);
      } else {
        const sortedData = getData.slice().sort((a, b) => {
          const hasActiveServiceProviderA = a.service_providers.some(
            (provider: { uuid: string }) => isActiveId(provider.uuid)
          );
          const hasActiveServiceProviderB = b.service_providers.some(
            (provider: { uuid: string }) => isActiveId(provider.uuid)
          );
          if (hasActiveServiceProviderA) return -1;
          if (hasActiveServiceProviderB) return 1;
          return 0;
        });
        setActiveData(sortedData);
      }
    }
  }, [getData, activeServiceProvider]);

  useEffect(() => {
    if (isDropdownOpen) {
      setSearchTerm("");
    }
  }, [isDropdownOpen]);

  useEffect(() => {
    const activeServiceProviderFromCookie = getCookie(
      "serviceprovidername"
    ) as string;
    const activeServiceProviderIdFromCookie = getCookie(
      "activeserviceprovider"
    ) as string;
    if (activeServiceProviderFromCookie) {
      setActiveServiceProvider({
        name: activeServiceProviderFromCookie,
        id: activeServiceProviderIdFromCookie,
      });
    }
  }, []);

  useEffect(() => {
    const lastAccessedPartnerId = getCookie("activepartner") as string;
    const lastAccessedServiceProviderId = getCookie(
      "activeserviceprovider"
    ) as string;
    if (lastAccessedPartnerId && lastAccessedServiceProviderId) {
      setActivePartnerFromCookies(
        lastAccessedPartnerId,
        lastAccessedServiceProviderId
      );
    } else if (Array.isArray(getData) && getData.length > 0) {
      const defaultPartner = getData[0];
      const defaultServiceProvider = defaultPartner.service_providers[0];
      const partnerId = defaultPartner.uuid;
      const serviceProviderId = defaultServiceProvider.uuid;
      const serviceProviderName = defaultServiceProvider.name;
      setCookie("serviceprovidername", serviceProviderName);
      setCookie("activepartner", partnerId);
      setCookie("activeserviceprovider", serviceProviderId);
      setActiveServiceProvider({
        name: serviceProviderName,
        id: serviceProviderId,
      });
    }
  }, [getData]);

  function setActivePartnerFromCookies(
    partnerId: string,
    serviceProviderId: string
  ) {
    const activePartner =
      Array.isArray(getData) &&
      getData.find((partner: { uuid: string }) => partner.uuid === partnerId);
    if (activePartner) {
      const activeServiceProvider = activePartner.service_providers.find(
        (serviceProvider: { uuid: string }) =>
          serviceProvider.uuid === serviceProviderId
      );
      if (activeServiceProvider) {
        setActiveServiceProvider({
          name: activeServiceProvider.name,
          id: serviceProviderId,
        });
      }
    }
  }

  const HandleUpdateActive = (
    partnerId: any,
    serviceProviderId: any,
    serviceProviderName: any
  ) => {
    setCookie("activepartner", partnerId);
    setCookie("activeserviceprovider", serviceProviderId);
    setCookie("serviceprovidername", serviceProviderName);
    setActiveServiceProvider({
      name: serviceProviderName,
      id: serviceProviderId,
    });
    window.location.reload();
    window.location.href = "/dashboard";
    // setIsLoading(false);
  };

  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (
        popupRef.current &&
        !popupRef.current.contains(event.target as Node)
      ) {
        setIsDropdownOpen(false);
      }
    }
    function handlePopstate() {
      setIsDropdownOpen(false);
    }
    document.addEventListener("mousedown", handleClickOutside);
    window.addEventListener("popstate", handlePopstate);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
      window.removeEventListener("popstate", handlePopstate);
    };
  }, [popupRef]);

  useEffect(() => {
    const handleScroll = () => {
      if (dropdownRef.current) {
        const { scrollTop, scrollHeight, clientHeight } = dropdownRef.current;
        if (scrollTop + clientHeight >= scrollHeight - 10) {
          loadMoreServiceProviders();
        }
      }
    };
    if (dropdownRef.current) {
      dropdownRef.current.addEventListener("scroll", handleScroll);
    }
    return () => {
      if (dropdownRef.current) {
        dropdownRef.current.removeEventListener("scroll", handleScroll);
      }
    };
  }, [dropdownRef, isDropdownOpen]);

  function toggleDropdown() {
    setIsDropdownOpen(!isDropdownOpen);
  }

  const isActiveLink = (href: string) => {
    if (pathname.startsWith(`${href}/`)) {
      return true;
    }
    return false;
  };

  const isActiveId = (serviceProviderUuid: string) => {
    return activeServiceProvider.id === serviceProviderUuid;
  };

  const handleSearch = (event: any) => {
    const searchValue = event.target.value;
    setSearchTerm(searchValue);
    const filteredData = getData?.filter((item: any) => {
      const itemName = item.name.toLowerCase();
      return itemName.includes(searchValue.toLowerCase());
    });
    setFilteredData(filteredData);
  };

  const loadMoreServiceProviders = () => {
    setVisibleServiceProviders(
      (prevVisibleServiceProviders) => prevVisibleServiceProviders + 15
    );
  };

  return (
    <div
      className={`sidebar-default mini-scrollbar  shadow-sm ${showBar ? "open" : "closed"
        }`}
    >
      <div className="admin-console-text-container">
        <h5 className="text-center admin-console-text">Admin Console</h5>
      </div>

      <ul className="navbar-nav pt-3 overflow-auto">
        <Link
          href="/dashboard"
          className="text-decoration-none text-dark lh-lg font-normal  "
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${pathname === "/dashboard" ? "font-bold" : "link"
              }`}
          >
            {getLabel("dashboard", initialLanguage)}
          </li>
        </Link>

        <Link
          href="/global-settings"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/global-settings") ? "font-bold" : "link"
              }`}
          >
            {getLabel("globalSettings", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/sites?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/sites") ? "font-bold" : "link"
              }`}
          >
            {getLabel("sites", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/users?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/users") ? "font-bold" : "link"
              }`}
          >
            {getLabel("users", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/userRoles?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/user-roles") ? "font-bold" : "link"
              }`}
          >
            {getLabel("userRoles", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/location?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/location") ? "font-bold" : "link"
              }`}
          >
            {getLabel("location", initialLanguage)}
          </li>
        </Link>
        <div className=" disabled py-1 lh-lg pt-4 mx-2 px-3 rounded system-icon-secondary font-80 font-normal">
          {getLabel("masterData", initialLanguage)}
        </div>
        <Link
          href="/general"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${pathname === "/general" ? "font-bold" : "link"
              }`}
          >
            {getLabel("General", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/products"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${pathname === "/products" ? "font-bold" : "link"
              }`}
          >
            {getLabel("products", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/themes-templates"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${pathname === "/themes-templates" ? "font-bold" : "link"
              }`}
          >
            {getLabel("ThemesTemplates", initialLanguage)}
          </li>
        </Link>
      </ul>
      <div className=" position-absolute bottom-0 logo-container d-flex border-top d-flex  align-items-center w-100">
        <div className=" system-secondary-3 p-2 mx-3 rounded d-flex align-items-center">
          <Image src={backArrow} alt="back-arrow" width={20} className="pointer" />
        </div>
        <div className="back-arrow-text">Go to Tenant Portal</div>
      </div>
    </div>
  );
};

export default AdminConsoleSidebar;
