
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createNoticeTitle, deleteNoticeTitle, getAllNoticeTitle, getOneNoticeTitle, updateNoticeTitle } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const noticeTitleReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all notice Title" action
  builder
    .addCase(getAllNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one notice Title" action
  builder
    .addCase(getOneNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create notice Title" action
  builder
    .addCase(createNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update notice Title" action
  builder
    .addCase(updateNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete notice Title" action
  builder
    .addCase(deleteNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default noticeTitleReducer;
