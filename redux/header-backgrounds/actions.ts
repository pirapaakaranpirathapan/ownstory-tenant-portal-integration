import { createAsyncThunk } from '@reduxjs/toolkit';
import { deleteMethod, get, patch, post, put } from '../../connections/fetch_wrapper';
import { IUser } from '../../interface/IUser';
import { BASE_ROUTE } from '../../config/constants';

export const getAllHeaderBackgrounds = createAsyncThunk('header-backgrounds/get-all', async (
  {
    active_partner,
    active_service_provider,
    page,
  }: {
    active_partner: string;
    active_service_provider: string;
    page: number;
  }, { rejectWithValue }) => {
  try {
    const response = await get(`${BASE_ROUTE}/master-data/header-backgrounds?page=${page}`);
    if (response.code == 200) {
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});

//Single View =====================================================================================================================

export const getOneHeaderBackground = createAsyncThunk(
  "header-backgrounds/get-one",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/master-data/header-backgrounds/${id}`
      );

      if (response.code == 200) {
        // console.log("response", response);
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//Create========================================================================================================
// export const createHeaderBackground = createAsyncThunk('header-backgrounds/create', async ({ data }: { data: object | unknown }, { rejectWithValue }) => {
export const createHeaderBackground = createAsyncThunk(
  "header-backgrounds/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      createdData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      createdData: {
        state_id: string;
        name: string;
        image_url: string;
        color: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/master-data/header-backgrounds`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Update 
export const updateHeaderBackground = createAsyncThunk(
  "header-backgrounds/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
      updatedData: {
        state_id: number;
        name: string;
        image_url: string;
        color: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/master-data/header-backgrounds/${id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Delete   ==========================================================================================
export const deleteHeaderBackground = createAsyncThunk(
  "header-backgrounds/deleted",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/master-data/header-backgrounds/${id}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);