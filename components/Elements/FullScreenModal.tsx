import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import cross from "../../public/images/cross.svg";

type Props = {
  show: boolean;
  onClose: Function;
  size?: "sm" | "lg" | "xl";
  position?:
  | "centered"
  | "full-screen"
  | "left"
  | "right"
  | "bottom"
  | "top"
  | "left-bottom"
  | "right-bottom"
  | "left-top"
  | "right-top"
  | "center";
  title?: string;
  description?: string;
  footer?: any;
  children?: any;
  footerhide?: boolean;
  cusSize?: any;
};

const FullScreenModal: React.FC<Props> = ({
  show,
  onClose,
  size,
  position,
  title,
  description,
  children,
  footer,
  footerhide,
  cusSize,
}) => {
  const [showModal, setShowModal] = useState(show);

  const [positionModal, setPositionModal] = useState(position);

  const handleClose = () => {
    onClose();
    setShowModal(false);
  };

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  useEffect(() => {
    setPositionModal(position);
  }, [position]);

  return (
    <Modal
      dialogClassName={`modal-dialog-${position} ${cusSize} `}
      show={showModal}
      onHide={handleClose}
      size={size}
      fullscreen={positionModal == "full-screen" ? true : undefined}
      aria-labelledby="contained-modal-title-vcenter"
    >
      <Modal.Header className="px-4 py-3 d-flex align-items-center justify-content-between">
        {title != undefined && (
          <Modal.Title id="contained-modal-title-vcenter ">
            <div className="">
              <h5 className="mb-0 font-150 font-bold ">{title}</h5>
              {description && (
                <p className="font-100 font-normal text-secondary mb-0">
                  {description}
                </p>
              )}
            </div>
          </Modal.Title>
        )}

        <div
          className="pointer custom-close-button d-flex align-items-center justify-content-center"
          onClick={handleClose}
        >
          <Image src={cross} alt="world-icon" className="" />
        </div>
      </Modal.Header>
      <Modal.Body className="px-4">{children}</Modal.Body>
      {footer !== undefined && !footerhide && (
        <Modal.Footer className="my-modal-footer px-4 pb-4 p-0">
          <div>{footer}</div>
        </Modal.Footer>
      )}
    </Modal>
  );
};
export default FullScreenModal;
