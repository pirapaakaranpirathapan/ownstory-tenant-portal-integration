import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../../redux/store/hooks";
import { noticeTypesState } from "../../../../../redux/notice-types";

interface AddNoticeType {
  actionAddBtnRef: any;
  handleCreate: (
    state_id: number,
    name: string,
    slug: string,
    title: string,
    description: string
  ) => void;
}
const CreateNoticeType = ({ handleCreate, actionAddBtnRef }: AddNoticeType) => {
  const { error: apiError } = useAppSelector(noticeTypesState);
  const [formData, setFormData] = useState({
    state_id: 1,
    name: "",
    slug: "",
    title: "",
    description: "",
  });

  // Add handle
  const [clickStatus, setClickStatus] = useState(false);
  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // submit data
  useEffect(() => {
    if (actionAddBtnRef.current) {
      actionAddBtnRef.current.onclick = () => {
        setClickStatus(true);
      };
    }

    if (clickStatus) {
      handleCreate(
        formData.state_id,
        formData.name,
        formData.slug,
        formData.title,
        formData.description
      );
      setClickStatus(false);
    }
  }, [clickStatus, handleCreate]);

  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">State Id</div>
            <input
              type="text"
              name="state_id"
              className="form-control border border-2 p-2 system-control "
              value={formData.state_id}
              onChange={handleChange}
            />
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              Name <span className="font-semibold">*</span>
            </div>
            <input
              type="text"
              name="name"
              className="form-control border border-2 p-2 system-control "
              value={formData.name}
              onChange={handleChange}
            />
            <div className="row  align-items-start m-0 text-danger">
              {apiError?.error?.name[0]}
            </div>
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Slug</div>
            <input
              type="text"
              name="slug"
              className="form-control border border-2 p-2 system-control "
              value={formData.slug}
              onChange={handleChange}
            />
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Title</div>
            <input
              type="text"
              name="title"
              className="form-control border border-2 p-2 system-control "
              value={formData.title}
              onChange={handleChange}
            />
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Description</div>
            <input
              type="text"
              name="description"
              className="form-control border border-2 p-2 system-control "
              value={formData.description}
              onChange={handleChange}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateNoticeType;
