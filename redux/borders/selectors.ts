import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const borderState = (state: RootState) => state.borderReducer

export const borderSelector = createSelector(
    borderState,
  state => state
)
