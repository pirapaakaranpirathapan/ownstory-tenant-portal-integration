import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const eventTypesState = (state: RootState) => state.eventTypeReducer

export const eventTypeSelector = createSelector(
  eventTypesState,
  state => state
)
