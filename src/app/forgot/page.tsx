import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import arrow from "../../../public/images/left-arrow.png"
import { getLabel } from '../../../utils/lang-manager'
const page = () => {
    const initialLanguage = "ENGLISH";
    return (
        <div>
            <div className="d-flex justify-content-center w-100 mb-5 mt-5">
                <div className="col-9 col-sm-6 col-md-4 col-lg-4 col-xl-3 login-container">
                    <h5 className="d-flex mb-4 login-header">
                        {/* {getLabel("signInToBlackBeesPartner", initialLanguage)} */}
                        blackbee
                    </h5>
                    <p className="d-flex login-header-title-check-email">
                        {/* {getLabel("signInToBlackBeesPartner", initialLanguage)} */}
                        Check your email
                    </p>
                    <p className=" check-email-sub">
                        We have sent a password recover instruction to your email. Did not received the email? Check your spam filter, or <Link href="/forgot-password"
                            className="text-decoration-none text-dark font-bold font-100" ><span className="try-another-email">try another email address</span></Link>
                    </p>
                    <div className="d-flex back-login">
                        <Link
                            href="/"
                            className="text-decoration-none text-dark font-bold font-100"
                        >
                            <Image src={arrow} alt='' width={20} height={20} style={{ padding: 3 }} />Back to log in
                        </Link> </div>
                </div>
            </div>
            <div className="bottom-container  d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <a href="#" className="a-tag-disable">
                    <span className="pointer">{getLabel("termsofService", initialLanguage)}</span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="mx-sm-3 mx-0 pointer">
                        {getLabel("support", initialLanguage)}
                    </span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="pointer">2022 {getLabel("companyName", initialLanguage)}</span>
                </a>
            </div>
        </div>
    )
}

export default page