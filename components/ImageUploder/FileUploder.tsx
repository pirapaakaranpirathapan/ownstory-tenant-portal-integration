import { Upload, Notification } from '@arco-design/web-react';
import React, { useEffect, useState } from 'react'
import s3Client, { bucketName } from '../../connections/s3_connection';
import { PutObjectCommand } from '@aws-sdk/client-s3';
interface Props {
    handleUploadImage: (uploadImageUrl: string) => void;
}
const FileUploder = ({ handleUploadImage }: Props) => {
    const [uploadImageUrl, setUploadImageUrl] = useState("");
    const [clickstatus, setClickStatus] = useState(false);

    const handleUpload = async (files: any) => {
        const uuid = require("uuid");
        const pushKey = `${uuid.v4()}`;
        const file = files;
        const putObjectCommand = new PutObjectCommand({
            Bucket: bucketName,
            Key: pushKey,
            Body: file,
            ContentType: file.type,
        });
        try {
            const data = await s3Client.send(putObjectCommand);
            const objectUrl = `https://${bucketName}.s3.amazonaws.com/${pushKey}`;
            setClickStatus(true);
            setUploadImageUrl(objectUrl);
        } catch (error) {
            Notification.error({
                title: "Error uploading image",
                duration: 3000,
                content: undefined,
            });
        } finally {
        }
    };
    useEffect(() => {
        if (clickstatus) {
            handleUploadImage(uploadImageUrl);
            setClickStatus(false);
        }
    }, [clickstatus]);

    return (
        <div>
            <Upload
                drag
                accept='image/*'
                action='/'
                onChange={(e) => {
                    handleUpload(e[0].originFile);
                }}
                tip='Only pictures can be uploaded'
            />
        </div>
    )
}

export default FileUploder