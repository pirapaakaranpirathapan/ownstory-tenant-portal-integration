// import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { getMe } from './actions';
import { ResponseStatus } from '../../helpers/response_status';
// import { getCookie, removeCookies, setCookie } from 'cookies-next';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: {} as unknown as any,
};

export const meReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getMe.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getMe.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      // const defaultLanguage= payload?.data?.default_language?.name
      // setCookie('language', defaultLanguage);
    })
    .addCase(getMe.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default meReducer;
