
import { getCookie } from 'cookies-next';
import { decryptToken } from '../utils/encryptionAndDecryptionUtils';

export async function getToken(req?: any) {
  try {
    const tokenCookie = req ? getCookie('token', { req }) : getCookie('token');
    const decryptedToken = tokenCookie ? decryptToken(String(tokenCookie)) : '';
    // console.log("decryptedToken", decryptedToken);
    return decryptedToken || '';
  } catch (error) {
    return '';
  }
}


