"use client"
import React, { useEffect, useState } from 'react'
import { Provider } from 'react-redux'
import { store } from '../../redux/store/store'
import 'bootstrap/dist/css/bootstrap.min.css';
import DefaultLayout from '../../components/Layout/Layout/DefalutLayout';
import { SSRProvider } from "react-bootstrap";
import AdminConsole from '../../components/Layout/Layout/AdminConsole';
import { ConfigProvider } from '@arco-design/web-react';
import enUS from '../../config/dayjs/locale/en-US';
import { getCookie } from 'cookies-next';
const Main = ({ children }: { children: React.ReactNode }) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        const token = getCookie('token');
        if (token) {
            setIsLoggedIn(true);
        } else {
            setIsLoggedIn(false);
        }
    }, []);
    return (
        <>
            <Provider store={store}>
                <SSRProvider>
                    <ConfigProvider locale={enUS}>
                        <AdminConsole>
                            {children}
                        </AdminConsole>
                    </ConfigProvider>
                </SSRProvider>
            </Provider>
        </>
    )
}

export default Main;
