import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const contactMethodState = (state: RootState) => state.contactMethodReducer

export const contactMethodSelector = createSelector(
  contactMethodState,
  state => state
)
