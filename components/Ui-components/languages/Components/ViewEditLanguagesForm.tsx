import React, { useEffect, useState } from "react";
import { getOneLanguages, languagesSelector } from "../../../../redux/languages";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";

interface ViewEditLanguages {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (code: string, name: string, native_name: string) => void;
}
const ViewEditLanguages = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
}: ViewEditLanguages) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        code: "",
        name: "",
        native_name: "",
    });

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleEdit(formData.code, formData.name, formData.native_name);
            setClickStatus(false);
        }
    }, [clickStatus, handleEdit]);

    const { dataOne: oneLanguagesData } =
        useAppSelector(languagesSelector);
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        dispatch(
            getOneLanguages({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                uuid: viewId,
            })
        ).then((res: any) => {
            if (res.payload.code === 200) {
                setSkelton(false);
            }
        });
    };

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Code<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Code"
                                name="code"
                                defaultValue={oneLanguagesData[0]?.code}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={oneLanguagesData[0]?.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Native name</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="native_name"
                                name="native_name"
                                defaultValue={oneLanguagesData[0]?.native_name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default ViewEditLanguages;
