import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const frameState = (state: RootState) => state.framesReducer

export const frameSelector = createSelector(
  frameState,
  state => state
)
