import React from "react";
import Image from "next/image";
import ShimmerEffect from "./ShimmerEffect";

const TemplateSkelton  = ({ loopTimes }: any) => {
    return ( 
        <>
        <div className="d-none d-sm-block skelton-disable" style={{ cursor: "default", pointerEvents: "none" }}>
                <table className="table mt-3 ">
                    <tbody className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start no-margin-bottom">

                        {Array.from({ length: loopTimes }).map((_, index) => {
                            return (
                                <div className="d-none d-sm-block">
                                <div className="d-flex flex-wrap gap-1 gap-sm-2 mb-5 justify-content-start">
                                                <div style={{ borderRadius: "12px" }}>
                                                    <div className="flex">
                                                     <ShimmerEffect width="133px" height="133px" />
                                                    </div>


                                                    {/* <Image
                                                        src= ""
                                                        alt="Page Template"
                                                        className={`pointer "selected-photo-item" : ""}`}
                                                        width={133}
                                                        height={133}
                                                        style={{ borderRadius: "12px", border: "2px" }}
                                                        onClick={() => {
                                                           }}/> */}
            
                                                </div>
                                </div>
                            </div>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
};
export default TemplateSkelton ;