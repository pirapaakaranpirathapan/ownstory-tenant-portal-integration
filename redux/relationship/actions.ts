import { createAsyncThunk } from '@reduxjs/toolkit';
import { deleteMethod, get, patch, post, put } from '../../connections/fetch_wrapper';
import { IUser } from '../../interface/IUser';
import { BASE_ROUTE } from '../../config/constants';

export const getAllRelationships = createAsyncThunk('relationships/get-all', async (
  {
    active_partner,
    active_service_provider,
    page,
  }: {
    active_partner: string;
    active_service_provider: string;
    page: number;
  }, { rejectWithValue }) => {
  try {
    const response = await get(`${BASE_ROUTE}/master-data/relationships?page=${page}`);
    if (response.code == 200) {
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});

//Single View =====================================================================================================================

export const getOneRelationship = createAsyncThunk(
  "relationship/get-one",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/master-data/relationships/${id}`
      );

      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//Create========================================================================================================
// export const createRelationship = createAsyncThunk('relationship/create', async ({ data }: { data: object | unknown }, { rejectWithValue }) => {
export const createRelationship = createAsyncThunk(
  "relationship/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      createdData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      createdData: {
        name: string;
        slug: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/master-data/relationships`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Update 
export const updateRelationship = createAsyncThunk(
  "relationship/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
      updatedData: {
        name: string;
        slug: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/master-data/relationships/${id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Delete
export const deleteRelationship = createAsyncThunk(
  "relationship/deleted",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/master-data/relationships/${id}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);