
import { createReducer } from '@reduxjs/toolkit';
import { getAllServiceProvider } from './actions';
import { setCookie } from 'cookies-next';
import { ResponseStatus } from '../../helpers/response_status';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const serviceProviderReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllServiceProvider.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllServiceProvider.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;

    })
    .addCase(getAllServiceProvider.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default serviceProviderReducer;
