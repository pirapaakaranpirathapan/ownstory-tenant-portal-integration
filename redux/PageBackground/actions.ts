import { createAsyncThunk } from '@reduxjs/toolkit';
import { deleteMethod, get, patch, post,} from '../../connections/fetch_wrapper';
import { BASE_ROUTE } from '../../config/constants';

export const getAllPageBackground = createAsyncThunk('master-data/page-backgrounds/get-all', async (
  {
    active_partner,
    active_service_provider,
    page,
  }: {
    active_partner: string;
    active_service_provider: string;
    page: number;
  }, { rejectWithValue }) => {
  try {
    const response = await get(`${BASE_ROUTE}/master-data/page-backgrounds?page=${page}`);
    if (response.code == 200) {
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});

//Single View =====================================================================================================================

export const getOnePageBackground = createAsyncThunk(
  "master-data/page-backgrounds/get-one",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/master-data/page-backgrounds/${id}`
      );

      if (response.code == 200) {
        // console.log("response", response);
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const createPageBackground = createAsyncThunk(
  "master-data/page-backgrounds/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      createdData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      createdData: {
        name: string;
        slug: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/master-data/page-backgrounds`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Update 
export const updatePageBackground = createAsyncThunk(
  "master-data/page-backgrounds/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
      updatedData: {
        name: string;
        slug: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/master-data/page-backgrounds/${id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Delete   ==========================================================================================
export const deletePageBackground = createAsyncThunk(
  "master-data/page-backgrounds",
  async (
    {
      activePartner,
      activeServiceProvider,
      id,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/master-data/page-backgrounds/${id}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);