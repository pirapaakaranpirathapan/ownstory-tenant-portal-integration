
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createHeaderBackground, deleteHeaderBackground, getAllHeaderBackgrounds, getOneHeaderBackground, updateHeaderBackground} from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const headerBackgroundReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all header-background" action
  builder
    .addCase(getAllHeaderBackgrounds.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllHeaderBackgrounds.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllHeaderBackgrounds.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one header-background" action
  builder
    .addCase(getOneHeaderBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneHeaderBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneHeaderBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create header-background" action
  builder
    .addCase(createHeaderBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createHeaderBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createHeaderBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update header-background" action
  builder
    .addCase(updateHeaderBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateHeaderBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateHeaderBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete header-background" action
  builder
    .addCase(deleteHeaderBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteHeaderBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteHeaderBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default headerBackgroundReducer;
