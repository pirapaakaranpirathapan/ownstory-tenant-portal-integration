export default function validatePassword(password: any) {
    // Check if password length is at least 12 characters
    if (password.length < 12) {
        return false;
    }

    // Check if password contains at least one uppercase letter
    if (!/[A-Z]/.test(password)) {
        return false;
    }

    // Check if password contains at least one number
    if (!/\d/.test(password)) {
        return false;
    }

    // Check if password contains at least one symbol
    if (!/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(password)) {
        return false;
    }

    // If all checks pass, return true
    return true;
}
