import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const pageBackgroundState = (state: RootState) => state.pageBackgroundReducer

export const pageBackgroundSelector = createSelector(
  pageBackgroundState,
  state => state
)
