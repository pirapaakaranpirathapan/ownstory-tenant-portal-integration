import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const languageState = (state: RootState) => state.languageReducer;

export const languageSelector = createSelector(
    languageState, 
    state => state
)
