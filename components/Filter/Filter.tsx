import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import search from "../../public/images/search.svg";
import minus from "../../public/images/minus.svg";

const Filter = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const toggleFilter = (): void => {
    setIsOpen(!isOpen);
  };

  return (
    <div>
      <div className="row  align-items-center mb-3 d-none">
        <div className="col-md-6 col-12  m-0 p-0">
          <div className="row m-0  mt-2 mt-md-0 ">
            <div className="col-auto ">
              <button
                type="button"
                className="btn btn-system-secondary"
                onClick={toggleFilter}
              >
                Filter
              </button>
            </div>

            <div className="col-auto text-start pt-2">
              <Link href="" className="system-text-primary a-tag">
                Clear Filter
              </Link>
            </div>
          </div>
        </div>

        <div className="col-md-6 col-12 order-first order-md-last mb-2 mb-md-0">
          <div className="search-box">
            <Image src={search} alt="search icon" className="search-icon" />
            <input
              type="search"
              id="header-search"
              className="form-control"
              placeholder="Search Customer"
            />
          </div>
        </div>
      </div>

      <div
        className={`filter-panel system-filter-secondary px-1 px-sm-4 py-1 rounded ${isOpen ? "filter-panel open" : "f-close"
          }`}
      >
        <div className="row m-0 p-0 py-3 ">
          <div className="col-12 col-xl-9 d-flex gap-2 p-0 w-100 ">
            <div className="row m-0 gap-sm-4 w-100 align-items-sm-center">
              <div className="col-auto  col-sm-auto p-0 d-flex">
                <div>
                  <button
                    type="button"
                    className=" btn btn-sm px-3 px-sm-3 py-2 border btn-system-secondary "
                  >
                    <Image src={minus} alt="minus" className="" />
                  </button>
                </div>
              </div>
              <div className="filter-content col-auto col-sm-auto p-0 ">
                <div className="row m-0">
                  <div className="col-12 col-sm-4 p-0 mb-sm-0 mb-2">
                    <select
                      className="form-select border border-2 p-2 bg-white font-90  system-control pointer"
                      id="dropdownMenuLink"
                    >
                      <option defaultValue={"country"}>Country</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div className="col-6 col-sm-4 p-0 pe-2  px-0 px-sm-4 ">
                    <select
                      className="form-select border border-2 p-2 bg-white font-90  system-control pointer"
                      id="dropdownMenuLink"
                    >
                      <option defaultValue={"Exclude"}>Exclude</option>
                      <option value="">Include</option>
                    </select>
                  </div>
                  <div className="col-6 col-sm-4 p-0 ">
                    {/* <input
                      type="text"
                      className="  form-control border border-2 p-2 system-control"
                      value={'United Kingdom'}
                    /> */}
                    <select
                      className="form-select border border-2 p-2 bg-white font-90  system-control pointer"
                      id="dropdownMenuLink"
                    >
                      <option defaultValue={"United Kingdom"}>
                        United Kingdom
                      </option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex gap-4 pb-3 pt-1   justify-content-center justify-content-sm-start">
          <button
            type="button"
            className="btn px-3 px-sm-4 btn-system-primary smaller-font "
          >
            Apply Filter
          </button>
          <button
            type="button"
            className="btn px-3 px-sm-4 btn-system-secondary smaller-font "
          >
            <strong>+ </strong>Add condition
          </button>
        </div>
      </div>
    </div>
  );
};

export default Filter;
