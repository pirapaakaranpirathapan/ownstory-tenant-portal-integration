// import { hashPermission } from "@/services/HasPermission";
import React, { ReactNode, useEffect, useState } from "react";

type Props = {
  children?: ReactNode;
  permissions: string[];
};

const AuthPermission = ({ children, permissions }: Props) => {
  const [isAccept, setIsAccept] = useState(false);

  useEffect(() => {
    checkPermissions();
  }, []);

  async function checkPermissions() {
    // setIsAccept(hashPermission(permissions));
  }

  return <>{isAccept && children}</>;
};

export default AuthPermission;
