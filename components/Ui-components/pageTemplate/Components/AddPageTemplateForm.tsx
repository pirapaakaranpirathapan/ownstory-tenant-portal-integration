import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { pageTemplateSelector } from "../../../../redux/pageTemplate";

interface AddPageTemplate {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string) => void;
    response: any
}
const CreatePageTemplate = ({
    handleCreate,
    actionAddBtnRef,
}: AddPageTemplate) => {
    const { error: apiError } = useAppSelector(pageTemplateSelector);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        response:"",
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.name, formData.slug);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);



    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.name[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreatePageTemplate;
