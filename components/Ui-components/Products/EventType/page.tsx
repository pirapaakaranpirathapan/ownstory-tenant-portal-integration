"use client";
import React, { useEffect, useRef, useState } from "react";
import Head from "next/head";
import { getLabel } from "../../../../utils/lang-manager";
import Filter from "../../../Filter/Filter";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import {
  ACTIVE_PARTNER,
  ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";

import {
  createEventTypes,
  deleteEventTypes,
  getAllEventTypes,
  eventTypeSelector,
  updateEventTypes,
} from "../../../../redux/event-types";
import TestPage from "../../../Paginations";
import FullScreenModal from "../../../Elements/FullScreenModal";
import { Button, Notification } from "@arco-design/web-react";
import AlertModal from "../../../Elements/AlertModal";
import TableHeaderDiscriptionSkelton from "../../../Skelton/TableHeaderDiscriptionSkelton";
import EventTypeSkelton from "../../../Skelton/EventTypeSkelton";
import CreateEventType from "./Components/AddEventTypeForm";
import ViewEditEventType from "./Components/ViewEventTypeForm";

const EventTypeComponent = ({ initialLanguage }: any) => {
  // Creating a reference to a button element
  const actionAddBtnRef = useRef<HTMLButtonElement>(null);
  const actionEditBtnRef = useRef<HTMLButtonElement>(null);

  const [addButtonDisabled, setAddButtonDisabled] = useState(false);
  const [editButtonDisabled, setEditButtonDisabled] = useState(false);

  const [page, setPage] = useState(1);
  const [addModelOpen, setAddModelOpen] = useState(false);
  const [viewEditModelOpen, setViewEditModelOpen] = useState(false);
  const [viewId, setViewId] = useState("");
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [IsDeleteDisabled, setIsDeleteDisabled] = useState(false);
  const [loading, setLoading] = useState(true);

  // Initializing dispatch function and Fetching data using selector hook
  const dispatch = useAppDispatch();
  const { data: allEventTypeData } = useAppSelector(eventTypeSelector);

  // Function to load data based on pagination page number
  const getAllData = (page: number) => {
    setLoading(true);
    dispatch(
      getAllEventTypes({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: page,
      })
    ).then(() => {
      setLoading(false);
    });
  };

  useEffect(() => {
    getAllData(page);
  }, []);

  // Function to handle pagination
  const handlePagination = (page: number) => {
    setPage(page);
    getAllData(page);
  };

  // create realtionship
  const handleCreate = (state_id: number, name: string, slug: string) => {
    setAddButtonDisabled(true);
    const createdData = {
      state_id: state_id,
      name: name,
      slug: slug,
    };
    dispatch(
      createEventTypes({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        createdData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setAddButtonDisabled(false);
        getAllData(page);
        setAddModelOpen(false);
        Notification.success({
          title: "Event Type Create Successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        setAddButtonDisabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Event Type Create Failed",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };

  // edit realtionship
  const handleEdit = (state_id: number, name: string, slug: string) => {
    setEditButtonDisabled(true);
    const updatedData = {
      state_id: state_id,
      name: name,
      slug: slug,
    };
    dispatch(
      updateEventTypes({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        id: viewId,
        updatedData,
      })
    ).then((response: any) => {
      setEditButtonDisabled(false);
      if (response.payload.success == true) {
        getAllData(page);
        setViewEditModelOpen(false);
        Notification.success({
          title: "Event Type Edit Successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        setEditButtonDisabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Event Type Edit Failed",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };

  return (
    <>
      <Head>
        <title>Blackbee Tenant | EventTypes</title>
      </Head>
      <div>
        <div className="d-flex justify-content-between align-items-center page-heading-container">
          {!loading ? (
            <div className="page-header">
              <p className="font-modal-header">
                {getLabel("allEventType", initialLanguage)}
              </p>
              <p className="font-modal-header-description text-muted">
                {getLabel("allEventTypeDescription", initialLanguage)}
              </p>
            </div>
          ) : (
            <>
              <TableHeaderDiscriptionSkelton />
            </>
          )}
          {!loading ? (
            <button
              type="button"
              className="btn btn-system-primary border-0 create-button-text"
              onClick={() => setAddModelOpen(true)}
            >
              <strong>+ </strong>
              {getLabel("create", initialLanguage)}
            </button>
          ) : (
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
            ></button>
          )}
        </div>
        {/* <hr className="page-heading-divider" /> */}
        <div className="mb-0">
          <Filter />
        </div>

        <div className="d-none d-sm-block">
          {loading ? (
            <EventTypeSkelton loopTimes={15} />
          ) : (
            allEventTypeData && (
              <>
                {allEventTypeData?.data?.length > 0 ? (
                  <>
                    <table className="table mt-3">
                      <tbody className="">
                        {allEventTypeData?.data?.map((data: any) => {
                          return (
                            <tr key={data.id} className="align-middle ps-0">
                              <td
                                className="col-3 ps-0"
                                style={{ maxWidth: "150px" }}
                              >
                                <div className="d-flex align-items-center gap-3">
                                  <div className="" style={{ maxWidth: "50%" }}>
                                    <div className="font-110 font-semibold text-truncate">
                                      {data.name}
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td
                                className="col-7 p-0"
                                style={{ maxWidth: "150px" }}
                              >
                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal">
                                  <p className="p-0 m-0 text-truncate">
                                    {data?.slug}
                                  </p>
                                </div>
                              </td>
                              <td className="col-2 p-2">
                                <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                                  <Button
                                    type="secondary"
                                    className="btn btn-system-light d-none d-sm-flex font-semibold"
                                    onClick={() => {
                                      setViewId(data.uuid),
                                        setViewEditModelOpen(true);
                                    }}
                                  >
                                    {getLabel("view", initialLanguage)}
                                  </Button>
                                </div>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </>
                ) : (
                  <div className="w-100 d-flex align-items-center justify-content-center grey-600 py-4 font-120 font-normal">
                    No Event Type found.
                  </div>
                )}
              </>
            )
          )}
        </div>

        {/* -------------------pagination-------------------,------- */}
        {allEventTypeData && allEventTypeData.data?.length == 0
          ? ""
          : allEventTypeData.data?.length > 0 && (
              <>
                <hr className="" />
                <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                  <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                    {getLabel("totalRecords", initialLanguage)}:
                    {/* <span> {allRelationshipsData?.total}</span>{" "} */}
                    <span> {20}</span>
                  </div>
                  <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end">
                    <TestPage
                      page={15}
                      totalPages={200}
                      handlePagination={handlePagination}
                    />
                  </div>
                </div>
              </>
            )}
      </div>

      {/* Add model */}
      <FullScreenModal
        show={addModelOpen}
        onClose={() => {
          setAddModelOpen(false);
        }}
        title={getLabel("addEventType", initialLanguage)}
        description={getLabel("addEventTypeDescription", initialLanguage)}
        footer={
          <div className="d-flex align-items-center">
            <Button
              className="pointer btn btn-system-primary border-0 submit-button"
              ref={actionAddBtnRef}
              disabled={addButtonDisabled}
            >
              {getLabel("submit", initialLanguage)}
            </Button>
          </div>
        }
      >
        <CreateEventType
          actionAddBtnRef={actionAddBtnRef}
          handleCreate={handleCreate}
        />
      </FullScreenModal>

      {/* Edit model */}
      <FullScreenModal
        show={viewEditModelOpen}
        onClose={() => {
          setViewEditModelOpen(false);
        }}
        title={getLabel("viewEventType", initialLanguage)}
        description={getLabel("viewEventTypeDescription", initialLanguage)}
        footer={
          <div className="d-flex align-items-center">
            <Button
              className="pointer btn btn-system-primary border-0 me-4 submit-button"
              ref={actionEditBtnRef}
              disabled={editButtonDisabled}
            >
              {getLabel("submit", initialLanguage)}
            </Button>
            <div
              className="pointer text-danger close-button"
              onClick={() => setShowDeleteModal(true)}
            >
              {getLabel("delete", initialLanguage)}
            </div>
          </div>
        }
      >
        <ViewEditEventType
          actionEditBtnRef={actionEditBtnRef}
          handleEdit={handleEdit}
          viewId={viewId}
        />
      </FullScreenModal>

      {/* delete modal */}
      <AlertModal
        show={showDeleteModal}
        isSmallModal={true}
        position="centered"
        title="Confirmation"
        onClose={() => {
          setShowDeleteModal(false);
        }}
      >
        <div className="mb-4 d-flex conform-color">
          Do you want to delete it?
          {/* <span
            className="text-truncate ps-1 font-semibold"
            style={{
              maxWidth: "100px",
            }}
          >
            {deleteData.name}
          </span>
          ? */}
        </div>

        <div className="col-auto">
          <button
            className="btn btn-system-primary me-3"
            onClick={() => setShowDeleteModal(false)}
          >
            No
          </button>
          <span
            className="yes-delete-it pointer font-120"
            onClick={() => {
              setIsDeleteDisabled(true);
              IsDeleteDisabled
                ? null
                : dispatch(
                    deleteEventTypes({
                      activePartner: ACTIVE_PARTNER,
                      activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                      id: viewId,
                    })
                  ).then((response: any) => {
                    if (response.payload.success == true) {
                      getAllData(page);
                      setViewEditModelOpen(false);
                      setIsDeleteDisabled(false);
                      Notification.success({
                        title: "Deleted Successfully",
                        duration: 3000,
                        content: undefined,
                      });
                      setShowDeleteModal(false);
                    } else {
                      setIsDeleteDisabled(false);
                      Notification.error({
                        title: "Delete Failed",
                        duration: 3000,
                        content: undefined,
                      });
                    }
                  });
            }}
          >
            Yes, Delete it
          </span>
        </div>
      </AlertModal>
    </>
  );
};

export default EventTypeComponent;
