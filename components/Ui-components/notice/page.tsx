"use client"
import React, { useEffect, useRef, useState } from "react";
import Head from "next/head";
import { getLabel } from "../../../utils/lang-manager";
import Filter from "../../../components/Filter/Filter";
import { useAppDispatch, useAppSelector } from "../../../redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "../../../config/constants";
import TestPage from "../../../components/Paginations";
import FullScreenModal from "../../../components/Elements/FullScreenModal";
import { Button, Notification } from "@arco-design/web-react";
import ViewEditRelationship from "./Components/ViewNoticeTitleForm";
import AlertModal from "../../../components/Elements/AlertModal";
import TableHeaderDiscriptionSkelton from "../../Skelton/TableHeaderDiscriptionSkelton";
import { createNoticeTitle, deleteNoticeTitle, getAllNoticeTitle, noticeTitleSelector, updateNoticeTitle } from "../../../redux/notice";
import CreateNoticeTitle from "./Components/AddNoticeTitleForm";
import NoticeTitleSkelton from "../../Skelton/TemplateSkelton";
import RelationshipSkelton from "../../Skelton/RelationshipSkelton";

const NoticeComponent = ({ initialLanguage }: any) => {
    // Creating a reference to a button element
    const actionAddBtnRef = useRef<HTMLButtonElement>(null);
    const actionEditBtnRef = useRef<HTMLButtonElement>(null);
    const [addButtonDisabled, setAddButtonDisabled] = useState(false);
    const [editButtonDisabled, setEditButtonDisabled] = useState(false);
    const [page, setPage] = useState(1);
    const [addModelOpen, setAddModelOpen] = useState(false);
    const [viewEditModelOpen, setViewEditModelOpen] = useState(false);
    const [viewId, setViewId] = useState("");
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [IsDeleteDisabled, setIsDeleteDisabled] = useState(false);
    const [loading, setLoading] = useState(true);
    const [response, setResponse] = useState("");
    const [editResponse, setEditResponse] = useState("");
    const [allNoticeTitleData, setAllNoticeTitleData] = useState([
        { uuid: '1', name: 'obituary', client_Id: 'Fernando', language_id: 'English', typeId: 'Lodge Notice', description: 'description1', slug: 'slug1sss' },
        { uuid: '2', name: 'Dicta mollitia ut', client_Id: 'Alexa', language_id: 'Tamil', typeId: 'Thank you Message', description: 'description2', slug: 'slug2' },
        { uuid: '3', name: 'obituary', client_Id: 'Thakor', language_id: 'English', typeId: 'Enabled', description: 'description3', slug: 'slug3' },
        { uuid: '4', name: 'Expedita magnam aut et ab', client_Id: 'mathesh', language_id: 'Singala', typeId: 'Thank you Message', description: 'description4', slug: 'slug4' },
        { uuid: '5', name: 'obituary', client_Id: 'Fernando', language_id: 'English', typeId: 'Thank you Message', description: 'description5', slug: 'slug5' },

    ]);

    // Initializing dispatch function and Fetching data using selector hook
    const dispatch = useAppDispatch();
    // const { data: allNoticeTitleData } = useAppSelector(noticeTitleSelector);

    // Function to load data based on pagination page number
    const getAllData = (page: number) => {
        setLoading(true);
        dispatch(
            getAllNoticeTitle({
                active_partner: ACTIVE_PARTNER,
                active_service_provider: ACTIVE_SERVICE_PROVIDER,
                page: page,
            })
        ).then(() => {
            setLoading(false);
        });
    };

    useEffect(() => {
        getAllData(page);
    }, []);

    // Function to handle pagination
    const handlePagination = (page: number) => {
        setPage(page);
        getAllData(page);
    };

    // create Notice Title
    const handleCreate = (
        name: string,
        slug: string,
        description: string,
        language_id: string,
        typeId: string,
        client_Id: string
    ) => {
        setAddButtonDisabled(true);
        const createdData = {
            name: name,
            slug: slug,
            description: description,
            language_id: language_id,
            typeId: typeId,
            client_Id: client_Id
        };
        dispatch(
            createNoticeTitle({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                createdData,
            })
        ).then((response: any) => {
            if (response.payload.success == true) {
                setResponse(response)
                setAddButtonDisabled(false);
                getAllData(page);
                setAddModelOpen(false);
                Notification.success({
                    title: "Notice Title Create Successfully",
                    duration: 3000,
                    content: undefined,
                });
            } else {
                setAddButtonDisabled(false);
                response.payload.code != 422 &&
                    Notification.error({
                        title: "Notice Title Create Failed",
                        duration: 3000,
                        content: undefined,
                    });
            }
        });
    };


    // edit noticeTitle
    const handleEdit = (
        name: string,
        slug: string,
        description: string,
        language_id: string,
        typeId: string,
        client_Id: string
    ) => {
        setEditButtonDisabled(true);
        const updatedData = {
            name: name,
            slug: slug,
            description: description,
            language_id: language_id,
            typeId: typeId,
            client_Id: client_Id
        };
        dispatch(
            updateNoticeTitle({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                id: viewId,
                updatedData,
            })
        ).then((response: any) => {
            setEditButtonDisabled(false);
            if (response.payload.success == true) {
                getAllData(page);
                setEditResponse(response);
                setViewEditModelOpen(false);
                Notification.success({
                    title: "Notice Title Edit Successfully",
                    duration: 3000,
                    content: undefined,
                });
            } else {
                setEditButtonDisabled(false);
                response.payload.code != 422 &&
                    Notification.error({
                        title: "Notice Title Edit Failed",
                        duration: 3000,
                        content: undefined,
                    });
            }
        });
    };

    return (
        <>
            <Head>
                <title>Blackbee Tenant | Notice Title</title>
            </Head>
            <div>
                <div className="d-flex justify-content-between align-items-center page-heading-container">
                    {!loading ? <div className="page-header">
                        <p className="font-modal-header">
                            {getLabel("allNoticeTitle", initialLanguage)}
                        </p>
                        <p className="font-modal-header-description text-muted">
                            {getLabel("allNoticeTitleDescription", initialLanguage)}
                        </p>
                    </div> : <><TableHeaderDiscriptionSkelton /></>}
                    {!loading ? (
                        <button
                            type="button"
                            className="btn btn-system-primary border-0 create-button-text"
                            onClick={() => setAddModelOpen(true)}
                        >
                            <strong>+ </strong>
                            {getLabel("create", initialLanguage)}
                        </button>
                    ) : (
                        <button
                            type="button"
                            className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
                        ></button>
                    )}
                </div>
                {/* <hr className="page-heading-divider" /> */}
                <div className="mb-0">
                    <Filter />
                </div>

                <div className="d-none d-sm-block">
                    {loading ? (
                        <RelationshipSkelton loopTimes={5} />
                    ) : (
                        allNoticeTitleData && (
                            <>
                                {allNoticeTitleData?.length > 0 ? (
                                    <>
                                        <table className="table mt-3">
                                            <tbody className="">
                                                <script>
                                                    {/* console.log(allNoticeTitleData); */}
                                                </script>

                                                {allNoticeTitleData?.map((data: any) => {
                                                    return (
                                                        <tr
                                                            key={data.id}
                                                            className="align-middle ps-0">
                                                            <td className="col-3 ps-0" style={{ maxWidth: "150px" }} >
                                                                <div className="d-flex align-items-center gap-3">
                                                                    <div className="" style={{ maxWidth: "100%" }}>
                                                                        <div className="font-110 font-semibold text-truncate">
                                                                            {data.name}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td
                                                                className="col-1 p-0"
                                                                style={{ maxWidth: "150px" }}>
                                                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal">
                                                                    <p className="p-0 m-0 text-truncate">
                                                                        {data?.slug}
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td
                                                                className="col-4 p-0"
                                                                style={{ maxWidth: "150px" }}
                                                            >
                                                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal" >
                                                                    <p className="p-0 m-0 text-truncate">
                                                                        {data?.description}
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td
                                                                className="col-2 p-0"
                                                                style={{ maxWidth: "150px" }}
                                                            >
                                                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal">
                                                                    <p className="p-0 m-0 text-truncate">
                                                                        {data?.language_id}
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td
                                                                className="col-3 p-0"
                                                                style={{ maxWidth: "150px" }}
                                                            >
                                                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal">
                                                                    <p className="p-0 m-0 text-truncate">
                                                                        {data?.typeId}
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td
                                                                className="col-3 p-0"
                                                                style={{ maxWidth: "150px" }}
                                                            >
                                                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start font-normal">
                                                                    <p className="p-0 m-0 text-truncate">
                                                                        {data?.client_Id}
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td className="col-2 p-2">
                                                                <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                                                                    <Button type='secondary' className="btn btn-system-light d-none d-sm-flex font-semibold" onClick={() => {
                                                                        setViewId(data.uuid),
                                                                            setViewEditModelOpen(true)
                                                                    }}>
                                                                        {getLabel("view", initialLanguage)}
                                                                    </Button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    );
                                                })}
                                            </tbody>
                                        </table>
                                    </>
                                ) : (
                                    <div className="w-100 d-flex align-items-center justify-content-center grey-600 py-4 font-120 font-normal">
                                        No Notice Title found.
                                    </div>
                                )}
                            </>
                        )
                    )}
                </div>

                {/* -------------------pagination-------------------,------- */}
                {allNoticeTitleData && allNoticeTitleData.length == 0
                    ? ""
                    : allNoticeTitleData.length > 0 && (
                        <>
                            <hr className="" />
                            <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                                <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                                    {getLabel("totalRecords", initialLanguage)}:
                                    {/* <span> {allNoticeTitleData?.total}</span>{" "} */}
                                    <span> {20}</span>
                                </div>
                                <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end">
                                    <TestPage
                                        page={15}
                                        totalPages={200}
                                        handlePagination={handlePagination}
                                    />
                                </div>
                            </div>
                        </>
                    )}
            </div>


            {/* Add model */}
            <FullScreenModal
                show={addModelOpen}
                onClose={() => {
                    setAddModelOpen(false);
                }}
                title={getLabel("addNoticeTitle", initialLanguage)}
                description={getLabel("addNoticeTitleDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 submit-button"
                            ref={actionAddBtnRef}
                            disabled={addButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                    </div>
                }
            >
                <CreateNoticeTitle actionAddBtnRef={actionAddBtnRef} handleCreate={handleCreate} response={editResponse} />
            </FullScreenModal>

            {/* Edit model */}
            <FullScreenModal
                show={viewEditModelOpen}
                onClose={() => {
                    setViewEditModelOpen(false);
                }}
                title={getLabel("viewNoticeTitle", initialLanguage)}
                description={getLabel("viewRelationshipDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 me-4 submit-button"
                            ref={actionEditBtnRef}
                            disabled={editButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                        <div className="pointer text-danger close-button" onClick={() => setShowDeleteModal(true)}>
                            {getLabel("delete", initialLanguage)}
                        </div>
                    </div>
                }
            >
                <ViewEditRelationship actionEditBtnRef={actionEditBtnRef} handleEdit={handleEdit} viewId={viewId} response={response} />
            </FullScreenModal>


            {/* delete modal */}
            <AlertModal
                show={showDeleteModal}
                isSmallModal={true}
                position="centered"
                title="Confirmation"
                onClose={() => {
                    setShowDeleteModal(false);
                }}
            >
                <div className="mb-4 d-flex conform-color">
                    Do you want to delete it?
                    {/* <span
            className="text-truncate ps-1 font-semibold"
            style={{
              maxWidth: "100px",
            }}
          >
            {deleteData.name}
          </span>
          ? */}
                </div>

                <div className="col-auto">
                    <button
                        className="btn btn-system-primary me-3"
                        onClick={() => setShowDeleteModal(false)}
                    >
                        No
                    </button>
                    <span
                        className="yes-delete-it pointer font-120"
                        onClick={() => {
                            setIsDeleteDisabled(true);
                            IsDeleteDisabled
                                ? null
                                : dispatch(
                                    deleteNoticeTitle({
                                        activePartner: ACTIVE_PARTNER,
                                        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                                        id: viewId
                                    })
                                ).then((response: any) => {
                                    if (response.payload.success == true) {
                                        getAllData(page);
                                        setViewEditModelOpen(false);
                                        setIsDeleteDisabled(false);
                                        Notification.success({
                                            title: "Deleted Successfully",
                                            duration: 3000,
                                            content: undefined,
                                        });
                                        setShowDeleteModal(false);
                                    } else {
                                        setIsDeleteDisabled(false);
                                        Notification.error({
                                            title: "Delete Failed",
                                            duration: 3000,
                                            content: undefined,
                                        });
                                    }
                                });
                        }}
                    >
                        Yes, Delete it
                    </span>
                </div>
            </AlertModal>
        </>
    );
};

export default NoticeComponent;

