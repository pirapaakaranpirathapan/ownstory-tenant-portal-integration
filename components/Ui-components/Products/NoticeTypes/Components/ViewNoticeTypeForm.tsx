import React, { useEffect, useState } from "react";
import {
  getOneNoticeTypes,
  noticeTypesSelector,
} from "../../../../../redux/notice-types";
import {
  ACTIVE_PARTNER,
  ACTIVE_SERVICE_PROVIDER,
} from "../../../../../config/constants";
import {
  useAppDispatch,
  useAppSelector,
} from "../../../../../redux/store/hooks";
import InputSkelton from "../../../../Skelton/InputSkelton";
// import { noticeTypesSelector } from "../../../../../redux/notice-types";

interface ViewEditNoticeType {
  actionEditBtnRef: any;
  viewId: string;
  handleEdit: (
    state_id: number,
    name: string,
    slug: string,
    title: string,
    description: string
  ) => void;
}
const ViewEditNoticeType = ({
  handleEdit,
  actionEditBtnRef,
  viewId,
}: ViewEditNoticeType) => {
  const dispatch = useAppDispatch();
  const [formData, setFormData] = useState({
    state_id: 1,
    name: "",
    slug: "",
    title: "",
    description: "",
  });

  // Edit handle
  const [clickStatus, setClickStatus] = useState(true);
  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // submit data
  useEffect(() => {
    if (actionEditBtnRef.current) {
      actionEditBtnRef.current.onclick = () => {
        setClickStatus(true);
      };
    }

    if (clickStatus) {
      handleEdit(
        formData.state_id,
        formData.name,
        formData.slug,
        formData.title,
        formData.description
      );
      setClickStatus(false);
    }
  }, [clickStatus, handleEdit]);

  const { dataOne: OneNoticeTypesData } = useAppSelector(noticeTypesSelector);
  useEffect(() => {
    loadData();
  }, [viewId]);
  const [skelton, setSkelton] = useState(true);
  const loadData = () => {
    dispatch(
      getOneNoticeTypes({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        id: viewId,
      })
    ).then((res: any) => {
      if (res.payload.code === 200) {
        setSkelton(false);
      }
    });
  };

  // useEffect(() => {
  //   setFormData({
  //     name: OneNoticeTypesData[0]?.name,
  //     slug: OneNoticeTypesData[0]?.slug,
  //     title: OneNoticeTypesData[0]?.title,
  //     description: OneNoticeTypesData[0]?.description,
  //   });
  // }, [loadData]);

  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">State Id</div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="state_id"
                name="state_id"
                defaultValue={OneNoticeTypesData[0]?.state_id}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              Name<span className="font-semibold">*</span>
            </div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="Name"
                name="name"
                defaultValue={OneNoticeTypesData[0]?.name}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Slug</div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="slug"
                name="slug"
                defaultValue={OneNoticeTypesData[0]?.slug}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Title</div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="title"
                name="title"
                defaultValue={OneNoticeTypesData[0]?.title}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Description</div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="description"
                name="description"
                defaultValue={OneNoticeTypesData[0]?.description}
                onChange={handleChange}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewEditNoticeType;
