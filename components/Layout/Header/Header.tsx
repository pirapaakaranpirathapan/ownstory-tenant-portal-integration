import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../public/images/user.png'
import uparrow from '../../../public/images/uparrow.svg'
import arrowdown from '../../../public/images/arrowdown.svg'
import world from '../../../public/images/world.svg'
import search from '../../../public/images/search.svg'
import Notifications from '../../../public/images/Notifications.svg'
import barssvg from '../../../public/images/barssvg.svg'
import { getCookie, setCookie } from 'cookies-next'
import { useRouter } from 'next/router';
import router from 'next/router';
import { useAppDispatch, useAppSelector } from '../../../redux/store/hooks'
import { getMe, meSelector } from '../../../redux/me'
import { getLabel } from '../../../utils/lang-manager'
import { languageSelector } from '../../../redux/language'
import { logout } from '../../../redux/auth'
interface Props {
  toggleSideBar: () => void,
  initialLanguage: string,
}

const Header: React.FC<Props> = ({ toggleSideBar, initialLanguage }) => {
  const [showLanguage, setShowLanguage] = useState(false)
  const [showMobileLanguage, setShowMobileLanguage] = useState(false)
  const [showProfile, setShowProfile] = useState(false)
  const [showMobileProfile, setShowMobileProfile] = useState(false)
  const [showOption, setShowOption] = useState(false)
  const [meData, setMeData] = useState({}) as unknown as any;
  const [activeLanguage, setActiveLanguage] = useState<string>('');
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [searchTermLanguage, setSearchTermLanguage] = useState('');
  const [filteredLanguages, setFilteredLanguages] = useState([]);
  const [updateFilterLanguages, setUpdateFilterLanguages] = useState([]);
  const [visibleLanguages, setVisibleLanguages] = useState(16);
  const dropdownRef = useRef<HTMLDivElement | null>(null);
  const dispatch = useAppDispatch()
  // const router = useRouter();
  // const path = router.pathname;
  const profilePopupRef = useRef<HTMLDivElement>(null)
  const optionPopupRef = useRef<HTMLDivElement>(null)
  const { data: getMeData } = useAppSelector(meSelector)
  const { data: getAllLanguageData } = useAppSelector(languageSelector);
  const allowedPaths = ['/dashboard'];

  useEffect(() => {
    const activeLanguageFromCookie = getCookie('language') as string || 'English';
    if (activeLanguageFromCookie && getAllLanguageData) {
      setActiveLanguage(activeLanguageFromCookie);
      const updatedFilteredLanguages = getAllLanguageData.slice();
      const languageIndex = updatedFilteredLanguages.findIndex((data: any) => data.name === activeLanguageFromCookie);
      if (languageIndex !== -1) {
        const [selectedLanguage] = updatedFilteredLanguages.splice(languageIndex, 1);
        updatedFilteredLanguages.unshift(selectedLanguage);
      }
      setFilteredLanguages(updatedFilteredLanguages);
      setUpdateFilterLanguages(updatedFilteredLanguages);
    }
  }, [getAllLanguageData]);

  useEffect(() => {
    if (showLanguage === false) {
      setFilteredLanguages(updateFilterLanguages);
    }
  }, [showLanguage, updateFilterLanguages]);

  useEffect(() => {
    const maxRetries = 25;
    let retryCount = 0;
    const fetchUserData = async () => {
      const response = await dispatch(getMe());
      if (response.payload === undefined && retryCount < maxRetries) {
        retryCount++;
        const delay = retryCount * 1000;
        setTimeout(fetchUserData, delay);
      }
    };
    fetchUserData();
  }, []);

  useEffect(() => {
    setMeData(getMeData)
  }, [getMeData])

  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (
        profilePopupRef.current &&
        !profilePopupRef.current.contains(event.target as Node)
      ) {
        setShowProfile(false)
      }
    }
    function handleClickOutside2(event: MouseEvent) {
      if (
        optionPopupRef.current &&
        !optionPopupRef.current.contains(event.target as Node)
      ) {
        setShowOption(false)
      }
    }

    function handlePopstate() {
      setShowProfile(false);
      setShowOption(false);
    }
    document.addEventListener('mousedown', handleClickOutside)
    document.addEventListener('mousedown', handleClickOutside2)
    window.addEventListener('popstate', handlePopstate);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
      document.removeEventListener('mousedown', handleClickOutside2)
      window.removeEventListener('popstate', handlePopstate);
    }
  }, [profilePopupRef, optionPopupRef])

  useEffect(() => {
    showProfile == false ? setShowLanguage(false) : ''
  }, [showProfile])
  useEffect(() => {
    showMobileProfile == false ? setShowMobileLanguage(false) : ''
  }, [showMobileProfile])
  useEffect(() => {
    showOption == false ? setShowOption(false) : ''
  }, [showOption])

  useEffect(() => {
    const handleScroll = () => {
      if (dropdownRef.current) {
        const { scrollTop, scrollHeight, clientHeight } = dropdownRef.current;
        if (scrollTop + clientHeight >= scrollHeight - 10) {
          loadMoreServiceProviders();
        }
      }
    };
    if (dropdownRef.current) {
      dropdownRef.current.addEventListener('scroll', handleScroll);
    }
    return () => {
      if (dropdownRef.current) {
        dropdownRef.current.removeEventListener('scroll', handleScroll);
      }
    };
  }, [dropdownRef, showLanguage]);

  const logoutAction = () => {
    dispatch(logout())
  };

  const HandleSelectLanguage = (language: any) => {
    setCookie('language', language);
    setActiveLanguage(language);
    window.location.reload();
  }

  const setting = () => {
    router.push("/setting");
    setShowProfile(false);
  };

  const useraccess = () => {
    router.push("/user-access?p=1");
    setShowProfile(false);
  };

  const handleLanguageSearch = (event: { target: { value: string } }) => {
    const searchValue = event.target.value.toLowerCase();
    setSearchTermLanguage(searchValue);
    const filteredLanguages = getAllLanguageData?.filter((data: { name: string }) =>
      data.name.toLowerCase().includes(searchValue)
    );
    setFilteredLanguages(filteredLanguages);
  };

  const loadMoreServiceProviders = () => {
    setVisibleLanguages((prevVisibleLanguages) => prevVisibleLanguages + 16);
  };

  return (
    <nav className="navbar position-sticky top-0 w-100 ps-2 pe-4 pe-lg-2 ">
      <div className="navbar-container w-100 d-flex align-items-center ">
        <div className="m-3 d-flex d-lg-none toggle" onClick={toggleSideBar}>
          <Image src={barssvg} alt="" />
        </div>
        <div className=" row align-items-center w-100 ">
          {/* {allowedPaths.includes(path) ? ( */}
          <div className="col-md-7 col-9 d-flex align-items-center">
            <div className="search-box float-start mw-380">
              <Image
                src={search}
                alt="search icon"
                className="search-icon p-0"
              />
              <input
                type="search"
                id="header-search"
                className="form-control"
                placeholder={`${getLabel("search", initialLanguage)}`}
              />
              <div className="d-flex" ref={optionPopupRef}>
                <div
                  className="pointer"
                  onClick={() => {
                    setShowOption(!showOption)
                  }}>
                  <div className="header-option d-none d-sm-flex">{getLabel("option", initialLanguage)}</div>
                  <Image
                    src={arrowdown}
                    alt="arrow down"
                    className="search-arrow pointer"
                  />
                </div>
                {showOption && (
                  <div className="option-content">
                    <div className="px-2 my-1  font-90 system-icon-secondary">
                      {getLabel("showOptions", initialLanguage)}
                    </div>
                    <li className="px-2 rounded font-110 font-normal py-1 px-1 ">
                      <div className="d-flex align-items-center ">
                        <input
                          id="include-documents"
                          type="checkbox"
                          className="me-1 check-button pointer b-line mt-w"
                        />
                        <label
                          htmlFor="include-documents"
                          className="pointer p-0 font-normal font-100">
                          {getLabel("includeDocuments", initialLanguage)}
                        </label>
                      </div>
                    </li>
                    <li className="px-2 rounded font-110 font-normal py-1 px-1">
                      <div className="d-flex align-items-center">
                        <input
                          id="other-documents"
                          type="checkbox"
                          className="me-1 check-button pointer b-line mt-w"
                        />
                        <label
                          htmlFor="other-documents"
                          className="pointer font-normal font-100">
                          {getLabel("otherDocuments", initialLanguage)}
                        </label>
                      </div>
                    </li>
                  </div>
                )}
              </div>
            </div>
            <div className="d-none d-xl-flex align-items-center p-0 ps-3">
              <Link
                href=""
                className="text-decoration-none system-icon-secondary font-semibold  me-1 pointer">
                Go to Dinakaran.com
              </Link>
              <Image src={uparrow} alt="user image" className="mb-1" />
            </div>
          </div>
          {/* ) : <div className="col-md-7 col-9 d-flex align-items-center"></div>} */}
          <div className="col-md-5 col-3 p-0 d-flex justify-content-end align-items-center">
            <div className="position-relative p-0">
              <Image
                src={Notifications}
                alt="notification image"
                className="header-notification-image text-muted pointer"
              />
            </div>
            <div className="ms-1 ms-sm-4 d-flex rounded-circle border system-view-secondary header-user-image align-items-center justify-content-center d-sm-none">
              S
            </div>
            <div className="ps-3 ps-sm-4 mobile-dropdown d-none">
              <Image
                src={user}
                alt="user image"
                className=" header-user-image rounded-circle pointer"
                onClick={() => {
                  setShowMobileProfile(!showMobileProfile)
                }}
              />
              {showMobileProfile && (
                <div className="mobile-dropdown-content">
                  <div className="mx-4 mt-3">
                    <div className="font-130 font-normal text-truncate">
                      Sameee mobile
                    </div>
                    <p className="text-truncate font-100 system-icon-secondary font-normal ">
                      samee.kumarasamy@mydomain.co.uk
                    </p>
                  </div>
                  <hr className=" p-0" />
                  <li className="mx-3 rounded font-110 font-normal">
                    {getLabel("myAccountSettings", initialLanguage)}
                  </li>
                  <hr className="mx-3 p-0" />
                  <li className="mx-3 rounded font-110 font-normal">
                    {getLabel("partnerSettings", initialLanguage)}
                  </li>
                  <li className="mx-3 mb-5  rounded font-110 font-normal ">
                    {getLabel("userAccess", initialLanguage)}
                  </li>
                  <div className="mx-4 ">
                    <Link
                      href=""
                      className="text-decoration-none system-icon-secondary font-semibold  me-1">
                      Go to Dinakaran.com
                    </Link>
                    <Image src={uparrow} alt="user image" className="mb-1" />
                  </div>
                  <hr className=" p-0 " />
                  <div className="row m-0 mx-4 mb-3">
                    <div className="col-6 p-0 language-dropdown">
                      <div
                        className="d-flex align-items-center"
                        onClick={() => {
                          setShowMobileLanguage(!showMobileLanguage)
                        }}>
                        <Image src={world} alt="world-icon" className="me-2" />
                        <span className="font-110 font-normal"> English</span>
                      </div>
                      {showMobileLanguage && (
                        <ul className="language-dropdown-content">
                          <div className="row m-0">
                            <li className="col-6  rounded font-110 font-normal">
                              English
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Language 4
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              French
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Lang 5
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Tamil
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Lang 6
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Sinhala
                            </li>
                            <li className="col-6 rounded font-110 font-normal">
                              Lang 7
                            </li>
                          </div>
                        </ul>
                      )}
                    </div>
                    <div className="col-6 p-0 text-end font-110 font-normal">
                      <Link href="/" className="text-decoration-none text-dark">
                        <p className="p-0 m-0">{getLabel("logout", initialLanguage)}</p>
                      </Link>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="user-dropdown" ref={profilePopupRef}>
              <div
                className=" align-items-center ms-2 d-none d-sm-flex pointer"
                onClick={() => {
                  setShowProfile(!showProfile)
                }}>
                <div
                  className="text-dark  system-control  font-110 font-normal text-truncate "
                  style={{ maxWidth: '70px' }}>
                  {meData && meData.first_name && meData.last_name ? `${meData.first_name} ${meData.last_name}` : ''}
                </div>
                <Image src={arrowdown} alt="arrow down" className="ms-4" />
              </div>
              {showProfile && (
                <div className="user-dropdown-content ">
                  <div className="mx-4 mt-3">
                    <div className="font-130 font-normal text-truncate">
                      {meData && meData.first_name && meData.last_name ? `${meData.first_name} ${meData.last_name}` : ''}
                    </div>
                    <p className="font-100 system-icon-secondary font-normal  text-truncate">
                      {meData?.email}
                    </p>
                  </div>
                  <hr className=" p-0 my-2" />
                  {/* <li className={`mx-3 rounded font-110 font-normal  ${path === '/setting' ? 'active-link-acc font-bold' : ''}`}
                    onClick={setting} >
                    {getLabel("myAccountSettings", initialLanguage)}
                  </li> */}
                  <hr className="mx-3 p-0 m-2" />
                  {/* <li className={`mx-3 rounded font-110 font-normal  ${path === '/partner-setting' ? 'active-link-acc font-bold' : ''}`}>
                    {getLabel("partnerSettings", initialLanguage)}
                  </li> */}
                  {/* <li className={`mx-3 rounded font-110 font-normal  ${path === '/user-access' ? 'active-link-acc font-bold' : ''}`} onClick={useraccess} >
                    {getLabel("userAccess", initialLanguage)}
                  </li> */}
                  <hr className=" p-0 " />
                  <div className="row m-0  mb-3 px-3">
                    <div className="col-6 p-0 language-dropdown  ">
                      <div
                        className="d-flex align-items-center pointer px-2 py-1 li-hover rounded"
                        onClick={() => {
                          setShowLanguage(!showLanguage)
                        }}>
                        <Image src={world} alt="world-icon" className="me-2  " />
                        <span className="font-110 font-normal text-truncate lang-title-color" style={{ maxWidth: "120px" }}>{activeLanguage}</span>
                      </div>
                      {showLanguage && (
                        <ul className="language-dropdown-content">
                          <div className="search-box-side p-0">
                            <input
                              type="search"
                              id="sidebar-search"
                              onChange={handleLanguageSearch}
                              className="form-control"
                              placeholder="Search..."
                            />
                          </div>
                          <div className="row m-0 overflow-lang " ref={dropdownRef}>
                            {filteredLanguages.slice(0, visibleLanguages).map((data: any, languageIndex: any) => (
                              <li
                                key={languageIndex}
                                className={`col-6 rounded font-110 font-normal text-truncate ${activeLanguage === data.name ? "active-system font-bold" : ""
                                  }`}
                                onClick={() => HandleSelectLanguage(data.name)}
                              >
                                <p className="text-truncate m-0 p-0">
                                  {data.name}
                                </p>
                              </li>
                            ))}
                            {/* {filteredLanguages.map((data: any, languageIndex: any) => (
                              <li
                                key={languageIndex}
                                className={`col-6 rounded font-110 font-normal text-truncate ${activeLanguage === data.name ? "active-system font-bold" : ""
                                  }`}
                                onClick={() => HandleSelectLanguage(data.name)}
                              >
                                <p className="text-truncate m-0 p-0">
                                  {data.name}
                                </p>
                              </li>
                            ))} */}
                          </div>
                        </ul>
                      )}
                    </div>
                    <div
                      onClick={() => logoutAction()}
                      className="col-6 p-0 ps-5 font-110 font-normal text-decoration-none text-dark pointer text-end   ">
                      <p className="p-0 m-0 px-2 py-1  li-hover rounded">{getLabel("logout", initialLanguage)}</p>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Header
