"use client"
import React, { useEffect, useRef, useState } from "react";
import { GetServerSidePropsContext } from "next/types";
// import { decryptToken } from "@/utils/encryptionAndDecryptionUtils";
import { useRouter } from "next/router";
import { Form, InputGroup } from "react-bootstrap";
import Image from "next/image";
import Noticevj from "../../public/images/logo.svg";
import Link from "next/link";
import { Popover } from "@arco-design/web-react";
import { useAppDispatch, useAppSelector } from "../../redux/store/hooks";
import { authSelector, login } from "../../redux/auth";
import { getLabel } from "../../utils/lang-manager";
import Loader from "../../components/Loader/Loader";
import Error from "../../components/Errors/Error";
import { useApiErrorHandling } from "../../components/Errors/useApiErrorHandling";
const LoginPage = ({ initialLanguage }: any) => {
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [loginData, setLoginData] = useState({ email: "", password: "" } as unknown as any);
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  // const router = useRouter();
  // const dispatch = useAppDispatch();
  const passwordPopoverRef = useRef<HTMLDivElement | null>(null);
  const emailWrapperRef = useRef<HTMLDivElement | null>(null);
  const passwordWrapperRef = useRef<HTMLDivElement | null>(null);
  const { error: apiError } = useAppSelector(authSelector);

  const [createResponse, setCreateResponse] = useState("");
  const errors = useApiErrorHandling(apiError, createResponse);

  const onChangeSelect = (name: string, value: any) => {
    setLoginData({
      ...loginData,
      [name]: value,
    });
    setEmailError("");
    setPasswordError("");
  };
  const dispatch = useAppDispatch();

  useEffect(() => {
    const getAutofill = () => {
      let inputEmail = document.getElementById('formEmail') as HTMLInputElement;
      let inputPassword = document.getElementById('formPassword') as HTMLInputElement;
      if (inputEmail) {
        const autoFillEmail = inputEmail.matches(':-webkit-autofill');
        if (autoFillEmail) {
          const emailValue = inputEmail.value;
          setLoginData((prevLoginData: any) => ({
            ...prevLoginData,
            email: emailValue,
          }));
        }
      }
      // onInput
      if (inputPassword) {
        const autoFillPassword = inputEmail.matches(':-webkit-autofill');
        if (autoFillPassword) {
          const passwordValue = inputPassword.value;
          setLoginData((prevLoginData: any) => ({
            ...prevLoginData,
            password: passwordValue,
          }));
        }
      }
    }
    setTimeout(() => {
      getAutofill();
    }, 500);
  }, [])
  const isSignInButtonDisabled = !loginData.email || !loginData.password;

  // useEffect(() => {
  //   const handleBodyClick = (e: MouseEvent) => {
  //     if (emailWrapperRef.current && !emailWrapperRef.current.contains(e.target as Node) && emailError) {
  //       setEmailError("");
  //     }
  //     if (passwordWrapperRef.current && !passwordWrapperRef.current.contains(e.target as Node) && passwordError) {
  //       setPasswordError("");
  //     }
  //   };
  //   document.body.addEventListener("click", handleBodyClick);
  //   return () => {
  //     document.body.removeEventListener("click", handleBodyClick);
  //   };
  // }, [emailError, passwordError]);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    setLoading(true);
    if (loginData.email && loginData.password) {
      try {
        const loginResponse: any = await dispatch(login({ data: loginData }));
        // console.log("login===>", loginResponse);
        setCreateResponse(loginResponse.payload)


      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    }
    setEmailError("");
    setPasswordError("");
  };
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  return (
    <>
      {/* <div className="mb-5">
            <nav className="navbar position-sticky top-0 w-100 ps-2 pe-2 pe-md-0 ">
              <div className="row">
                <div className="col-auto  login-profile-box mt-2 ms-3 rounded">
                  <Image src={Noticevj} alt="" className="image rounded" />
                </div>
                <div className="col-auto pointer">
                  <h6 className="fw-semibold col-12 mt-2 mb-0">BlackBee</h6>
                  <p className="font-90 fw-semibold">Partner</p>
                </div>
              </div>
            </nav>
          </div> */}
      <div className="d-flex justify-content-center w-100 mb-5 mt-5">
        <div className="col-9 col-sm-6 col-md-4 col-lg-4 col-xl-3 login-container">
          <h5 className="d-flex  mb-5 login-header">
            {/* {getLabel("signInToBlackBeesPartner", initialLanguage)} */}
            Login
          </h5>
          <Form onSubmit={handleSubmit} className="">
            <Form.Group controlId="formEmail">
              <Form.Label className="m-0 mb-1">
                {getLabel("email", initialLanguage)}
              </Form.Label>
              <Form.Control
                value={loginData?.email}
                onChange={(e) => {
                  onChangeSelect("email", e.target.value);
                }}
                placeholder="sam@gmail.com"
                className="form-control inner-shadow-default p-2"
              />
            </Form.Group>
            <div className="d-flex justify-content-between align-items-center mb-1 mt-3">
              <Form.Label className="m-0">
                {getLabel("password", initialLanguage)}
              </Form.Label>
              <Form.Label className="m-0">
                <Link
                  href="/forgot-password"
                  className="text-decoration-none text-decoration-underline text-dark font-bold font-100"
                >
                  {getLabel("forgotPassword", initialLanguage)}
                </Link>
              </Form.Label>
            </div>

            <InputGroup className="password-container">
              <div>
                <Image
                  onClick={togglePasswordVisibility}
                  src={showPassword ? "/images/openeye.svg" : "/images/closeeye.svg"}
                  alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                  width={16}
                  height={16}
                  className="input-group-eye"
                />
              </div>
              <div className="w-100" ref={passwordWrapperRef}>
                <Popover
                  ref={passwordPopoverRef}
                  popupVisible={passwordError ? true : false}
                  position="bl"
                  className="arco-trigger-position-bl1 custom-dropdown"
                  content={
                    <span>
                      <p className="m-0">{passwordError}</p>
                    </span>
                  }
                >
                  <Form.Control
                    id="formPassword"
                    type={showPassword ? "text" : "password"}
                    value={loginData?.password}
                    onChange={(e) => {
                      onChangeSelect("password", e.target.value);
                    }}
                    placeholder="Password"
                    className="form-control pe-5 inner-shadow-default p-2"
                  />{" "}
                </Popover>
              </div>
            </InputGroup>
            <button
              type="submit"
              className="w-100 btn btn-dark mt-3 mb-4 p-2"
              disabled={isSignInButtonDisabled}
            >
              {getLabel("signin", initialLanguage)}
            </button>
          </Form>
          <div className="d-flex justify-content-center">
            <Error error={errors.email} />
          </div>
          <hr className="mb-5" />
          <div className="d-flex justify-content-center w-100">
            {/* {getLabel("joinAsPartner", initialLanguage)}? */}
            Want to partner with us?
            <span
              className="ps-2 font-semibold pointer"
            // onClick={openContactPage}
            >
              <u>
                Contact now
              </u>
            </span>
          </div>
        </div>
      </div>
      <div className="bottom-container  d-flex flex-sm-row flex-column justify-content-center align-items-center">
        <a href="#" className="a-tag-disable">
          <span className="pointer">{getLabel("termsofService", initialLanguage)}</span>
        </a>
        <a href="#" className="a-tag-disable">
          <span className="mx-sm-3 mx-0 pointer">
            {getLabel("support", initialLanguage)}
          </span>
        </a>
        <a href="#" className="a-tag-disable">
          <span className="pointer">2022 {getLabel("companyName", initialLanguage)}</span>
        </a>
      </div>
    </>
  );
};


export default LoginPage;
