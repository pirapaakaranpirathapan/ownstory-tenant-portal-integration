import { createAsyncThunk } from '@reduxjs/toolkit';
import { deleteMethod, get, patch, post, put } from '../../connections/fetch_wrapper';
import { IUser } from '../../interface/IUser';
import { BASE_ROUTE } from '../../config/constants';

export const getAllLanguages = createAsyncThunk('languages/get-all', async (
  {
    active_partner,
    active_service_provider,
    page,
  }: {
    active_partner: string;
    active_service_provider: string;
    page: number;
  }, { rejectWithValue }) => {
  try {
    const response = await get(`${BASE_ROUTE}/master-data/languages`);
    if (response.code == 200) {
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});

//Single View =====================================================================================================================

export const getOneLanguages = createAsyncThunk(
  "languages/get-one",
  async (
    {
      activePartner,
      activeServiceProvider,
      uuid,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/master-data/languages/${uuid}`
      );

      if (response.code == 200) {
        // console.log("response", response);
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//Create========================================================================================================
export const createLanguages = createAsyncThunk(
  "languages/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      createdData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      createdData: {
        code: string;
        name: string;
        native_name: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/master-data/languages`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Update 
export const updateLanguages = createAsyncThunk(
  "languages/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      uuid,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      uuid: string;
      updatedData: {
        code: string;
        name: string;
        native_name: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/master-data/languages/${uuid}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
// Delete   ==========================================================================================
export const deleteLanguages = createAsyncThunk(
  "languages/deleted",
  async (
    {
      activePartner,
      activeServiceProvider,
      uuid,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/master-data/languages/${uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);