import React, { useEffect, useState } from "react";
import {
    getOneRelationship,
    relationshipSelector,
} from "../../../../redux/relationship";
import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import { clipartSelector } from "../../../../redux/clipart";

interface ViewEditClipart {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (name: string, slug: string, description:string, thumb_url: string, left_url: string, left_print_url: string, right_url: string, right_print_url: string) => void;
}
const ViewEditClipart = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
}: ViewEditClipart) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        description: "",
        thumb_url: "",
        left_url: "",
        left_print_url: "",
        right_url: "",
        right_print_url: "",
    });

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleEdit(formData.name, formData.slug, formData.description, formData.thumb_url, formData.left_url, formData.left_print_url, formData.right_url, formData.right_print_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleEdit]);

    const { dataOne: OneClipartData } =
        useAppSelector(clipartSelector);
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        // dispatch(
        //     getOneRelationship({
        //         activePartner: ACTIVE_PARTNER,
        //         activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        //         id: viewId,
        //     })
        // ).then((res: any) => {
        //     if (res.payload.code === 200) {
        //         setSkelton(false);
        //     }
        // });
    };

    useEffect(() => {
        setFormData({
            name: OneClipartData[0]?.name,
            slug: OneClipartData[0]?.slug,
            description: OneClipartData[0]?.description,
            thumb_url: OneClipartData[0]?.thumb_url,
            left_url: OneClipartData[0]?.left_url,
            left_print_url: OneClipartData[0]?.left_print_url,
            right_url: OneClipartData[0]?.right_url,
            right_print_url: OneClipartData[0]?.right_print_url,
        });
    }, [loadData]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Clipart Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="slug"
                                defaultValue={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Description<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Description"
                                name="description"
                                defaultValue={formData.description}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default ViewEditClipart;
