"use client"
import React, { useEffect, useRef, useState } from "react";
import Head from "next/head";
import { getLabel } from "../../../utils/lang-manager";
import Filter from "../../Filter/Filter";
import { useAppDispatch, useAppSelector } from "../../../redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "../../../config/constants";
import BG from "../../../public/images/backImage.svg";
import TestPage from "../../Paginations";
import FullScreenModal from "../../Elements/FullScreenModal";
import { Button, Notification } from "@arco-design/web-react";
import AlertModal from "../../Elements/AlertModal";
import TableHeaderDiscriptionSkelton from "../../Skelton/TableHeaderDiscriptionSkelton";
import Image from "next/image";
import noticeTemplate from "../../../public/images/noticeTemplate1.png"
import { createNoticeTemplate, deleteNoticeTemplate, getAllNoticeTemplate, noticeTemplateSelector, updateNoticeTemplate } from "../../../redux/noticeTemplate";
import CreateNoticeTemplate from "./Components/AddNoticeTemplateForm";
import ViewEditNoticeTemplate from "./Components/ViewNoticeTemplateForm";
import RelationshipSkelton from "../../Skelton/RelationshipSkelton";
import TemplateSkelton from "../../Skelton/TemplateSkelton";

const NoticeTemplateComponent = ({ initialLanguage }: any) => {
    // Creating a reference to a button element
    const actionAddBtnRef = useRef<HTMLButtonElement>(null);
    const actionEditBtnRef = useRef<HTMLButtonElement>(null);
    const [addButtonDisabled, setAddButtonDisabled] = useState(false);
    const [editButtonDisabled, setEditButtonDisabled] = useState(false);
    const [page, setPage] = useState(1);
    const [addModelOpen, setAddModelOpen] = useState(false);
    const [viewEditModelOpen, setViewEditModelOpen] = useState(false);
    const [viewId, setViewId] = useState("");
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [IsDeleteDisabled, setIsDeleteDisabled] = useState(false);
    const [loading, setLoading] = useState(true);
    const [response, setResponse] = useState("");
    const [showNoticeTitle, setShowProfileShow] = useState(false);
    // Initializing dispatch function and Fetching data using selector hook
    const dispatch = useAppDispatch();
    const [editResponse, setEditResponse] = useState("");
    const { data: allNoticeTemplateData } = useAppSelector(noticeTemplateSelector);
    // Function to load data based on pagination page number
    const getAllData = (page: number) => {
        setLoading(true);
        dispatch(
            getAllNoticeTemplate({
                active_partner: ACTIVE_PARTNER,
                active_service_provider: ACTIVE_SERVICE_PROVIDER,
                page: page,
            })
        ).then(() => {
            setLoading(false);
        });
    };

    useEffect(() => {
        getAllData(page);
    }, []);

    // Function to handle pagination
    const handlePagination = (page: number) => {
        setPage(page);
        getAllData(page);
    };

    // create Notice Template
    const handleCreate = (
        name: string,
        slug: string,
        thumb_url: string,
        description: string,
        // is_premium: string,
        // has_header_background: string,
        // has_page_background: string,
        // notice_type_id: string,
        // model: string,
        // clip_arts: string,
        // frames: string,
        // header_backgrounds: string,
        // page_backgrounds: string,
        // borders: string,
        // languages: string,
        // tags: string,
        // template_groups: string
    ) => {
        setAddButtonDisabled(true);
        const createdData = {
            name: name,
            slug: slug,
            thumb_url: thumb_url,
            // is_premium: is_premium,
            // has_header_background: has_header_background,
            // has_page_background: has_page_background,
            // notice_type_id: notice_type_id,
             description: description,
            // model: model,
            // clip_arts: clip_arts,
            // frames: frames,
            // header_backgrounds: header_backgrounds,
            // page_backgrounds: page_backgrounds,
            // borders: borders,
            // languages: languages,
            // tags: tags,
            // template_groups: template_groups

        };
        dispatch(
            createNoticeTemplate({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                createdData,
            })
        ).then((response: any) => {
            if (response.payload.success == true) {
                setResponse(response)
                setAddButtonDisabled(false);
                getAllData(page);
                setAddModelOpen(false);
                Notification.success({
                    title: "Notice Template Create Successfully",
                    duration: 3000,
                    content: undefined,
                });
            } else {
                setAddButtonDisabled(false);
                response.payload.code != 422 &&
                    Notification.error({
                        title: "Notice Template Create Failed",
                        duration: 3000,
                        content: undefined,
                    });
            }
        });
    };


    // edit Notice Template
    const handleEdit = (
        name: string,
        slug: string,
        thumb_url: string,
        // is_premium: string,
        // has_header_background: string,
        // has_page_background: string,
        // notice_type_id: string,
         description: string,
        // model: string,
        // clip_arts: string,
        // frames: string,
        // header_backgrounds: string,
        // page_backgrounds: string,
        // borders: string,
        // languages: string,
        // tags: string,
        // template_groups: string
     
    ) => {
        setEditButtonDisabled(true);
        const updatedData = {
            name: name,
            slug: slug,
            thumb_url: thumb_url,
          // is_premium: is_premium,
            // has_header_background: has_header_background,
            // has_page_background: has_page_background,
            // notice_type_id: notice_type_id,
             description: description,
            // model: model,
            // clip_arts: clip_arts,
            // frames: frames,
            // header_backgrounds: header_backgrounds,
            // page_backgrounds: page_backgrounds,
            // borders: borders,
            // languages: languages,
            // tags: tags,
            // template_groups: template_groups
        };
        dispatch(
            updateNoticeTemplate({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                id: viewId,
                updatedData,
            })
        ).then((response: any) => {
            setEditButtonDisabled(false);
            if (response.payload.success == true) {
                getAllData(page);
                setViewEditModelOpen(false);
                setEditResponse(response)
                Notification.success({
                    title: "Notice Template Edit Successfully",
                    duration: 3000,
                    content: undefined,
                });
            } else {
                setEditButtonDisabled(false);
                response.payload.code != 422 &&
                    Notification.error({
                        title: "Notice Template Edit Failed",
                        duration: 3000,
                        content: undefined,
                    });
            }
        });
    };
    const [selectedOptionId, setSelectedOptionId] = useState("");
    return (
        <>
            <Head>
                <title>Blackbee Tenant | Notice Template</title>
            </Head>
            <div>
                <div className="d-flex justify-content-between align-items-center page-heading-container">
                    {!loading ? <div className="page-header">
                        <p className="font-modal-header">
                            {getLabel("allNoticeTemplate", initialLanguage)}
                        </p>
                        <p className="font-modal-header-description text-muted">
                            {getLabel("allNoticeTemplateDescription", initialLanguage)}
                        </p>
                    </div> : <><TableHeaderDiscriptionSkelton /></>}
                    {!loading ? (
                        <button
                            type="button"
                            className="btn btn-system-primary border-0 create-button-text"
                            onClick={() => setAddModelOpen(true)}
                        >
                            <strong>+ </strong>
                            {getLabel("create", initialLanguage)}
                        </button>
                    ) : (
                        <button
                            type="button"
                            className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
                        ></button>
                    )}
                </div>
                {/* <hr className="page-heading-divider" /> */}
                <div className="mb-0">
                    <Filter />
                </div>

                <div className="d-none d-sm-block">
                {loading ? (
                        <TemplateSkelton loopTimes={15}/>
                      
                    ) : (
                        allNoticeTemplateData && (

                   <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start ">
                        {allNoticeTemplateData?.data?.map((item: any, index: any) => {
                            return (
                                <>
                                    <div
                                        style={{ borderRadius: "12px" }}
                                    >
                                        <Image
                                            src={allNoticeTemplateData[index] ? item.thumb_url : BG}
                                            alt="NoticeTemplate"
                                            className={`pointer ${selectedOptionId === item.id ? "selected-photo-item" : ""}`}
                                            width={133}
                                            height={133}
                                            style={{ borderRadius: "12px", border: "2px" }}
                                            onClick={() => {
                                                setShowProfileShow(true);
                                                setSelectedOptionId(item.id);
                                            }}
                                        />

                                    </div>
                                </>
                            );
                        })}

                    </div>
                
                        )
                        )}
                </div>
                {/* -------------------pagination-------------------,------- */}
                {allNoticeTemplateData && allNoticeTemplateData.data?.length == 0
                    ? ""
                    : allNoticeTemplateData.data?.length > 0 && (
                        <>
                            <hr className="" />
                            <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                                <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                                    {getLabel("totalRecords", initialLanguage)}:
                                    {/* <span> {allNoticeTitleData?.total}</span>{" "} */}
                                    <span> {20}</span>
                                </div>
                                <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end">
                                    <TestPage
                                        page={15}
                                        totalPages={200}
                                        handlePagination={handlePagination}
                                    />
                                </div>
                            </div>
                        </>
                    )}
            </div>


            {/* Add model */}
            <FullScreenModal
                show={addModelOpen}
                onClose={() => {
                    setAddModelOpen(false);
                }}
                title={getLabel("addNoticeTitle", initialLanguage)}
                description={getLabel("addNoticeTitleDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 submit-button"
                            ref={actionAddBtnRef}
                            disabled={addButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                    </div>
                }
            >
                <CreateNoticeTemplate actionAddBtnRef={actionAddBtnRef} handleCreate={handleCreate} response={response} />
            </FullScreenModal>

            {/* Edit model */}
            <FullScreenModal
                show={viewEditModelOpen}
                onClose={() => {
                    setViewEditModelOpen(false);
                }}
                title={getLabel("viewNoticeTemplate", initialLanguage)}
                description={getLabel("viewNoticeTemplateDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 me-4 submit-button"
                            ref={actionEditBtnRef}
                            disabled={editButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                        <div className="pointer text-danger close-button" onClick={() => setShowDeleteModal(true)}>
                            {getLabel("delete", initialLanguage)}
                        </div>
                    </div>
                }
            >
                <ViewEditNoticeTemplate actionEditBtnRef={actionEditBtnRef} handleEdit={handleEdit} viewId={viewId}  response={response} />
            </FullScreenModal>


            {/* delete modal */}
            <AlertModal
                show={showDeleteModal}
                isSmallModal={true}
                position="centered"
                title="Confirmation"
                onClose={() => {
                    setShowDeleteModal(false);
                }}
            >
                <div className="mb-4 d-flex conform-color">
                    Do you want to delete it?
                    {/* <span
            className="text-truncate ps-1 font-semibold"
            style={{
              maxWidth: "100px",
            }}
          >
            {deleteData.name}
          </span>
          ? */}
                </div>

                <div className="col-auto">
                    <button
                        className="btn btn-system-primary me-3"
                        onClick={() => setShowDeleteModal(false)}
                    >
                        No
                    </button>
                    <span
                        className="yes-delete-it pointer font-120"
                        onClick={() => {
                            setIsDeleteDisabled(true);
                            IsDeleteDisabled
                                ? null
                                : dispatch(
                                    deleteNoticeTemplate({
                                        activePartner: ACTIVE_PARTNER,
                                        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                                        id: viewId
                                    })
                                ).then((response: any) => {
                                    if (response.payload.success == true) {
                                        getAllData(page);
                                        setViewEditModelOpen(false);
                                        setIsDeleteDisabled(false);
                                        Notification.success({
                                            title: "Deleted Successfully",
                                            duration: 3000,
                                            content: undefined,
                                        });
                                        setShowDeleteModal(false);
                                    } else {
                                        setIsDeleteDisabled(false);
                                        Notification.error({
                                            title: "Delete Failed",
                                            duration: 3000,
                                            content: undefined,
                                        });
                                    }
                                });
                        }}
                    >
                        Yes, Delete it
                    </span>
                </div>
            </AlertModal>

            
            {/* show image model */}


           
            <FullScreenModal
                show={showNoticeTitle}
                onClose={() => {
                    setShowProfileShow(false);
                }}
                size="lg"
                title=""
            >
                <div className="d-flex justify-content-center">
                    <div className="fullmodal-image-container">
                        <Image
                            src={noticeTemplate}
                            alt="memorialprofile"
                            className="pointer image-contain"
                            width={750}
                            height={600}
                        />
                    </div>
                </div>
                
                <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 me-4 submit-button"
                            ref={actionEditBtnRef}
                            disabled={editButtonDisabled}
                        >
                            {getLabel("edit", initialLanguage)}
                        </Button>
                        <div className="pointer text-danger close-button" onClick={() => setShowDeleteModal(true)}>
                            {getLabel("delete", initialLanguage)}
                        </div>
                    </div>
            </FullScreenModal>
        
        </>
    );
};

export default NoticeTemplateComponent;

