import { Select, Spin } from "@arco-design/web-react";
import { useEffect, useState } from "react";

const CustomSelect = ({
  data,
  placeholderValue,
  onChange,
  is_search,
  defaultValue,
}: any) => {
  const [filteredData, setFilteredData] = useState<string[]>([]);
  // const [id, setId] = useState<string>("");
  const [value, setValue] = useState<string>("");

  console.log("defaultValue", defaultValue);

  useEffect(() => {
    const dataArray = Array.isArray(data) ? data : [data];
    setFilteredData(dataArray);
    const defaultOption = dataArray.find(
      (option) => option.id === defaultValue
    );
    if (defaultOption) {
      setValue(defaultOption.name);
    } else {
      setValue("");
    }
  }, [data, defaultValue]);
  const handleChange = (option: any) => {
    // setId(option.id);
    onChange(option.id);
    setValue(option.name);
  };
  const handleKeyPress = (event: { key: string }) => {
    const selectedOption = filteredData.find(
      (option: any) => option.name === event
    );
    if (selectedOption) {
      handleChange(selectedOption);
    }
  };
  return (
    <Select
      placeholder={placeholderValue}
      style={{ width: "100%" }}
      // allowClear
      showSearch={is_search}
      value={value !== "" ? value : undefined}
      notFoundContent={
        <>
          <li className="px-3 py-2 font-110">Sorry!! No results found.</li>
        </>
      }
      onChange={handleKeyPress}
      getPopupContainer={(node) => {
        const container = node.parentElement;
        if (container) {
          return container;
        }
        // Return a default element if needed
        // For example, you can return document.body as the fallback
        return document.body;
      }}
    >
      {Array.isArray(filteredData) &&
        filteredData.map((option: any) => (
          <Select.Option
            key={option.id}
            value={option.name}
            onClick={() => handleChange(option)}
          >
            {/* {option.name} */}
            {option ? (
              <p className="m-0 p-0"> {option?.name}</p>
            ) : (
              <>
                <div className="d-flex align-items-center justify-content-center">
                  <Spin />
                </div>
              </>
            )}
          </Select.Option>
        ))}
    </Select>
  );
};
export default CustomSelect;
