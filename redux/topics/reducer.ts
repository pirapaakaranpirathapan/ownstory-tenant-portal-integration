
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createCustomer, getAllCustomer, getOneCustomer } from "./actions";
import { IUser } from "../../interface/IUser";

interface IReducerInitialState {
  // data: (ICustomerPages)[] ;
  data: any;
  dataOne: IUser[];
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const customerPageReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all customer" action
  builder
    .addCase(getAllCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one customer" action
  builder
    .addCase(getOneCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data as any;
      state.dataOne[0] = payload?.data as any;
      // state.data[0] = payload?.data as any;
      // console.log("getOneCustomerpayload",payload?.data);
    })
    .addCase(getOneCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create customer" action
  builder
    .addCase(createCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(createCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

});
export default customerPageReducer;
