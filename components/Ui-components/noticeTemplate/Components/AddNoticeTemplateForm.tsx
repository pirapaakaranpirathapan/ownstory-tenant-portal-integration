import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { noticeTemplateSelector } from "../../../../redux/noticeTemplate";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";
import Error from "../../../Errors/Error";
import FileUploder from "../../../ImageUploder/FileUploder";
import { getLabel } from "../../../../utils/lang-manager";


interface AddNoticeTemplate {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string,description: string, thumb_url: string) => void;
    response: any;
}
const CreateNoticeTemplate = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddNoticeTemplate) => {
    const { error: apiError } = useAppSelector(noticeTemplateSelector);
    const errors = useApiErrorHandling(apiError, response);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        description:"",
        thumb_url:"",
        // language_id:"",
        // typeId:"",
        // client_Id:"",
        // is_premium:"",
        // has_header_background:"",
        // has_page_background:"",
        // notice_type_id:"",
        // model:"",
        // clip_arts:"",
        // borders:"",
        // frames: "",
        // header_backgrounds: "",
        // languages:"",
        // page_backgrounds:"",
        // tags:"",
        // template_groups:""
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
         ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.name, formData.slug, formData.description,formData.thumb_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);
    
    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            name: formData.name,
            thumb_url:url,
            slug: formData.slug,
            description:formData.description,
        });
    }



    return (
        <>
             <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("noticeTitle", initialLanguage)}
                            <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                      <Error error={errors.name} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("description", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                        <Error error={errors.slug} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Notice Template
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateNoticeTemplate;
