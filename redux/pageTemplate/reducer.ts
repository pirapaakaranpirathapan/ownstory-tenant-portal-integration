
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createPageTemplate, deletePageTemplate, getAllPageTemplate, getOnePageTemplate, updatePageTemplate, } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const pageTemplateReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all page template " action
  builder
    .addCase(getAllPageTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllPageTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllPageTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one page template " action
  builder
    .addCase(getOnePageTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOnePageTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOnePageTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create page template " action
  builder
    .addCase(createPageTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createPageTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createPageTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update page template " action
  builder
    .addCase(updatePageTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updatePageTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updatePageTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete page template " action
  builder
    .addCase(deletePageTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deletePageTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deletePageTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default pageTemplateReducer;
