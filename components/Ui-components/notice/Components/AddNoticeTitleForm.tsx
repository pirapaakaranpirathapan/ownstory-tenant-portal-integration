import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { noticeTitleSelector } from "../../../../redux/notice";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";
import Error from "../../../Errors/Error";


interface AddNoticeTitle {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string, description: string, language_id: string, typeId: string, client_Id: string,) => void;
    response: any;
}
const CreateNoticeTitle = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddNoticeTitle) => {
    const { error: apiError } = useAppSelector(noticeTitleSelector);
    const errors = useApiErrorHandling(apiError, response);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        description: "",
        language_id: "",
        typeId: "",
        client_Id: ""
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.name, formData.slug, formData.description, formData.language_id, formData.typeId, formData.client_Id);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);



    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                       <Error error={errors.name} />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Description <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="description"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.description}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.description[0]}
                        </div>
                    </div>

                 
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Languages <span className="font-semibold">*</span></div>
                        <select
                            name="language_id"
                            className="form-control border border-2 p-2 system-control"
                            value={formData.language_id}
                            onChange={handleChange}
                        >
                            <option value="">English</option>
                            <option value="option1">Tamil</option>
                            <option value="option2">Singala</option>         
                        </select>
                        <div className="row align-items-start m-0 text-danger">
                            {apiError?.error?.language_id[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Notice <span className="font-semibold">*</span></div>
                        <select
                            name="typeId"
                            className="form-control border border-2 p-2 system-control"
                            value={formData.typeId}
                            onChange={handleChange}
                        >
                            <option value="">Select...</option>
                            <option value="option1">Rip</option>
                            <option value="option2">Option 2</option>         
                        </select>
                        <div className="row align-items-start m-0 text-danger">
                            {apiError?.error?.typeId[0]}
                        </div>
                    </div>

                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            client  <span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="client_Id"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.client_Id}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.client_Id[0]}
                        </div>
                    </div>


                </div>
            </div>
        </>
    );
};

export default CreateNoticeTitle;
