
import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_ROUTE } from "../../config/constants";
import { get } from "../../connections/fetch_wrapper";

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular Countries, and depending on the response, either returns the data or an error message.
export const getAllCountries = createAsyncThunk(
  "countries",
  async () => {
    try {
      const response = await get(`${BASE_ROUTE}/countries`);

      if (response.code == 200) {
        return response;
      } else {
        rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
function rejectWithValue(msg: string): any {
  throw new Error("Function not implemented.");
}
