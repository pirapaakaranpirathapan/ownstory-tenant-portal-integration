"use client"
import Head from 'next/head'
import { Key, useState } from 'react'
import folder from '../../../public/images/folder2.png'
import folder2 from '../../../public/images/folder2.png'
import folder3 from '../../../public/images/folder3.png'
import folder4 from '../../../public/images/folder4.png'
import { useAppDispatch } from '../../../redux/store/hooks'
import { getMe } from '../../../redux/me'
import { getLabel } from '../../../utils/lang-manager'
import DashboardCard from '../../../components/cards/DashboardCard'

export default function DashBoard({ initialLanguage }: any) {
    console.log("initialLanguage", initialLanguage);

    let sampleData = [
        {
            image: folder,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("memorialPage", initialLanguage)}`,
            description: `${getLabel("dasMemorialDes", initialLanguage)}`,
        },
        {
            image: folder,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("deathNotice", initialLanguage)}`,
            description: `${getLabel("dasDeathNoticeDes", initialLanguage)}`,
        },
        {
            image: folder2,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("otherNotices", initialLanguage)}`,
            description: `${getLabel("dasOtherNoticesDes", initialLanguage)}`,
        },
        {
            image: folder,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("remembrance", initialLanguage)}`,
            description: `${getLabel("dasRemembranceDes", initialLanguage)}`,
        },
        {
            image: folder4,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("tributes", initialLanguage)}`,
            description: `${getLabel("dasTributesDes", initialLanguage)}`,
        },
        {
            image: folder3,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("birthMemorial", initialLanguage)}`,
            description: `${getLabel("dasbirthMemorialDes", initialLanguage)}`,
        },
        {
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("prayerNotice", initialLanguage)}`,
            description: `${getLabel("dasPrayerNoticeDes", initialLanguage)}`,
        },
        {
            image: folder,
            amount: 80,
            amountsymbol: '£',
            title: `${getLabel("otherServices", initialLanguage)}`,
            description: `${getLabel("dasOtherServicesDes", initialLanguage)}`,
        },
    ] as any
    const [data, SetData] = useState(sampleData)
    // console.table(sampleData);
    const dispatch = useAppDispatch()
    const loadData = () => {
        dispatch(getMe())
    }

    // useEffect(() => {
    //   loadData()
    // }, [])


    return (
        <>
            <Head>
                <title>Blackbee | Dashboard</title>
            </Head>
            <div className="">
                <div className=" pe-0">
                    <div className="page-header mb-3">
                        <h1 className="page-heading ">{getLabel("createAnOrder", initialLanguage)}</h1>
                        <p className="page-heading-sub text-muted">
                            {getLabel("CustomerWillReceiveAUnique", initialLanguage)}
                        </p>
                    </div>
                    <div className="row d-lg-none">
                        {data.map((data: any, index: Key | null | undefined, array: string | any[]) => {
                            const isFirstChild = index === 0;
                            const isLastChild = index === array.length - 1;

                            const className = isFirstChild ? "first-style col-30 col-20 col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-3" : isLastChild ? "last-style col-30 col-20 col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-3" : "col-30 col-20 col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-12 mb-3";

                            return (
                                <div key={index} className={className}>
                                    <DashboardCard content={data} />
                                </div>
                            );
                        })}
                    </div>
                    <div className="d-none d-lg-flex gap-3 flex-wrap dash-child">
                        {data.map((data: any, index: Key | null | undefined, array: string | any[]) => {
                            const isFirstChild = index === 0;
                            const isLastChild = index === array.length - 1;

                            const className = isFirstChild ? "first-style" : isLastChild ? "last-style" : "";

                            return (
                                <div key={index} className={className}>
                                    <DashboardCard content={data} />
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    )
}
