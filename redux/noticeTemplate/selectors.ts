import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const noticeTemplateState = (state: RootState) => state.noticeTemplateReducer

export const noticeTemplateSelector = createSelector(
  noticeTemplateState,
  state => state
)
