import React, { useEffect, useState } from "react";
import {
    getOneRelationship,
    relationshipSelector,
} from "../../../../redux/relationship";
import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import Error from "../../../Errors/Error";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";

interface ViewEditRelationship {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (name: string, slug: string) => void;
    response: any;
}
const UpdateRelationship = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
    response
}: ViewEditRelationship) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
    });
    // error handle
    const { dataOne: oneRelationshipsData, error: apiError } = useAppSelector(relationshipSelector);
    const errors = useApiErrorHandling(apiError, response);

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                handleEdit(formData.name, formData.slug);
                setClickStatus(false);

            };
        }
    }, [clickStatus, handleEdit, handleChange]);


    // single view get
    useEffect(() => {
        loadData();
    }, [viewId]);

    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        dispatch(
            getOneRelationship({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                id: viewId,
            })
        ).then((res: any) => {
            if (res.payload.code === 200) {
                setSkelton(false);
            }
        });
    };

    useEffect(() => {
        setFormData({
            name: oneRelationshipsData[0]?.name,
            slug: oneRelationshipsData[0]?.slug
        })
    }, [skelton]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <>
                                <input
                                    type="text"
                                    className="form-control border border-2 p-2 system-control "
                                    placeholder="Name"
                                    name="name"
                                    value={formData.name}
                                    onChange={handleChange}
                                />
                                <Error error={errors.name} />
                            </>
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Slug"
                                name="slug"
                                value={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default UpdateRelationship;
