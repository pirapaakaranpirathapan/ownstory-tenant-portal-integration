
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createPageBackground,  deletePageBackground,  getAllPageBackground, getOnePageBackground, updatePageBackground } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const pageBackgroundReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all relationship" action
  builder
    .addCase(getAllPageBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllPageBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllPageBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one relationship" action
  builder
    .addCase(getOnePageBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOnePageBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOnePageBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create relationship" action
  builder
    .addCase(createPageBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createPageBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createPageBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update relationship" action
  builder
    .addCase(updatePageBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updatePageBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updatePageBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete relationship" action
  builder
    .addCase(deletePageBackground.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deletePageBackground.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deletePageBackground.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default pageBackgroundReducer;
