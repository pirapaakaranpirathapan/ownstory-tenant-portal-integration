// import { IPagination } from '../manage/IPagination';


export interface IUser {
  identifier_value: any;
  birth_certificate: any;
  center_leader: string;
  guardian_nic: any;
  guardian_name: any;
  note: any;
  child_account: number;
  withdrawable_amount: string;
  addresses: any;
  identifier: any;
  user_id: any;
  users_id: any;
  id: string,
  username: string,
  first_name?: string,
  full_name?: string,
  middle_name?: string,
  last_name?: string,
  dp: string,
  email: string,
  province: string,
  dob: string,
  type: string,
  city: string,
  status: string,
  maritial_status: string,
  residential_address: string,
  gender: string,
  gs_division: string,
  signed_for_customer: string,
  official_address: string,
  district: string,
  nationality: string,
  contact_primary?: string,
  contact_secondary?: string,
  nic_no?: string,
  passport_no?: string,
  code: string,
  party_id: string,
  title: string,
}

export interface IUserList {
  data: (IUser)[],
  // pagination: IPagination | null
}