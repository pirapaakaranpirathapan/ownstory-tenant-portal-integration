// LanguageManager.ts

import { ARABIC } from "../config/languages/arabic";
import { ENGLISH } from "../config/languages/english";
import { SINHALA } from "../config/languages/sinhala";
import { TAMIL } from "../config/languages/tamil";


export const getLabel = (labelKey: string, initialLanguage: string): string => {
    let langConstants = {} as any;
  if (initialLanguage === 'Tamil') {
    langConstants = TAMIL;
  } else if (initialLanguage === 'Sinhala, Sinhalese') {
    langConstants = SINHALA;
  } else if (initialLanguage === 'Arabic') {
    langConstants = ARABIC;
  } else {
    langConstants = ENGLISH;
  }

  return langConstants?.config[labelKey] || '';
};





// import { ENGLISH } from '@/config/languages/english';
// import { TAMIL } from '@/config/languages/tamil'
// import { SINHALA } from '@/config/languages/sinhala'
// import { ARABIC } from '@/config/languages/arabic'
// import { DEFAULT_LANGUAGE } from '@/config/constants';

// export const getLabel = ((labelKey: string): string => {
    
//     let langConstants = {} as any;
//     let currentLang = DEFAULT_LANGUAGE;

//     if (currentLang === 'Tamil') {
//         langConstants = TAMIL;
//     } else if (currentLang === 'Sinhala, Sinhalese') {
//         langConstants = SINHALA;
//     } else if (currentLang === 'Arabic') {
//         langConstants = ARABIC;
//     } else {
//         langConstants = ENGLISH;
//     }

//     return langConstants?.config[labelKey] || "";
// });