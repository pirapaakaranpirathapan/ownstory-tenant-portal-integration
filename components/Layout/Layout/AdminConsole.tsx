
"use client"
import React, { ReactNode, useEffect, useRef, useState } from "react";
import Sidebar from "../Sidebar/Sidebar";
import Header from "../Header/Header";
import { getCookie } from "cookies-next";
import { getAllCountries } from "../../../redux/countries";
import { useAppDispatch } from "../../../redux/store/hooks";
import { getAllServiceProvider } from "../../../redux/service-providers";
import { axiosInstance } from "../../../connections/api_connection";
import Loader from "../../Loader/Loader";
import AdminConsoleSidebar from "../Sidebar/AdminConsoleSidebar";
type Props = {
    children?: ReactNode;
    title?: string;
};
const AdminConsole = ({ children }: Props) => {
    const [languageInitial, setLanguageInitial] = useState("");
    const [sideBar, setSideBar] = useState<boolean>(true);
    const popupRef = useRef<HTMLDivElement>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [title, setTitle] = useState("");
    const [type, setType] = useState("");
    // const router = useRouter();
    const tokenCookie = getCookie("token");
    const dispatch = useAppDispatch();

    useEffect(() => {
        const initialLanguage: string = (getCookie("language") as string) || "";
        setLanguageInitial(initialLanguage);
    }, []);

    useEffect(() => {
        function handleClickOutside(event: MouseEvent) {
            if (
                popupRef.current &&
                !popupRef.current.contains(event.target as Node)
            ) {
                if (window.innerWidth < 992) {
                    setSideBar(false);
                }
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [popupRef]);

    const toggleSideBar = () => {
        setSideBar(!sideBar);
    };

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth >= 998) {
                setSideBar(true);
                setIsLoading(false);
            } else {
                setSideBar(false);
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000);
            }
        };
        handleResize();
        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    useEffect(() => {
        if (!tokenCookie) {
            // router.push("/");
        }
    }, [tokenCookie]);

    useEffect(() => {
        dispatch(getAllServiceProvider());
        dispatch(getAllCountries());
        // dispatch(getAllNoticeTypes({ active_partner: ACTIVE_PARTNER }));
        // dispatch(getAllManagerTypes());
        // dispatch(getAllInterfaceLanguages());
    }, []);

    const updateError = (message: string | null) => {
        if (message === "Unauthorized error") {
            setErrorMessage(message);
        } else {
            setErrorMessage(message ? "Something went wrong.." : "");
        }
    };

    useEffect(() => {
        if (!navigator.onLine) {
            updateError("You are currently offline.");
            setTitle("No Internet");
            setType("warning");
            return;
        }
        const interceptor = axiosInstance.interceptors.response.use(
            (response) => {
                // response.data.success && updateError(null);
                return response;
            },
            (error) => {
                if (error.response && error.response.status === 429) {
                    console.log(error.response.config.url);
                    updateError(error.response.data ? error.response.data.message : null);
                } else if (
                    error.response &&
                    error.response.data.message === "Unauthorized error"
                ) {
                    updateError(error.response.data ? error.response.data.message : null);
                }
                return Promise.reject(error);
            }
        );
        return () => {
            axiosInstance.interceptors.response.eject(interceptor);
        };
    }, []);

    const handleAlertClose = () => {
        setErrorMessage(null);
        window.location.reload();
    };

    const handleLinkClick = () => {
        if (window.innerWidth < 992) {
            setSideBar(false);
        }
    };

    return (
        <>
            {isLoading ? (
                <Loader />
            ) : (
                <div className="system-container">
                    <div ref={popupRef}>
                        <AdminConsoleSidebar
                            showBar={sideBar}
                            setIsLoading={setIsLoading}
                            initialLanguage={languageInitial}
                            handleLinkClick={handleLinkClick}
                        />
                    </div>
                    <main
                        className={`position-relative ${sideBar ? "container bg-white" : "default-close"
                            }`}
                    >
                        <Header
                            toggleSideBar={toggleSideBar}
                            initialLanguage={languageInitial}
                        />
                        <div className="main-container overflow-x">

                            <div className="m-sm-4 px-3 mt-3 ">{children}</div>
                        </div>
                    </main>
                </div>
            )}
        </>
    );
};

export default AdminConsole;
