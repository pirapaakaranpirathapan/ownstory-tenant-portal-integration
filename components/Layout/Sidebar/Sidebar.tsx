"use client"
import Image from "next/image";
import Link from "next/link";
import updown from "../../../public/images/updown.svg";
import logo from "../../../public/images/logo.svg";
import Noticevj from "../../../public/Noticevj.svg";
import { useCallback, useEffect, useRef, useState } from "react";

import { getCookie, setCookie } from "cookies-next";
import { getLabel } from "../../../utils/lang-manager";
import { useAppDispatch, useAppSelector } from "../../../redux/store/hooks";
import { serviceProviderSelector } from "../../../redux/service-providers";
import { usePathname, useRouter } from 'next/navigation'
type Props = {
  showBar: boolean;
  setIsLoading: any;
  initialLanguage: string;
  handleLinkClick?: () => void;
};
interface ServiceProvider {
  uuid: string;
}
interface Partner {
  uuid: string;
  service_providers: ServiceProvider[];
}

const Sidebar: React.FC<Props> = ({ showBar, setIsLoading, initialLanguage, handleLinkClick }) => {
  const [visibleServiceProviders, setVisibleServiceProviders] = useState(15);
  const dropdownRef = useRef<HTMLDivElement | null>(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [activeData, setActiveData] = useState<Partner[]>([]);
  const [activeServiceProvider, setActiveServiceProvider] = useState<{ name: string, id: string }>({ name: '', id: '' });
  const [cookiesSet, setCookiesSet] = useState(false);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const pathname = usePathname();


  const dispatch = useAppDispatch();
  const popupRef = useRef<HTMLDivElement>(null);
  const { data: getData } = useAppSelector(serviceProviderSelector);

  useEffect(() => {
    if (Array.isArray(getData) && getData.length > 0) {
      const activePartnerIndex = getData.findIndex((partner) =>
        partner.service_providers.some((provider: { uuid: string; }) => isActiveId(provider.uuid))
      );
      if (activePartnerIndex !== -1) {
        const activePartner = getData[activePartnerIndex];
        const partnersWithoutActive = [
          ...getData.slice(0, activePartnerIndex),
          ...getData.slice(activePartnerIndex + 1),
        ];
        const sortedServiceProviders = activePartner.service_providers.slice().sort((a: { uuid: string; }, b: { uuid: string; }) => {
          if (isActiveId(a.uuid)) return -1;
          if (isActiveId(b.uuid)) return 1;
          return 0;
        });
        const sortedActivePartner = {
          ...activePartner,
          service_providers: sortedServiceProviders,
        };
        const sortedData = [sortedActivePartner, ...partnersWithoutActive];
        setActiveData(sortedData);
      } else {
        const sortedData = getData.slice().sort((a, b) => {
          const hasActiveServiceProviderA = a.service_providers.some((provider: { uuid: string; }) => isActiveId(provider.uuid));
          const hasActiveServiceProviderB = b.service_providers.some((provider: { uuid: string; }) => isActiveId(provider.uuid));
          if (hasActiveServiceProviderA) return -1;
          if (hasActiveServiceProviderB) return 1;
          return 0;
        });
        setActiveData(sortedData);
      }
    }
  }, [getData, activeServiceProvider]);

  useEffect(() => {
    if (isDropdownOpen) {
      setSearchTerm("")
    }
  }, [isDropdownOpen])

  useEffect(() => {
    const activeServiceProviderFromCookie = getCookie('serviceprovidername') as string;
    const activeServiceProviderIdFromCookie = getCookie('activeserviceprovider') as string;
    if (activeServiceProviderFromCookie) {
      setActiveServiceProvider({ name: activeServiceProviderFromCookie, id: activeServiceProviderIdFromCookie });
    }
  }, []);

  useEffect(() => {
    const lastAccessedPartnerId = getCookie('activepartner') as string;
    const lastAccessedServiceProviderId = getCookie('activeserviceprovider') as string;
    if (lastAccessedPartnerId && lastAccessedServiceProviderId) {
      setActivePartnerFromCookies(lastAccessedPartnerId, lastAccessedServiceProviderId);
    } else if (Array.isArray(getData) && getData.length > 0) {
      const defaultPartner = getData[0];
      const defaultServiceProvider = defaultPartner.service_providers[0];
      const partnerId = defaultPartner.uuid;
      const serviceProviderId = defaultServiceProvider.uuid;
      const serviceProviderName = defaultServiceProvider.name;
      setCookie('serviceprovidername', serviceProviderName);
      setCookie('activepartner', partnerId);
      setCookie('activeserviceprovider', serviceProviderId);
      setActiveServiceProvider({ name: serviceProviderName, id: serviceProviderId });
    }
  }, [getData]);

  function setActivePartnerFromCookies(partnerId: string, serviceProviderId: string) {
    const activePartner = Array.isArray(getData) && getData.find((partner: { uuid: string; }) => partner.uuid === partnerId);
    if (activePartner) {
      const activeServiceProvider = activePartner.service_providers.find((serviceProvider: { uuid: string; }) => serviceProvider.uuid === serviceProviderId);
      if (activeServiceProvider) {
        setActiveServiceProvider({ name: activeServiceProvider.name, id: serviceProviderId });
      }
    }
  }

  const HandleUpdateActive = (partnerId: any, serviceProviderId: any, serviceProviderName: any) => {
    setCookie('activepartner', partnerId);
    setCookie('activeserviceprovider', serviceProviderId);
    setCookie('serviceprovidername', serviceProviderName);
    setActiveServiceProvider({ name: serviceProviderName, id: serviceProviderId });
    window.location.reload();
    window.location.href = '/dashboard'
    // setIsLoading(false);
  };

  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (
        popupRef.current &&
        !popupRef.current.contains(event.target as Node)
      ) {
        setIsDropdownOpen(false);
      }
    }
    function handlePopstate() {
      setIsDropdownOpen(false);
    }
    document.addEventListener("mousedown", handleClickOutside);
    window.addEventListener('popstate', handlePopstate);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
      window.removeEventListener('popstate', handlePopstate);
    };
  }, [popupRef]);

  useEffect(() => {
    const handleScroll = () => {
      if (dropdownRef.current) {
        const { scrollTop, scrollHeight, clientHeight } = dropdownRef.current;
        if (scrollTop + clientHeight >= scrollHeight - 10) {
          loadMoreServiceProviders();
        }
      }
    };
    if (dropdownRef.current) {
      dropdownRef.current.addEventListener('scroll', handleScroll);
    }
    return () => {
      if (dropdownRef.current) {
        dropdownRef.current.removeEventListener('scroll', handleScroll);
      }
    };
  }, [dropdownRef, isDropdownOpen]);

  function toggleDropdown() {
    setIsDropdownOpen(!isDropdownOpen);
  }

  const isActiveLink = (href: string) => {
    if (pathname.startsWith(`${href}/`)) {
      return true;
    }
    return false;
  };

  const isActiveId = (serviceProviderUuid: string) => {
    return activeServiceProvider.id === serviceProviderUuid;
  };

  const handleSearch = (event: any) => {
    const searchValue = event.target.value;
    setSearchTerm(searchValue);
    const filteredData = getData?.filter((item: any) => {
      const itemName = item.name.toLowerCase();
      return itemName.includes(searchValue.toLowerCase());
    });
    setFilteredData(filteredData);
  };

  const loadMoreServiceProviders = () => {
    setVisibleServiceProviders((prevVisibleServiceProviders) => prevVisibleServiceProviders + 15);
  };

  return (
    <div
      className={`sidebar-default mini-scrollbar  shadow-sm ${showBar ? "open" : "closed"
        }`}
    >
      <div>
        <div className="switch-dropdown pointer" ref={popupRef}>
          <div onClick={toggleDropdown} className="switch-container p-1 ps-3 pe-3 d-flex justify-content-between align-items-center font-normal w-100">
            <div className="d-flex align-items-center">
              <div className="col-auto switch-ratio d-flex align-items-center justify-content-center rounded system-grey font-semibold font-90">
                {activeServiceProvider ? activeServiceProvider?.name[0] : "A"}
              </div>
              <div className="px-2 font-110 font-bold text-truncate switch-providers">
                {activeServiceProvider?.name}
              </div>
            </div>
            <div className="d-flex align-items-center">
              <Image src={updown} alt="updown" className="w-100" />
            </div>
          </div>
          {isDropdownOpen && (
            <ul className="switch-content" >
              <div className="search-box-side">
                <input
                  type="search"
                  id="sidebar-search"
                  onChange={handleSearch}
                  className="form-control"
                  placeholder="Search..."
                />
              </div>
              <div className="overflow-xx" ref={dropdownRef}>
                {Array.isArray(activeData) && activeData?.map((item: any, index: any) => {
                  const filteredServiceProviders =
                    item.service_providers?.filter((serviceProvider: any) =>
                      serviceProvider.name
                        .toLowerCase()
                        .includes(searchTerm.toLowerCase())
                    );
                  return (
                    <>
                      <div
                        className="mx-2 my-1 font-90 system-icon-secondary"
                        key={index}
                      >
                        {item?.name}
                      </div>
                      <div>
                        {Array.isArray(filteredServiceProviders) &&
                          filteredServiceProviders.slice(0, visibleServiceProviders).map((serviceProvider, serviceProviderIndex) => (
                            <li
                              className={`rounded mb-1 ${isActiveId(serviceProvider.uuid) ? 'active-service-provider' : 'li-hover'}`}
                              key={serviceProviderIndex}
                              onClick={() => HandleUpdateActive(item?.uuid, serviceProvider.uuid, serviceProvider.name)}
                            >
                              <div className="row m-0 w-100 align-items-center">
                                <div className="col-2 switch-ratio mx-2 d-flex align-items-center justify-content-center rounded system-grey font-semibold font-90">
                                  {serviceProvider.name ? serviceProvider.name[0] : 'A'}
                                </div>
                                <div className="col-9 p-0 text-truncate font-semibold font-100 switch-list">
                                  {serviceProvider.name}
                                </div>
                              </div>
                            </li>
                          ))}
                      </div>
                    </>
                  );
                })}
              </div>
            </ul>
          )}


        </div>
      </div>

      <ul className="navbar-nav pt-3 overflow-auto">
        <Link
          href="/dashboard"
          className="text-decoration-none text-dark lh-lg font-normal  "
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${pathname === "/dashboard" ? "font-bold" : "link"
              }`}
          >
            {getLabel("dashboard", initialLanguage)}
          </li>
        </Link>

        <Link
          href="/customers?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/customers") ? "font-bold" : "link"
              }`}
          >
            {getLabel("customers", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/memorials?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/memorials") ? "font-bold" : "link"
              }`}
          >
            {getLabel("memorialPages", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/notices?p=1"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/notices") ? "font-bold" : "link"
              }`}
          >
            {getLabel("notices", initialLanguage)}
          </li>
        </Link>
        <div className=" disabled py-1 lh-lg pt-4 mx-2 px-3 rounded system-icon-secondary font-80 font-normal">
          {getLabel("manage", initialLanguage)}
        </div>
        <Link
          href="/billings"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/billings") ? "font-bold" : "link"
              }`}
          >
            {getLabel("billings", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/reports"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/reports") ? "font-bold" : "link"
              }`}
          >
            {getLabel("reports", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/setting"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/setting") ? "font-bold" : "link"
              }`}
          >
            {getLabel("settings", initialLanguage)}
          </li>
        </Link>
        <Link
          href="/user-access"
          className="text-decoration-none text-dark lh-lg font-normal"
          onClick={handleLinkClick}
        >
          <li
            className={`py-1 lh-lg mx-2 px-3 rounded ${isActiveLink("/user-access") ? "font-bold" : "link"
              }`}
          >
            {getLabel("usersAccess", initialLanguage)}
          </li>
        </Link>
      </ul>
      <div className=" position-absolute bottom-0 logo-container d-flex border-top d-flex  align-items-center w-100">
        <div className=" system-secondary-3 p-2 mx-3 rounded d-flex align-items-center">
          <Image src={logo} alt="Rootify" className="" />
        </div>
        <div className="font-bolder font-110 system-success-dark">ROOTIFY</div>
      </div>
    </div>
  );
};

export default Sidebar;