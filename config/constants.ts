import { getCookie } from "cookies-next";

export const BASE_ROUTE = 'https://dev.zoftline.com/api/v1/admin'
// export const BASE_ROUTE='v1/aBZu7ui0/partner-admin'
// export const ACTIVE_PARTNER: string = getCookie('activepartner') as string;
export const ACTIVE_PARTNER: string = "aBZu7ui0";
export const ACTIVE_SERVICE_PROVIDER: string = getCookie('activeserviceprovider') as string;

//LANGUAGE
// export const DEFAULT_LANGUAGE: string = getCookie('language')as string;

