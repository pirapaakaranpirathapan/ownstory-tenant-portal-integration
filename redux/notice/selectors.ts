import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const noticeTitleState = (state: RootState) => state.noticeTitleReducer

export const noticeTitleSelector = createSelector(
  noticeTitleState,
  state => state
)
