
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createNoticeTemplate, deleteNoticeTemplate, getAllNoticeTemplate, getOneNoticeTemplate, updateNoticeTemplate } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const noticeTemplateReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all notice template" action
  builder
    .addCase(getAllNoticeTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllNoticeTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllNoticeTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one notice template" action
  builder
    .addCase(getOneNoticeTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneNoticeTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneNoticeTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create notice template" action
  builder
    .addCase(createNoticeTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createNoticeTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createNoticeTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update notice template" action
  builder
    .addCase(updateNoticeTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateNoticeTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateNoticeTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete notice template" action
  builder
    .addCase(deleteNoticeTemplate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteNoticeTemplate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteNoticeTemplate.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default noticeTemplateReducer;
