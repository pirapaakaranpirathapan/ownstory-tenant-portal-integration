import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { getLabel } from "../../../../utils/lang-manager";
import { Upload } from "@arco-design/web-react";
import FileUploder from "../../../ImageUploder/FileUploder";
import { clipartSelector } from "../../../../redux/clipart";
import Error from "../../../Errors/Error";
import { useApiErrorHandling } from "../../../Errors/useApiErrorHandling";

interface AddClipart {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string, description:string, thumb_url: string, left_url: string, left_print_url: string, right_url: string, right_print_url: string) => void;
    response: any
}
const CreateClipart = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddClipart) => {

    // error handle
    const { error: apiError } = useAppSelector(clipartSelector);
    const errors = useApiErrorHandling(apiError, response);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        description: "",
        thumb_url: "",
        left_url: "",
        left_print_url: "",
        right_url: "",
        right_print_url: "",

    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.name, formData.slug, formData.description, formData.thumb_url, formData.left_url, formData.left_print_url, formData.right_url, formData.right_print_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);

    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            name: formData.name,
            thumb_url: url,
            slug: formData.slug,
            description: formData.description,
            left_url: url,
            left_print_url: url,
            right_url: url,
            right_print_url: url
        });
    }

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("clipArtName", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.name[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("slug", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("description", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="description"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.description}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.description[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("thumbUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="thumb_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.thumb_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.thumb_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("leftUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="left_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.left_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.left_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("leftPrintUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="left_print_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.left_print_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.left_print_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("rightUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="right_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.right_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.right_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("rightPrintUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="right_print_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.right_print_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.right_print_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            ClipArt
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateClipart;
