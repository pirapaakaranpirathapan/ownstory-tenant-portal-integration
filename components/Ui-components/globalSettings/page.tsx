import React, { useEffect, useState } from "react";


import { useAppDispatch } from "../../../redux/store/hooks";


// interface ViewEditVGlobalSettings {
//     actionEditBtnRef: any;
//     viewId: string
//     handleEdit:() => void;
//     response: any;
// }
// const ViewEditVGlobalSettings = ({
export default function ViewEditVGlobalSettings() {

    return (

        <>
            <div className="admin-console-heading">
                <b>Global settings</b>
            </div>
            <hr />
            <div className="global-setting_title">
                Platform Settings
            </div>
            <div className="row  align-items-center  m-0 col-6">
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">
                        <div className="font-semibold">Platform Name</div>
                    </div>
                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="name"
                    />
                </div>
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">Brand Logo</div>
                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="slug"
                    />

                </div>
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">Brand Favicon</div>

                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="description"
                    />
                </div>
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">Platform Site URL</div>
                    <div className="input-group mb-3">
                    <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">https://</span>
                     </div>
                        <input type="text" className="form-control" id="basic-url" aria-describedby="basic-addon3" />
                   </div>
                </div>

            </div>
            <hr />
            <div className="global-setting_title">
                SMTP Settings
            </div>
            <div className="row  align-items-center  m-0 col-6">
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">
                        <div className="font-semibold">SMTP host</div>
                    </div>
                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="name"
                    />
                </div>
            </div>

            <hr />



            <div className="global-setting_title">
                Email Settings
            </div>
            <div className="row  align-items-center  m-0 col-6">
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">
                        <div className="font-semibold">Sender Name</div>
                    </div>
                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="name"
                    />
                </div>
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">Sender Email</div>
                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="slug"
                    />

                </div>

                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">No-reply Email</div>

                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="description"
                    />
                </div>
                <div className="col-md-12 col-12 p-0 mb-3">
                    <div className="font-semibold">Signature</div>

                    <input
                        type="text"
                        className="form-control border border-2 p-2 system-control "

                        name="description"
                    />
                </div>
                <div className="global-setting_title">
                    Notification Settings
                </div>
            </div>

            <div className="notification-setting-condition">
                <div className="notification-setting-condition-container">
                    <p><b>By Email</b></p>
                    <div>
                        <div className="d-flex gap-2">
                            <div className="">
                                <input type="checkbox"></input>
                            </div>
                            <div className="">
                                <p className="m-0 p-0"><b>Comments</b></p>
                                <p className="m-0 p-0">Get notified when someones posts a comment on a posting.</p>
                            </div>
                        </div>
                        <div className="d-flex gap-2">
                            <div className="">
                                <input type="checkbox"></input>
                            </div>
                            <div className="">
                                <p className="m-0 p-0"><b>Comments</b></p>
                                <p className="m-0 p-0">Get notified when a candidate applies for a job.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div className="pt-3">
            <button type="button" className="btn btn-primary">Save</button>
            </div>




        </>
    );
};

