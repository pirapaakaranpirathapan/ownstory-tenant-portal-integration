import React from "react";
import ShimmerEffect from "./ShimmerEffect";
const EventTypeSkelton = ({ loopTimes }: any) => {
  return (
    <>
      <div
        className="d-none d-sm-block skelton-disable"
        style={{ cursor: "default", pointerEvents: "none" }}
      >
        <table className="table mt-3 ">
          <tbody className="">
            {Array.from({ length: loopTimes }).map((_, index) => {
              return (
                <tr className="align-middle ps-0">
                  <td className="col-3 ps-0" style={{ maxWidth: "80px" }}>
                    <td className="col-7 p-0" style={{ maxWidth: "80px" }}>
                      <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                        <div className="p-0 m-0 text-truncate ">
                          {" "}
                          <ShimmerEffect width="80px" height="15px" />
                        </div>
                      </div>
                    </td>
                  </td>
                  <td className="col-7 p-0" style={{ maxWidth: "80px" }}>
                    <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                      <div className="p-0 m-0 text-truncate ">
                        {" "}
                        <ShimmerEffect width="80px" height="15px" />
                      </div>
                    </div>
                  </td>
                  <td className=" col-2 p-2">
                    <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                      <button
                        type="button"
                        className=" d-none d-sm-flex me-3 shimmer-button shimmer-effect"
                      ></button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default EventTypeSkelton;
