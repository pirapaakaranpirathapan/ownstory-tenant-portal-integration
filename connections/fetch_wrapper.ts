
import { IApiResponse } from '../interface/manage/IApiResponse';
import {
    axiosInstance,
    axiosInstanceFormData,
    axiosInstanceServer,
} from './api_connection';

export async function get(url: string) {
    const response = await axiosInstance
        .get(url)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'] ?? 0,
            };
            return data;
        })
        .catch(function (error) {
            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                error: error.response.data,
                success: false,
            };
            return data;
        });
    return response;
}

export async function post(url: string, body: any, config?: any) {
    const response = await axiosInstance

        .post(url, body, config)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data,
                error: null,
                success: true,
                count: response.data['count'],
            };
            return data;
        })
        .catch(function (error) {
            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                error: error.response.data.errors,
                success: false,
            };

            return data;

        });
    return response;

}
export async function postFormData(url: string, body: any) {
    const response = await axiosInstanceFormData
        .post(url, body)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'],
            };
            return data;
        })
        .catch(function (error) {
            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                error: error.response.data.error,
                success: false,
            };
            return data;
        });
    return response;
}

export async function put(url: string, body: any) {
    const response = await axiosInstance
        .put(url, body)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'],
            };
            return data;
        })
        .catch(function (error) {
            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                error: error.response.data,
                success: false,
            };
            return data;
        });
    return response;
}
export async function patch(url: string, body: any) {
    const response = await axiosInstance

        .patch(url, body)
        .then(function (response) {
            // console.log('patch-response1fetch', response);
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'],
            };
            // console.log('patch-response', data);

            return data;

        })
        .catch(function (error) {
            // console.log('patch-response error', error);

            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                // error: error.response.data,
                error: error.response.data.errors,
                success: false,
            };
            return data;
        });
    return response;

}

export async function deleteMethod(url: string) {
    const response = await axiosInstance
        .delete(url)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'],
            };
            return data;
        })
        .catch(function (error) {
            const data: IApiResponse = {
                msg: error.response.data.message,
                code: error.response.status,
                data: null,
                error: error.response.data,
                success: false,
            };
            return data;
        });
    return response;
}

export async function getServerProps(url: string) {
    const response = await axiosInstanceServer
        .get(url)
        .then(function (response) {
            const data: IApiResponse = {
                msg: response.data['message'],
                code: 200,
                data: response.data['data'],
                error: null,
                success: true,
                count: response.data['count'] ?? 0,
            };
            return data;
        })
        .catch(function (error) {
            console.log(error);

            const data: IApiResponse = {
                msg: error?.response?.data?.message,
                code: error?.response?.status,
                data: null,
                error: error?.response?.data,
                success: false,
            };
            return data;
        });
    return response;
}
export async function search(url: string, params: any) {
    try {
        const response = await axiosInstance.get(url, { params });

        const data: IApiResponse = {
            msg: response.data['message'],
            code: 200,
            data: response.data,
            error: null,
            success: true,
            count: response.data['count'],
        };
        return data;
    } catch (error: any) {
        const response = error.response;

        const data: IApiResponse = {
            msg: response.data.message,
            code: response.status,
            data: null,
            error: response.data.errors,
            success: false,
        };
        return data;
    }
}
