import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../../redux/store/hooks";
import { eventTypeSelector } from "../../../../../redux/event-types";

interface AddEventType {
  actionAddBtnRef: any;
  handleCreate: (state_id: number, name: string, slug: string) => void;
}
const CreateEventType = ({ handleCreate, actionAddBtnRef }: AddEventType) => {
  const { error: apiError } = useAppSelector(eventTypeSelector);
  const [formData, setFormData] = useState({
    state_id: 1,
    name: "",
    slug: "",
  });

  // Add handle
  const [clickStatus, setClickStatus] = useState(false);
  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // submit data
  useEffect(() => {
    if (actionAddBtnRef.current) {
      actionAddBtnRef.current.onclick = () => {
        setClickStatus(true);
      };
    }

    if (clickStatus) {
      handleCreate(formData.state_id, formData.name, formData.slug);
      setClickStatus(false);
    }
  }, [clickStatus, handleCreate]);

  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="col-md-12 col-12 p-0 mb-3">
              <div className="font-semibold">State Id</div>
              <input
                type="text"
                name="state_id"
                className="form-control border border-2 p-2 system-control "
                value={formData.state_id}
                onChange={handleChange}
              />
            </div>

            <div className="font-semibold">
              Name <span className="font-semibold">*</span>
              <input
                type="text"
                name="name"
                className="form-control border border-2 p-2 system-control "
                value={formData.name}
                onChange={handleChange}
              />
              <div className="row  align-items-start m-0 text-danger"></div>
            </div>
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Slug</div>

            <input
              type="text"
              name="slug"
              className="form-control border border-2 p-2 system-control "
              value={formData.slug}
              onChange={handleChange}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateEventType;
