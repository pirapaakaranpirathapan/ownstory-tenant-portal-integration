"use client"
import React, { useState } from 'react'
import { getLabel } from '../../../utils/lang-manager';
import RelationshipComponent from '../../../components/Ui-components/relationship/page';
import LanguagesComponent from '../../../components/Ui-components/languages/page';

const page = () => {
    const [activeTab, setActiveTab] = useState('');
    const initialLanguage = "English"
    const handleTabSelect = (key: string) => {
        setActiveTab(key);
    };
    return (
        <div
            className="row"
            id="filter-panel">
            <div className="admin-console-top-container">
                <p className="admin-console-heading">General</p>
                <span className="admin-console-sub-heading">Master Data</span>
            </div>
            <div className="admin-console-side-width1 col-md-3 col-12 m-0 p-0 ps-0 ps-md-1">
                <div className="p-3 inpage-sidebar">
                    <h1 className="single-sidebar-header">
                        <strong>{getLabel("masterData", initialLanguage)}</strong>
                    </h1>
                    <div className=" scrollmenu d-flex align-items-center d-md-block">
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('countries')}
                                className="font-semibold">
                                <div
                                    className={
                                        activeTab === 'personal-information'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `rounded shadow-md sidelink pointer `
                                    }>
                                    {getLabel("countries", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('languages')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'languages'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("languages", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('religions')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'religions'
                                            ? `system-text-primary shadow-md sidelink active font-bold r pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("religionss", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('relationships')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'relationships'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("relationships", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('salutations')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'salutations'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("salutations", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('nature-deaths')} className='font-semibold'>
                                <div
                                    className={
                                        activeTab === 'nature-deaths'
                                            ? `system-text-primary shadow-md sidelink  active font-bold  mb-0 mb-md-4 pointer`
                                            : ` sidelink  mb-0 mb-md-4 pointer`
                                    }>
                                    {getLabel("natureDeaths", initialLanguage)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-9 col-12 data-body pe-xxl-0">
                <div className="row ">
                    <div className="col-12 m-0 p-3 ps-3 ps-md-4 pe-xl-0">
                        {activeTab === 'countries' && (
                            <>
                                {/* <PersonalInformation /> */}country
                            </>
                        )}
                        {activeTab === 'languages' && (
                            <>
                                <LanguagesComponent />
                            </>
                        )}
                        {activeTab === 'religions' && (
                            <>

                            </>
                        )}
                        {activeTab === 'relationships' && (
                            <>
                                <RelationshipComponent />
                            </>
                        )}
                        {activeTab === 'salutations' && (
                            <>
                                {/* <PageManager /> */}
                            </>
                        )}
                        {activeTab === 'nature-deaths' && (
                            <>
                                {/* // <Settings /> */}
                            </>
                        )}
                    </div>
                </div>
            </div>
        </div >
    )
}

export default page