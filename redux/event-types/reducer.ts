
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createEventTypes, deleteEventTypes, getAllEventTypes, getOneEventTypes, updateEventTypes } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const eventTypeReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all relationship" action
  builder
    .addCase(getAllEventTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllEventTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllEventTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one relationship" action
  builder
    .addCase(getOneEventTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneEventTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneEventTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create relationship" action
  builder
    .addCase(createEventTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createEventTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createEventTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update relationship" action
  builder
    .addCase(updateEventTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateEventTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateEventTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete relationship" action
  builder
    .addCase(deleteEventTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteEventTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteEventTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default eventTypeReducer;
