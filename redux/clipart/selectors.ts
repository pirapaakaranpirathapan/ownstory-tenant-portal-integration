import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const clipartState = (state: RootState) => state.clipartReducer

export const clipartSelector = createSelector(
  clipartState,
  state => state
)
