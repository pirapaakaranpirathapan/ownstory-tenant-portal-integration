
import { createReducer } from '@reduxjs/toolkit';
import { getAllCountries } from './actions';
import { ResponseStatus } from '../../helpers/response_status';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const countriesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllCountries.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllCountries.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllCountries.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default countriesReducer;
