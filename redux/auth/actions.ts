
import { createAsyncThunk } from '@reduxjs/toolkit';
// import { setCookie } from 'cookies-next';
import { getCookie, setCookie } from 'cookies-next'
import { post } from '../../connections/fetch_wrapper';
import { BASE_ROUTE } from '../../config/constants';


// This file contains Redux actions related to user authentication: login and logout.

export const login = createAsyncThunk('login', async ({ data }: any, thunkAPI) => {
  const authHeader = `Basic ${btoa(`${data.email}:${data.password}`)}`;
  setCookie('authHeader', authHeader);
  try {
    const response = await post(`${BASE_ROUTE}/auth/login`, {});
    if (response.code === 200) {
      return response;
    } else {
      return thunkAPI.rejectWithValue(response);
    }
  } catch (err: any) {
    return thunkAPI.rejectWithValue(err);
  }
}
);


export const logout = createAsyncThunk('logout', async (_, thunkAPI) => {
  try {
    const response = await post(`${BASE_ROUTE}/auth/logout`, {});
    return response;
  } catch (err) {
    return thunkAPI.rejectWithValue(err);
  }
});


