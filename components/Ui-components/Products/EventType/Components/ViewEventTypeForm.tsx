import React, { useEffect, useState } from "react";
import {
  getOneEventTypes,
  eventTypeSelector,
} from "../../../../../redux/event-types";
import {
  ACTIVE_PARTNER,
  ACTIVE_SERVICE_PROVIDER,
} from "../../../../../config/constants";
import {
  useAppDispatch,
  useAppSelector,
} from "../../../../../redux/store/hooks";
import InputSkelton from "../../../../Skelton/InputSkelton";

interface ViewEditEventType {
  actionEditBtnRef: any;
  viewId: string;
  handleEdit: (state_id: number, name: string, slug: string) => void;
}
const ViewEditEventType = ({
  handleEdit,
  actionEditBtnRef,
  viewId,
}: ViewEditEventType) => {
  const dispatch = useAppDispatch();
  const [formData, setFormData] = useState({
    state_id: 1,
    name: "",
    slug: "",
  });

  // Edit handle
  const [clickStatus, setClickStatus] = useState(true);
  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // submit data
  useEffect(() => {
    if (actionEditBtnRef.current) {
      actionEditBtnRef.current.onclick = () => {
        setClickStatus(true);
      };
    }

    if (clickStatus) {
      handleEdit(formData.state_id, formData.name, formData.slug);
      setClickStatus(false);
    }
  }, [clickStatus, handleEdit]);

  const { dataOne: OneEventTypesData } = useAppSelector(eventTypeSelector);
  useEffect(() => {
    loadData();
  }, [viewId]);
  const [skelton, setSkelton] = useState(true);
  const loadData = () => {
    dispatch(
      getOneEventTypes({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        id: viewId,
      })
    ).then((res: any) => {
      if (res.payload.code === 200) {
        setSkelton(false);
      }
    });
  };

  // useEffect(() => {
  //   setFormData({
  //     state_id: OneEventTypesData[0]?.state_id,
  //     name: OneEventTypesData[0]?.name,
  //     slug: OneEventTypesData[0]?.slug,
  //   });
  // }, [loadData]);

  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              State Id<span className="font-semibold">*</span>
            </div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="state_id"
                name="state_id"
                defaultValue={OneEventTypesData[0]?.state_id}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              Name<span className="font-semibold">*</span>
            </div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="Name"
                name="name"
                defaultValue={OneEventTypesData[0]?.name}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Slug</div>
            {skelton ? (
              <InputSkelton />
            ) : (
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="slug"
                name="slug"
                defaultValue={OneEventTypesData[0]?.slug}
                onChange={handleChange}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewEditEventType;
