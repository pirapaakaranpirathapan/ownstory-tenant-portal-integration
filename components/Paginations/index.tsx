import { Pagination } from "@arco-design/web-react";
import { useEffect, useState } from "react";

function itemRender(page: any, type: string, originElement: any) {
  if (type === "prev") {
    return (
      <a
        style={{
          fontSize: 14,
          margin: "0 8px",
          fontWeight: 400,
          color: "rgba(0, 0, 0, 1)",
          lineHeight: "17.42px",
        }}
      >
        Previous
      </a>
    );
  }

  if (type === "next") {
    return (
      <a
        style={{
          fontSize: 14,
          margin: "0 8px",
          fontWeight: 400,
          color: "rgba(0, 0, 0, 1)",
          lineHeight: "17.42px",
        }}
      >
        Next
      </a>
    );
  }

  return originElement;
}

const TestPage = ({
  page,
  totalPages,
  handlePagination,
}: {
  page: number;
  totalPages: number;
  handlePagination: (pageValue: number) => void;
}) => {
  const [isMobile, setIsMobile] = useState(true);
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 576) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    };
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    // <Pagination
    //   pageSize={15}
    //   itemRender={!isMobile ? itemRender : undefined}
    //   total={totalPages}
    //   onChange={(page) => handlePagination(page)}
    //   current={page}
    //   // sizeOptions={[2]}
    //   // size="mini"
    //   // bufferSize={4}
    //   style={{
    //     display: "flex",
    //     justifyContent: "center",
    //     alignItems: "center",
    //     marginTop: 20,
    //   }}
    //   pageItemStyle={{
    //     fontSize: 13,
    //     margin: "0 5px",
    //     fontWeight: 400,
    //   }}
    // />

    <Pagination
      itemRender={!isMobile ? itemRender : undefined}
      total={200}
      pageItemStyle={{ background: 'var(--color-bg-2)' }}
      activePageItemStyle={{ background: 'var(--color-fill-2)' }}
    />
  );
};

export default TestPage;
