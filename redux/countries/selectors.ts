import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const countriesState = (state: RootState) => state.countriesReducer;

export const countriesSelector = createSelector(
    countriesState,
    state => state
)
