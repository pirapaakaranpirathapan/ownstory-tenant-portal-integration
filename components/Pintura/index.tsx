/* eslint-disable no-undef */
import { useContext, useRef, useState } from "react";

// import { pintura } from '@pqina/pintura/pintura.module.css';
import styles from "@pqina/pintura/pintura.module.css";

// react-pintura
import {
  PinturaEditor,
  PinturaEditorModal,
  PinturaEditorOverlay,
} from "@pqina/react-pintura";

// pintura
import {
  // editor
  locale_en_gb,
  createDefaultImageReader,
  createDefaultImageWriter,
  createDefaultShapePreprocessor,

  // plugins
  setPlugins,
  plugin_crop,
  plugin_crop_locale_en_gb,
  plugin_finetune,
  plugin_finetune_locale_en_gb,
  plugin_finetune_defaults,
  plugin_filter,
  plugin_filter_locale_en_gb,
  plugin_filter_defaults,
  plugin_annotate,
  plugin_annotate_locale_en_gb,
  markup_editor_defaults,
  markup_editor_locale_en_gb,
} from "@pqina/pintura";
import { getLabel } from "../../utils/lang-manager";

setPlugins(plugin_crop, plugin_finetune, plugin_filter, plugin_annotate);

const editorDefaults = {
  imageReader: createDefaultImageReader(),
  imageWriter: createDefaultImageWriter(),
  shapePreprocessor: createDefaultShapePreprocessor(),
  ...plugin_finetune_defaults,
  ...plugin_filter_defaults,
  ...markup_editor_defaults,
  locale: {
    ...locale_en_gb,
    ...plugin_crop_locale_en_gb,
    ...plugin_finetune_locale_en_gb,
    ...plugin_filter_locale_en_gb,
    ...plugin_annotate_locale_en_gb,
    ...markup_editor_locale_en_gb,
  },
};
type Props = {
  src: string;
  onLoad?: (res: any) => void;
  onVoidProcess: (res: any) => void;
};
export default function PinturaImageUploader({ src, onVoidProcess }: Props) {

  const [btnStatus, setBtnStatus] = useState(false);
  const [inlineResult, setInlineResult] = useState("");
  const [iniButtonShow, setIniButtonShow] = useState(true);
  const pinturaEditorRef = useRef<any>(null);
  const handleButtonClick = () => {
    setBtnStatus(true);
    const pinturaEditorInstance = pinturaEditorRef.current;

    if (pinturaEditorInstance) {
      pinturaEditorInstance.editor
        .processImage()
        .then((processedImage: any) => {
          onVoidProcess(processedImage?.dest);
          console.log("Processed Image:", processedImage);
        });
    }
  };
  const initialLanguage = "ENGLISH"
  return (
    <div className="App">
      <div style={{ height: "70vh" }}>
        <PinturaEditor
          ref={pinturaEditorRef}
          {...editorDefaults}
          className={styles.pintura}
          src={src}
          enableButtonExport={false}
          onReady={() => {
            setIniButtonShow(false);
          }}
        />
      </div>
      {!iniButtonShow && (
        <button
          className="btn btn-system-primary"
          onClick={handleButtonClick}
          disabled={btnStatus}
        >
          {getLabel("submit", initialLanguage)}
        </button>
      )}
    </div>
  );
}
