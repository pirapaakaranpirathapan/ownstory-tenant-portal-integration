
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createNoticeTypes, deleteNoticeTypes, getAllNoticeTypes, getOneNoticeTypes, updateNoticeTypes } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const NoticeTypeReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all relationship" action
  builder
    .addCase(getAllNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one relationship" action
  builder
    .addCase(getOneNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create relationship" action
  builder
    .addCase(createNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update relationship" action
  builder
    .addCase(updateNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete relationship" action
  builder
    .addCase(deleteNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default NoticeTypeReducer;
