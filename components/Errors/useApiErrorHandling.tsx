import { useEffect, useState } from "react";

export const useApiErrorHandling = (apiError: any, response: any) => {
  const [errors, setErrors] = useState({
    msg: "",
    name: "",
    slug: "",
    email: "",
    password: "",
    code:"",
  });
  useEffect(() => {
    if (apiError) {
      setErrors({
        msg: apiError.error ? apiError.error.message : "",
        name: apiError.error?.name
          ? apiError.error.name.length > 1
            ? apiError.error.name[1]
            : apiError.error.name[0]
          : "",

        slug: apiError.error?.first_name
          ? apiError.error.slug.length > 1
            ? apiError.error.slug[1]
            : apiError.error.slug[0]
          : "",
        email: apiError.error?.email
          ? apiError.error.email.length > 1
            ? apiError.error.email
            : apiError.error.email
          : "",
        password: apiError.error?.password
          ? apiError.error.password.length > 1
            ? apiError.error.password
            : apiError.error.password
          : "",
        code: apiError.error?.code
          ? apiError.error.code.length > 1
            ? apiError.error.code
            : apiError.error.code
          : "",
      });
    }
  }, [apiError]);
  useEffect(() => {
    setErrors({
      msg: "",
      name: "",
      slug: "",
      email: "",
      password: "",
      code:"",
    });
  }, []);
  useEffect(() => {
    if (response?.payload?.success) {
      clearErrorFields();
    }
  }, [response]);

  const clearErrorFields = () => {
    setErrors({
      msg: "",
      name: "",
      slug: "",
      email: "",
      password: "",
      code:"",
    });
  };

  return errors;
};
