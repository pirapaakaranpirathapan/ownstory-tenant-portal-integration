import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const languagesState = (state: RootState) => state.languagesReducer

export const languagesSelector = createSelector(
  languagesState,
  state => state
)
