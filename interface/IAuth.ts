
import { type } from "os";

export interface IAuth {
  data: any;
  // data(arg0: string, data: any): unknown;
  // access_token: any;
  // error: any;
  // token?: String;
  // language: string;
  access_token?: string;
  expires_in?: number;
  type?: string;
}
