export interface IRelationship {
    name: string;
    slug: string;
}