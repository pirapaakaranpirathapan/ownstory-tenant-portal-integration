
import { Action, ThunkAction, configureStore } from "@reduxjs/toolkit";
import { authReducer } from "../auth";
import { customerPageReducer } from "../topics";
import { relationshipReducer } from "../relationship";
import { serviceProviderReducer } from "../service-providers";
import { meReducer } from "../me";
import { languageReducer } from "../language";
import { languagesReducer } from "../languages";
import { countriesReducer } from "../countries";
import { pageTemplateReducer } from "../pageTemplate";
import { noticeTitleReducer } from "../notice";
import { framesReducer } from "../frames";
import { noticeTemplateReducer } from "../noticeTemplate";
import pageBackgroundReducer from "../PageBackground/reducer";
import { eventTypeReducer } from "../event-types";
import { NoticeTypeReducer } from "../notice-types";
import { contactMethodReducer } from "../contact-method";
import { clipartReducer } from "../clipart";
import { borderReducer } from "../borders";
import { headerBackgroundReducer } from "../header-backgrounds";

export const store = configureStore({
    reducer: {
        customerReducer: customerPageReducer,
        authReducer: authReducer,
        pageTemplateReducer:pageTemplateReducer,
        noticeTemplateReducer:noticeTemplateReducer,
        relationshipReducer: relationshipReducer,
        pageBackgroundReducer:pageBackgroundReducer,
        serviceProviderReducer: serviceProviderReducer,
        noticeTitleReducer: noticeTitleReducer,
        meReducer: meReducer,
        languageReducer: languageReducer,
        languagesReducer: languagesReducer,
        countriesReducer: countriesReducer,
        framesReducer: framesReducer,
        eventTypeReducer: eventTypeReducer,
        noticeTypeReducer: NoticeTypeReducer,
        contactMethodReducer: contactMethodReducer,
        clipartReducer: clipartReducer,
        borderReducer: borderReducer,
        headerBackgroundReducer: headerBackgroundReducer,
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;