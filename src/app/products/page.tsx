"use client"
import React, { useState } from 'react'
import { getLabel } from '../../../utils/lang-manager';
import RelationshipComponent from '../../../components/Ui-components/relationship/page';
import FramesComponent from '../../../components/Ui-components/Frames/page';
import ContactMethodComponent from '../../../components/Ui-components/Contact-methods/page';
import NoticeTypeComponent from '../../../components/Ui-components/Products/NoticeTypes/page';
import EventTypeComponent from '../../../components/Ui-components/Products/EventType/page';

const page = () => {
    const [activeTab, setActiveTab] = useState('');
    const initialLanguage = "English"
    const handleTabSelect = (key: string) => {
        setActiveTab(key);
    };
    return (
        <div
            className="row"
            id="filter-panel">
            <div className="admin-console-top-container">
                <p className="admin-console-heading">Products</p>
                <span className="admin-console-sub-heading">Master Data</span>
            </div>
            <div className="admin-console-side-width1 col-md-3 col-12 m-0 p-0 ps-0 ps-md-1">
                <div className="p-3 inpage-sidebar">
                    <h1 className="single-sidebar-header">
                        <strong>{getLabel("page", initialLanguage)}</strong>
                    </h1>
                    <div className=" scrollmenu d-flex align-items-center d-md-block">
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('countries')}
                                className="font-semibold">
                                <div
                                    className={
                                        activeTab === 'personal-information'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `rounded shadow-md sidelink pointer `
                                    }>
                                    {getLabel("pageTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('languages')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'languages'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("petCat", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('religions')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'religions'
                                            ? `system-text-primary shadow-md sidelink active font-bold r pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("musics", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("notice", initialLanguage)}</strong>
                        </h1>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('notices-types')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'notices-types'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("noticesTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('event-types')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'event-types'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("eventTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('contact-methods')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'contact-methods'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("contactMethods", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('social medias')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Social medias'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("socialMedias", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("video", initialLanguage)}</strong>
                        </h1>

                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('video types')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Video types'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("videoTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Video themes')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Video themes'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("videoThemes", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Video scene types')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Video scene types'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("videoSceneTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Video placements')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Video placements'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("videoPlacements", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("tributes", initialLanguage)}</strong>
                        </h1>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Tribute types')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Tribute types'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("tributeTypes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Tribute type designs')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Tribute type designs'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("tributeTypeDesigns", initialLanguage)}
                                </div>
                            </div>
                        </div>

                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("lifeStory", initialLanguage)}</strong>
                        </h1>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('Life story templates')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'Life story templates'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("lifeStoryTemplates", initialLanguage)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-9 col-12 data-body pe-xxl-0">
                <div className="row ">
                    <div className="col-12 m-0 p-3 ps-3 ps-md-4 pe-xl-0">

                        {activeTab === 'countries' && (
                            <>
                                {/* <PersonalInformation /> */}fff
                            </>
                        )}
                        {activeTab === 'languages' && (
                            <>
                                {/* <ContactDetail /> */}
                            </>
                        )}
                        {activeTab === 'religions' && (
                            <>

                            </>
                        )}
                        {activeTab === 'relationships' && (
                            <>
                                <RelationshipComponent />
                            </>
                        )}
                        {activeTab === 'notices-types' && (
                            <>
                                <NoticeTypeComponent />
                            </>
                        )}
                        {activeTab === 'event-types' && (
                            <>
                                <EventTypeComponent />
                            </>
                        )}
                        {activeTab === 'contact-methods' && (
                            <>
                                <ContactMethodComponent />
                            </>
                        )}
                    </div>
                </div>
            </div>
        </div >
    )
}

export default page