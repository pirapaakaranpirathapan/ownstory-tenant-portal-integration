import React, { useEffect, useState } from "react";
import {
    getOneRelationship,
    relationshipSelector,
} from "../../../../redux/relationship";
import {
    ACTIVE_PARTNER,
    ACTIVE_SERVICE_PROVIDER,
} from "../../../../config/constants";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";

interface ViewEditRelationship {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (name: string, slug: string) => void;
}
const ViewEditRelationship = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
}: ViewEditRelationship) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
    });

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleEdit(formData.name, formData.slug);
            setClickStatus(false);
        }
    }, [clickStatus, handleEdit]);

    const { dataOne: OneRelationshipsData } =
        useAppSelector(relationshipSelector);
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        // dispatch(
        //     getOneRelationship({
        //         activePartner: ACTIVE_PARTNER,
        //         activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        //         id: viewId,
        //     })
        // ).then((res: any) => {
        //     if (res.payload.code === 200) {
        //         setSkelton(false);
        //     }
        // });
    };

    useEffect(() => {
        setFormData({
            name: OneRelationshipsData[0]?.name,
            slug: OneRelationshipsData[0]?.slug,
        });
    }, [loadData]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">Slug</div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="slug"
                                name="slug"
                                defaultValue={formData.slug}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default ViewEditRelationship;
