
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createRelationship, deleteRelationship, getAllRelationships, getOneRelationship, updateRelationship } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const relationshipReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all relationship" action
  builder
    .addCase(getAllRelationships.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllRelationships.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllRelationships.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one relationship" action
  builder
    .addCase(getOneRelationship.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneRelationship.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneRelationship.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create relationship" action
  builder
    .addCase(createRelationship.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createRelationship.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createRelationship.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update relationship" action
  builder
    .addCase(updateRelationship.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateRelationship.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(updateRelationship.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete relationship" action
  builder
    .addCase(deleteRelationship.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteRelationship.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteRelationship.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default relationshipReducer;
