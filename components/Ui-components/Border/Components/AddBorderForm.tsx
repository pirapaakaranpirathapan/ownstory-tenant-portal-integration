import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { getLabel } from "../../../../utils/lang-manager";
import { Upload } from "@arco-design/web-react";
import FileUploder from "../../../ImageUploder/FileUploder";
import { borderSelector } from "../../../../redux/borders";

interface AddBorders {
    actionAddBtnRef: any;
    handleCreate: (name: string, slug: string, thumb_url: string) => void;
    response: any
}
const CreateBorder = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddBorders) => {
    const { error: apiError } = useAppSelector(borderSelector);
    const [formData, setFormData] = useState({
        name: "",
        slug: "",
        thumb_url: ""
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.name, formData.slug, formData.thumb_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);

    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            name: formData.name,
            thumb_url: url,
            slug: formData.slug
        });
    }

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("borderName", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.name}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.name[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("slug", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("thumbUrl", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="thumb_url"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.thumb_url}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.thumb_url[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Border
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateBorder;
