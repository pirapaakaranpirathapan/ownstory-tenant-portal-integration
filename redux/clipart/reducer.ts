
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createClipart, deleteClipart, getAllClipart, getOneClipart, updateClipart } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const clipartReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all clipart" action
  builder
    .addCase(getAllClipart.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllClipart.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllClipart.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one clipart" action
  builder
    .addCase(getOneClipart.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneClipart.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneClipart.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create clipart" action
  builder
    .addCase(createClipart.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createClipart.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createClipart.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update clipart" action
  builder
    .addCase(updateClipart.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateClipart.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateClipart.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete clipart" action
  builder
    .addCase(deleteClipart.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteClipart.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteClipart.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default clipartReducer;
