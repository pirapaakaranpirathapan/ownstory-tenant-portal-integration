"use client"
import React, { useState } from 'react'
import { Form } from 'react-bootstrap';
import { getLabel } from '../../../utils/lang-manager';
import arrow from "../../../public/images/left-arrow.png"
import Image from 'next/image';
import Link from 'next/link';
const page = () => {
    const [loginData, setLoginData] = useState({ email: "" } as unknown as any);
    const handleSubmit = () => {

    }
    const initialLanguage = "ENGLISH";
    const onChangeSelect = (name: string, value: any) => {
        setLoginData({
            ...loginData,
            [name]: value,
        });
    };
    return (
        <>
            <div className="d-flex justify-content-center w-100 mb-5 mt-5">
                <div className="col-9 col-sm-6 col-md-4 col-lg-4 col-xl-3 login-container">
                    <h5 className="d-flex mb-4 login-header">
                        {/* {getLabel("signInToBlackBeesPartner", initialLanguage)} */}
                        blackbee
                    </h5>
                    <p className="d-flex login-header-title">
                        {/* {getLabel("signInToBlackBeesPartner", initialLanguage)} */}
                        Reset password
                    </p>
                    <p className="d-flex login-header-title_sub">Enter the email associated with your account and we'll send an email with instructions to reset your password.</p>
                    <Form onSubmit={handleSubmit} className="">
                        <Form.Group controlId="formEmail">
                            <Form.Label className="m-0 mb-1">
                                {getLabel("email", initialLanguage)}
                            </Form.Label>
                            <Form.Control
                                value={loginData?.email}
                                onChange={(e) => {
                                    onChangeSelect("email", e.target.value);
                                }}
                                placeholder="sam@gmail.com"
                                className="form-control inner-shadow-default p-2"
                            />
                        </Form.Group>
                        <button
                            type="submit"
                            className="w-100 btn btn-dark mt-3 mb-4 p-2"
                        >
                            Continue
                        </button>
                        <div className="d-flex justify-content-center">
                            {/* <Error error={errors.email} /> */}
                        </div>
                        <div className="d-flex back-login">
                            <Link
                                href="/"
                                className="text-decoration-none text-dark font-bold font-100"
                            >
                                <Image src={arrow} alt='' width={20} height={20} style={{ padding: 3 }} />Back to log in
                            </Link>
                        </div>

                    </Form>
                </div>
            </div>
            <div className="bottom-container  d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <a href="#" className="a-tag-disable">
                    <span className="pointer">{getLabel("termsofService", initialLanguage)}</span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="mx-sm-3 mx-0 pointer">
                        {getLabel("support", initialLanguage)}
                    </span>
                </a>
                <a href="#" className="a-tag-disable">
                    <span className="pointer">2022 {getLabel("companyName", initialLanguage)}</span>
                </a>
            </div>
        </>
    )
}

export default page