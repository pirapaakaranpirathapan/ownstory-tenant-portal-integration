
import { createReducer } from "@reduxjs/toolkit";
import { ResponseStatus } from "../../helpers/response_status";
import { createLanguages, deleteLanguages, getAllLanguages, getOneLanguages, updateLanguages } from "./actions";

interface IReducerInitialState {
  data: any;
  dataOne: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const languagesReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all relationship" action
  builder
    .addCase(getAllLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload as any;

    })
    .addCase(getAllLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


  // Reducer for handling the "get one relationship" action
  builder
    .addCase(getOneLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne[0] = payload?.data as any;
    })
    .addCase(getOneLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create relationship" action
  builder
    .addCase(createLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      state.error = {}
    })
    .addCase(createLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update relationship" action
  builder
    .addCase(updateLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "delete relationship" action
  builder
    .addCase(deleteLanguages.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteLanguages.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteLanguages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

});
export default languagesReducer;
