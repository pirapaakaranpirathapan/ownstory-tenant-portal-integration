import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../../redux/store/hooks";
import InputSkelton from "../../../Skelton/InputSkelton";
import { headerBackgroundSelector } from "../../../../redux/header-backgrounds";

interface ViewEditHeaderBackground {
    actionEditBtnRef: any;
    viewId: string;
    handleEdit: (state_id:string, name: string, image_url: string, color: string) => void;
}
const ViewEditHeaderBackground = ({
    handleEdit,
    actionEditBtnRef,
    viewId,
}: ViewEditHeaderBackground) => {
    const dispatch = useAppDispatch();
    const [formData, setFormData] = useState({
        state_id: "",
        name: "",
        image_url: "",
        color: "",
    });

    // Edit handle
    const [clickStatus, setClickStatus] = useState(true);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionEditBtnRef.current) {
            actionEditBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleEdit(formData.state_id, formData.name, formData.image_url, formData.color);
            setClickStatus(false);
        }
    }, [clickStatus, handleEdit]);

    const { dataOne: OneHeaderBackgroundData } =
        useAppSelector(headerBackgroundSelector);
    useEffect(() => {
        loadData();
    }, [viewId]);
    const [skelton, setSkelton] = useState(true);
    const loadData = () => {
        // dispatch(
        //     getOneRelationship({
        //         activePartner: ACTIVE_PARTNER,
        //         activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        //         id: viewId,
        //     })
        // ).then((res: any) => {
        //     if (res.payload.code === 200) {
        //         setSkelton(false);
        //     }
        // });
    };

    useEffect(() => {
        setFormData({
            state_id: OneHeaderBackgroundData[0]?.state_id,
            name: OneHeaderBackgroundData[0]?.name,
            image_url: OneHeaderBackgroundData[0]?.image_url,
            color: OneHeaderBackgroundData[0]?.color,

        });
    }, [loadData]);

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            State ID<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="State ID"
                                name="state_id"
                                defaultValue={formData.state_id}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Header Background Name<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Name"
                                name="name"
                                defaultValue={formData.name}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Image Url<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Image Url"
                                name="image_url"
                                defaultValue={formData.image_url}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Color<span className="font-semibold">*</span>
                        </div>
                        {skelton ? (
                            <InputSkelton />
                        ) : (
                            <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder="Color"
                                name="color"
                                defaultValue={formData.color}
                                onChange={handleChange}
                            />
                        )}
                    </div>
                    
                </div>
            </div>
        </>
    );
};

export default ViewEditHeaderBackground;
