import Image from 'next/image'
import folder from '../../public/folder.png'
import folder2 from '../../public/folder2.png'
import React from 'react'
import Link from 'next/link'

type Props = {
  content: any
}

const DashboardCard = ({ content }: Props) => {
  return (
    <Link href="#" className='a-tag-disable '>
      <div className="card border dashboard-card mb-4">
        <div className="card-body">
          <div className="row mb-3 align-items-end">
            <div className="col-3 h-36">
              <Image src={content.image} alt="" className="dashboard-product-icon " />
            </div>
            <div className="col-9 d-flex flex-row-reverse card-title  pe-3 text-truncate a-tag-disable1">
              {content.amount}
              <span className=""> {content.amountsymbol}</span>
            </div>
          </div>
          <h6 className="card-title text-truncate a-tag-disable1 mb-2">{content.title}</h6>
          <div className="">
            <p className="fw-normal system-icon-secondary avoid-overflow-two-lines lh-sm">
              {content.description}
            </p>
          </div>
        </div>
      </div>
    </Link>

  )
}

export default DashboardCard
