import { createAsyncThunk } from '@reduxjs/toolkit';
import { deleteMethod, get, post, put } from '../../connections/fetch_wrapper';
import { IUser } from '../../interface/IUser';
import { BASE_ROUTE } from '../../config/constants';

export const getAllCustomer = createAsyncThunk('topics/get-all', async (
  {
    active_partner,
    active_service_provider,
    page,
  }: {
    active_partner: string;
    active_service_provider: string;
    page: number;
  }, { rejectWithValue }) => {
  try {
    const response = await get(`${BASE_ROUTE}/${active_partner}/topics?page=${page}`);
    if (response.code == 200) {
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});

//Single View =====================================================================================================================
export const getOneCustomer = createAsyncThunk('parties/get-one', async ({ id }: { id: string | string[] }, { rejectWithValue }) => {
  try {
    const response = await get(`parties/${id}`);
    if (response.code == 200) {
      return response;
    } else {
      console.log("🚀 ~ file: actions.ts ~ line 51 ~ createCustomer ~ response", response)
      return rejectWithValue(response)
    }

  } catch (err) {
    console.log("🚀 ~ file: actions.ts ~ line 54 ~ createCustomer ~ err", err)
    return rejectWithValue(err)
  }

});
//Create========================================================================================================
export const createCustomer = createAsyncThunk('parties/create', async ({ data }: { data: object | unknown }, { rejectWithValue }) => {
  console.log('response4', rejectWithValue);
  try {
    const response = await post('parties', data);
    if (response.code == 200) {
      console.log('response1', response);
      return response;
    } else {
      console.log('response2', response);
      return rejectWithValue(response)

    }
  } catch (err) {
    console.log('response3', err);
    return rejectWithValue(err)

  }

});
// Update 
export const updatedCustomer = createAsyncThunk('parties/updated', async ({ id, data }: { id: string | string[] | undefined, data: object | unknown }, { rejectWithValue }) => {
  try {
    const response = await put(`parties/${id}`, data);
    if (response.code == 200) {
      return response;
    } else {
      console.log("🚀 ~ file: actions.ts ~ line 51 ~ createCustomer ~ response", response)
      return rejectWithValue(response)
    }

  } catch (err) {
    console.log("🚀 ~ file: actions.ts ~ line 54 ~ createCustomer ~ err", err)
    return rejectWithValue(err)
  }

});
// Delete   ==========================================================================================
export const deleteCustomer = createAsyncThunk('parties/delete', async ({ id }: { id: string }, { rejectWithValue }) => {
  try {
    const response = await deleteMethod('parties/' + id);
    if (response.code == 200) {
      response.data = id;
      return response;
    } else {
      console.log(response);

      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});


// CLEAR   ==========================================================================================
export const clearCustomer = createAsyncThunk('parties/clear', async () => {
  return "clear";
});

//GET CUSTOMER ACCOUNT DATA =====================================================================================================================
export const getSavingAccountData = createAsyncThunk('parties/get-saving-account', async ({ id }: { id: string | string[] }, { rejectWithValue }) => {
  try {
    const response = await get(`customer/${id}/eligibility-check`);
    if (response.code == 200) {
      console.log('response', response);
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});
export const updateDp = createAsyncThunk('parties/updated-dp', async ({ id, data }: { id: string | string[] | IUser, data: any }, { rejectWithValue }) => {
  try {
    const response = await post(`users/${id}/update-dp`, data);
    if (response.code == 200) {
      console.log('response', response);
      return response;
    } else {
      return rejectWithValue(response)
    }

  } catch (err) {
    return rejectWithValue(err)
  }

});