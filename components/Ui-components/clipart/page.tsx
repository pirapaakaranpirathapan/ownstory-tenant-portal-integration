"use client"
import React, { useEffect, useRef, useState } from "react";
import Head from "next/head";
import { getLabel } from "../../../utils/lang-manager";
import Filter from "../../../components/Filter/Filter";
import { useAppDispatch, useAppSelector } from "../../../redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "../../../config/constants";
import TestPage from "../../../components/Paginations";
import FullScreenModal from "../../../components/Elements/FullScreenModal";
import { Button, Notification, Popover } from "@arco-design/web-react";
import RelationshipSkelton from "../../../components/Skelton/RelationshipSkelton";
import AlertModal from "../../../components/Elements/AlertModal";
import TableHeaderDiscriptionSkelton from "../../Skelton/TableHeaderDiscriptionSkelton";
import Image from "next/image";
import edit from "../../../public/images/edit.svg"
import BG from "../../../public/images/backImage.svg";
import { ImageNewUploder } from "../../ImageUploder/imageNewUploder";
import { clipartSelector, createClipart, deleteClipart, getAllClipart, updateClipart } from "../../../redux/clipart";
import CreateClipart from "./Components/AddClipartForm";
import frame1 from "../../../public/images/frame1.jpg"

const ClipartComponent = ({ initialLanguage }: any) => {

    // Creating a reference to a button element
    const actionAddBtnRef = useRef<HTMLButtonElement>(null);
    const actionEditBtnRef = useRef<HTMLButtonElement>(null);

    const [addButtonDisabled, setAddButtonDisabled] = useState(false);
    const [editButtonDisabled, setEditButtonDisabled] = useState(false);

    const [page, setPage] = useState(1);
    const [addModelOpen, setAddModelOpen] = useState(false);
    const [viewEditModelOpen, setViewEditModelOpen] = useState(false);
    const [viewId, setViewId] = useState("");
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [IsDeleteDisabled, setIsDeleteDisabled] = useState(false);
    const [loading, setLoading] = useState(true);
    const [createResponse, setCreateResponse] = useState("");
    const [editResponse, setEditResponse] = useState("");
    
    const [selectedOptionId, setSelectedOptionId] = useState("");
    const [showProfileShow, setshowProfileShow] = useState(false);
    // Initializing dispatch function and Fetching data using selector hook
    const dispatch = useAppDispatch();
    const { data: allClipartsData } = useAppSelector(clipartSelector);

    // Function to load data based on pagination page number
    const getAllData = (page: number) => {
        setLoading(true);
        dispatch(
            getAllClipart({
                active_partner: ACTIVE_PARTNER,
                active_service_provider: ACTIVE_SERVICE_PROVIDER,
                page: page,
            })
        ).then(() => {
            setLoading(false);
        });
    };

    useEffect(() => {
        getAllData(page);
    }, []);

    // Function to handle pagination
    const handlePagination = (page: number) => {
        setPage(page);
        getAllData(page);
    };

    // create clipart
    const handleCreate = (
        name: string,
        slug: string,
        description: string,
        thumb_url: string,
        left_url: string,
        left_print_url: string,
        right_url: string,
        right_print_url: string,
    ) => {
        setAddButtonDisabled(true);
        const createdData = {
            name: name,
            slug: slug,
            description: description,
            thumb_url: thumb_url,
            left_url: left_url,
            left_print_url: left_print_url,
            right_url: right_url,
            right_print_url: right_print_url
        };
        dispatch(
            createClipart({
                activePartner: ACTIVE_PARTNER,
                activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                createdData,
            })
        ).then((response: any) => {
            if (response.payload.success == true) {
                setCreateResponse(response);
                setAddButtonDisabled(false);
                getAllData(page);
                setAddModelOpen(false);
                Notification.success({
                    title: "Clipart Create Successfully",
                    duration: 3000,
                    content: undefined,
                });
            } else {
                setAddButtonDisabled(false);
                response.payload.code != 422 &&
                    Notification.error({
                        title: "Clipart Create Failed",
                        duration: 3000,
                        content: undefined,
                    });
            }
        });
    };

        // delete 
        const handleDelete = (viewId: string) => {
            dispatch(
                deleteClipart({
                    activePartner: ACTIVE_PARTNER,
                    activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
                    id: viewId
                })
            ).then((response: any) => {
                if (response.payload.success == true) {
                    getAllData(page);
                    setViewEditModelOpen(false);
                    setIsDeleteDisabled(false);
                    Notification.success({
                        title: "Deleted Successfully",
                        duration: 3000,
                        content: undefined,
                    });
                    setShowDeleteModal(false);
                } else {
                    setIsDeleteDisabled(false);
                    Notification.error({
                        title: "Delete Failed",
                        duration: 3000,
                        content: undefined,
                    });
                }
            });
        }
    return (
        <>
            <Head>
                <title>Blackbee Tenant | Clipart</title>
            </Head>
            <div>
                <div className="d-flex justify-content-between align-items-center page-heading-container">
                    {!loading ? <div className="page-header">
                        <p className="font-modal-header">
                            {getLabel("allClipart", initialLanguage)}
                        </p>
                        <p className="font-modal-header-description text-muted">
                            {getLabel("allClipartDescription", initialLanguage)}
                        </p>
                    </div> : <><TableHeaderDiscriptionSkelton /></>}
                    {!loading ? (
                        <button
                            type="button"
                            className="btn btn-system-primary border-0 create-button-text"
                            onClick={() => setAddModelOpen(true)}
                        >
                            <strong>+ </strong>
                            {getLabel("create", initialLanguage)}
                        </button>
                    ) : (
                        <button
                            type="button"
                            className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
                        ></button>
                    )}
                </div>
                <div className="mb-0">
                    <Filter />
                </div>

                    {/* --------------profile photos-------------- */}
                    <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start ">
                        {allClipartsData?.data?.map((item: any, index: any) => {
                            return (
                                <>
                                    <div className="position-relative profile-photo-container shimmer-effect"
                                        style={{ borderRadius: "12px" }}
                                    >
                                        <Image
                                            src={allClipartsData[index] ? item.thumb_url : BG}
                                            alt="frames"
                                            className={`pointer ${selectedOptionId === item.id ? "selected-photo-item" : ""}`}
                                            width={133}
                                            height={133}
                                            style={{ borderRadius: "12px", border: "2px" }}
                                            onClick={() => {
                                                setshowProfileShow(true);
                                                setSelectedOptionId(item.id);
                                            }}
                                        />

                                    <Popover
                                        trigger="click"
                                        position="right"
                                        content={
                                            <div className="popover-content">
                                                <li
                                                    className="pointer p-0 m-0 px-1 px-sm-4"
                                                    onClick={() => {
                                                        setViewEditModelOpen(true);
                                                    }}
                                                >
                                                    {getLabel("edit", initialLanguage)}
                                                </li>
                                                <li
                                                    className="pointer p-0 m-0 px-1 px-sm-4"
                                                    onClick={() => {
                                                        handleDelete(item.uuid);
                                                    }}
                                                >
                                                    {getLabel("delete", initialLanguage)}
                                                </li>
                                            </div>
                                        }
                                    >

                                    <Image
                                        src={edit}
                                        alt="frames"
                                        className="img-fluid position-absolute top-0 end-0 m-2 pointer"
                                    />
                                    </Popover>
                                    </div>
                                </>
                            );
                        })}
                    </div>

                {/* -------------------pagination-------------------,------- */}
                {allClipartsData && allClipartsData.data?.length == 0
                    ? ""
                    : allClipartsData.data?.length > 0 && (
                        <>
                            <hr className="" />
                            <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                                <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                                    {getLabel("totalRecords", initialLanguage)}:
                                    {/* <span> {allRelationshipsData?.total}</span>{" "} */}
                                    <span> {20}</span>
                                </div>
                                <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end">
                                    <TestPage
                                        page={15}
                                        totalPages={200}
                                        handlePagination={handlePagination}
                                    />
                                </div>
                            </div>
                        </>
                    )}
            </div>


            {/* Add model */}
            <FullScreenModal
                show={addModelOpen}
                onClose={() => {
                    setAddModelOpen(false);
                }}
                title={getLabel("addClipart", initialLanguage)}
                description={getLabel("addClipartDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 submit-button"
                            ref={actionAddBtnRef}
                            disabled={addButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                    </div>
                }
            >
                <div>
                    <CreateClipart actionAddBtnRef={actionAddBtnRef} handleCreate={handleCreate} response={createResponse}/>
                </div>
            </FullScreenModal>

           {/* Edit model */}
           <FullScreenModal
                show={viewEditModelOpen}
                onClose={() => {
                    setViewEditModelOpen(false);
                }}
                title={getLabel("viewClipart", initialLanguage)}
                description={getLabel("viewClipartDescription", initialLanguage)}
                footer={
                    <div className="d-flex align-items-center">
                        <Button
                            className="pointer btn btn-system-primary border-0 submit-button"
                            ref={actionAddBtnRef}
                            disabled={addButtonDisabled}
                        >
                            {getLabel("submit", initialLanguage)}
                        </Button>
                    </div>
                }
            >
                <div>
                    <CreateClipart actionAddBtnRef={actionAddBtnRef} handleCreate={handleCreate} response={createResponse} />
                </div>
            </FullScreenModal>
            
            {/* show image model */}
            <FullScreenModal
                show={showProfileShow}
                onClose={() => {
                    setshowProfileShow(false);
                }}
                size="lg"
                title=""
            >
                <div className="d-flex justify-content-center">
                    <div className="fullmodal-image-container">
                        <Image
                            src={frame1}
                            alt="memorialprofile"
                            className="pointer image-contain"
                            width={750}
                            height={600}
                        />
                    </div>
                </div>
            </FullScreenModal>
        </>
    );
};

export default ClipartComponent;

