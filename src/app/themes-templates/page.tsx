"use client"
import React, { useState } from 'react'
import { getLabel } from '../../../utils/lang-manager';
import RelationshipComponent from '../../../components/Ui-components/relationship/page';
import FramesComponent from '../../../components/Ui-components/Frames/page';
import NoticeTemplateComponent from '../../../components/Ui-components/noticeTemplate/page';
import PageTemplateComponent from '../../../components/Ui-components/pageTemplate/page';
import PageBackgroundComponent from '../../../components/Ui-components/PageBackground/page';
import BordersComponent from '../../../components/Ui-components/Border/page';
import HeaderBackgroundComponent from '../../../components/Ui-components/Header-background/page';
import ClipartComponent from '../../../components/Ui-components/clipart/page';

const page = () => {
    const [activeTab, setActiveTab] = useState('');
    const initialLanguage = "English"
    const handleTabSelect = (key: string) => {
        setActiveTab(key);
    };
    return (
        <div
            className="row"
            id="filter-panel">
            <div className="admin-console-top-container">
                <p className="admin-console-heading">Themes and Templates</p>
                <span className="admin-console-sub-heading">Master Data</span>
            </div>
            <div className="admin-console-side-width1 col-md-3 col-12 m-0 p-0 ps-0 ps-md-1">
                <div className="p-3 inpage-sidebar">
                    <h1 className="single-sidebar-header">
                        <strong>{getLabel("themes", initialLanguage)}</strong>
                    </h1>
                    <div className=" scrollmenu d-flex align-items-center d-md-block">
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('countries')}
                                className="font-semibold">
                                <div
                                    className={
                                        activeTab === 'personal-information'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `rounded shadow-md sidelink pointer `
                                    }>
                                    {getLabel("siteThemes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('languages')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'languages'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("partnerThemes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div
                                onClick={() => handleTabSelect('religions')}
                                className=" font-semibold">
                                <div
                                    className={
                                        activeTab === 'religions'
                                            ? `system-text-primary shadow-md sidelink active font-bold r pointer`
                                            : `sidelink pointer `
                                    }>
                                    {getLabel("embedthemes", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("templates", initialLanguage)}</strong>
                        </h1>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('page-template')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'page-template'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("pagetemplates", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('noticetemplates')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'noticetemplates'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("noticetemplates", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <h1 className="single-sidebar-header">
                            <strong>{getLabel("decorations", initialLanguage)}</strong>
                        </h1>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('frames')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'frames'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("frames", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('clipArts')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'clipArts'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("clipArts", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('borders')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'borders'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("borders", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('headerbackgrounds')} className=' font-semibold'>
                                <div
                                    className={
                                        activeTab === 'headerbackgrounds'
                                            ? `system-text-primary shadow-md sidelink active font-bold pointer`
                                            : `sidelink pointer`
                                    }>
                                    {getLabel("headerbackgrounds", initialLanguage)}
                                </div>
                            </div>
                        </div>
                        <div className="single-sidebar-row">
                            <div onClick={() => handleTabSelect('pagebackgrounds')} className='font-semibold'>
                                <div
                                    className={
                                        activeTab === 'pagebackgrounds'
                                            ? `system-text-primary shadow-md sidelink  active font-bold  mb-0 mb-md-4 pointer`
                                            : ` sidelink  mb-0 mb-md-4 pointer`
                                    }>
                                    {getLabel("pagebackgrounds", initialLanguage)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-9 col-12 data-body pe-xxl-0">
                <div className="row ">
                    <div className="col-12 m-0 p-3 ps-3 ps-md-4 pe-xl-0">
                        {activeTab === 'countries' && (
                            <>

                            </>
                        )}
                        {activeTab === 'languages' && (
                            <>

                            </>
                        )}
                        {activeTab === 'religions' && (
                            <>

                            </>
                        )}
                        {activeTab === 'page-template' && (
                            <>
                                <PageTemplateComponent />
                            </>
                        )}
                        {activeTab === 'salutations' && (
                            <>
                                {/* <PageManager /> */}
                            </>
                        )}
                        {activeTab === 'nature-deaths' && (
                            <>
                                {/* // <Settings /> */}
                            </>
                        )}
                        {activeTab === 'frames' && (
                            <>
                                <FramesComponent />
                            </>
                        )}
                        {activeTab === 'clipArts' && (
                            <>
                                <ClipartComponent />
                            </>
                        )}
                        {activeTab === 'borders' && (
                            <>
                                <BordersComponent />
                            </>
                        )}
                        {activeTab === 'headerbackgrounds' && (
                            <>
                                <HeaderBackgroundComponent />
                            </>
                        )}
                        {activeTab === 'noticetemplates' && (
                            <>
                                <NoticeTemplateComponent />
                            </>
                        )}
                        {activeTab === 'pagebackgrounds' && (
                            <>
                                <PageBackgroundComponent />

                            </>
                        )}
                    </div>
                </div>
            </div>
        </div >
    )
}

export default page