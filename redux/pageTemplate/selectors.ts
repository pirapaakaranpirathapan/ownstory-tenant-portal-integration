import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const pageTemplateState = (state: RootState) => state.pageTemplateReducer

export const pageTemplateSelector = createSelector(
  pageTemplateState,
  state => state
)
