import type { Metadata } from "next";
import { Inter } from "next/font/google";
// import "./globals.css";
import "../../src/app/styles/style.css";
import "@arco-design/web-react/dist/css/arco.css";
import Main from "./main";
import React from "react";
const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Blackbee Tenant",
  description: "Blackbee Tenant",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>
        <Main children={children} />
      </body>
    </html>
  );
}
