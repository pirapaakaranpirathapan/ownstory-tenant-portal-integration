import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store/store';

export const relationshipState = (state: RootState) => state.relationshipReducer

export const relationshipSelector = createSelector(
  relationshipState,
  state => state
)
