import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../../redux/store/hooks";
import { relationshipSelector } from "../../../../redux/relationship";
import { getLabel } from "../../../../utils/lang-manager";
import { Upload } from "@arco-design/web-react";
import FileUploder from "../../../ImageUploder/FileUploder";

interface AddFrames {
    actionAddBtnRef: any;
    handleCreate: (frame_name: string, slug: string, frame_url: string) => void;
    response: any
}
const CreateFrames = ({
    handleCreate,
    actionAddBtnRef,
    response
}: AddFrames) => {
    const { error: apiError } = useAppSelector(relationshipSelector);
    const [formData, setFormData] = useState({
        frame_name: "",
        slug: "",
        frame_url: ""
    });

    // Add handle
    const [clickStatus, setClickStatus] = useState(false);
    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    // submit data
    useEffect(() => {
        if (actionAddBtnRef.current) {
            actionAddBtnRef.current.onclick = () => {
                setClickStatus(true);
            };
        }

        if (clickStatus) {
            handleCreate(formData.frame_name, formData.slug, formData.frame_url);
            setClickStatus(false);
        }
    }, [clickStatus, handleCreate]);

    const initialLanguage = "ENSLISH";
    const handleUploadImage = (url: string) => {
        setFormData({
            frame_name: formData.frame_name,
            frame_url: url,
            slug: formData.slug
        });
    }

    return (
        <>
            <div>
                <div className="row  align-items-center  m-0 ">
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("frameName", initialLanguage)}<span className="font-semibold">*</span>
                        </div>
                        <input
                            type="text"
                            name="frame_name"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.frame_name}
                            onChange={handleChange}
                        />
                        <div className="row  align-items-start m-0 text-danger">
                            {apiError?.error?.name[0]}
                        </div>
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            {getLabel("slug", initialLanguage)}
                        </div>
                        <input
                            type="text"
                            name="slug"
                            className="form-control border border-2 p-2 system-control "
                            value={formData.slug}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="col-md-12 col-12 p-0 mb-3">
                        <div className="font-semibold">
                            Frame
                        </div>
                        <FileUploder handleUploadImage={handleUploadImage} />
                    </div>
                </div>
            </div>
        </>
    );
};

export default CreateFrames;
